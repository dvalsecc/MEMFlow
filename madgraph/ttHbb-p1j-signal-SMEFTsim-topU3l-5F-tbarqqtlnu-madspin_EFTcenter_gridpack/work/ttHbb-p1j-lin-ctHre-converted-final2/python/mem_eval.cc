// Load all the subprocesses
#include "processes.h"

#include <pybind11/pybind11.h>
#include <pybind11/numpy.h>

#include <iostream>
#include <chrono>
#include <thread>
#include <mutex>
#include <condition_variable>

class Barrier {
public:
    explicit Barrier(int count) : threadCount(count), counter(0), waiting(0) {}

    void arrive_and_wait() {
        std::unique_lock<std::mutex> lock(mtx);
        ++counter;
        ++waiting;
        if (waiting == threadCount) {
            waiting = 0;  // Reset waiting count for the next use
            cv.notify_all();  // Wake up all threads
        } else {
            cv.wait(lock, [this] { return counter % threadCount == 0; });
        }
    }

private:
    int threadCount;
    int counter;
    int waiting;
    std::mutex mtx;
    std::condition_variable cv;
};

namespace py = pybind11;

using input_array_t = py::array_t<double, py::array::c_style | py::array::forcecast>;
using input_int_t = py::int_;

constexpr std::array<std::pair<int,int>, 31> ids = {
        std::make_pair(21, 21),

	std::make_pair(1, -1),
	std::make_pair(2, -2),
	std::make_pair(3, -3),
	std::make_pair(4, -4),
	std::make_pair(5, -5),

	std::make_pair(-1, 1),
	std::make_pair(-2, 2),
	std::make_pair(-3, 3),
	std::make_pair(-4, 4),
	std::make_pair(-5, 5),
	
	std::make_pair(1, 21),
	std::make_pair(2, 21),
	std::make_pair(3, 21),
	std::make_pair(4, 21),
	std::make_pair(5, 21),

	std::make_pair(21, 1),
	std::make_pair(21, 2),
	std::make_pair(21, 3),
	std::make_pair(21, 4),
	std::make_pair(21, 5),

	std::make_pair(-1, 21),
	std::make_pair(-2, 21),
	std::make_pair(-3, 21),
	std::make_pair(-4, 21),
	std::make_pair(-5, 21),

	std::make_pair(21, -1),
	std::make_pair(21, -2),
	std::make_pair(21, -3),
	std::make_pair(21, -4),
	std::make_pair(21, -5),
};
constexpr int nprocesses = ids.size();

std::array<double, nprocesses> call_madgraph(std::vector<double*> & momenta,
		   const std::vector<ProcessClass*> & processes){

  // Loop over moments
  for (auto & process : processes) {
    // Call the matrix element
    process->setMomenta(momenta);
    // Compute the matrix element
    process->sigmaKin();
  }
  // Now storing the result
  
  std::array<double, nprocesses> results;

  int z = 0;
  for (auto & [i, j] : ids) {
    for (auto & process : processes) {
    	process->setInitial(i, j);
    	//std::cout << "Initial particles: " << i << " " << j << " : " << process->sigmaHat() << std::endl;
    	double M = process->sigmaHat();
    	if (M != 0) {
    	  results[z++] = M;
    	  break;
    	}
    }
  }
  return results;
  
}

void loop_parallel(double* ptr_input, double* results, int no_particles, int stride_event, int stride_particle, int start, int end, Barrier& syncPoint) {

    //std::cout << ptr_input << " " << start << endl;

    auto processes = getProcesses("../Cards/param_card.dat");
    
    for (int i = start; i < end; ++i) {
        std::vector<double*> momenta;

        double* event_ptr = ptr_input + i * stride_event;

        for (int j = 0; j < no_particles; ++j) {
            double* particle_ptr = event_ptr + j * stride_particle;
            momenta.push_back(particle_ptr);
        }
        
        auto out = call_madgraph(momenta, processes);


        for (int j = 0; j < nprocesses; ++j) {
            results[i * nprocesses + j] = out[j];
            //std::cout << out[j] << " ";
        }

    }
    // Wait at the barrier
    // syncPoint.arrive_and_wait();
    
}

// Dummy computation function that processes a 2D slice
py::array_t<double> compute_mem(input_array_t array, input_int_t numThreads) {
        auto buffer = array.request();
        double* ptr = static_cast<double*>(buffer.ptr);

        long int shape[3] = {buffer.shape[0], buffer.shape[1], buffer.shape[2]};
	long unsigned int strides[3] = {
	  buffer.strides[0]/sizeof(double),
	  buffer.strides[1]/sizeof(double),
	  buffer.strides[2]/sizeof(double)};

	std::cout << "Shape: " << shape[0] << " " << shape[1] << " " << shape[2] << std::endl;
	std::cout << "Strides: " << strides[0] << " " << strides[1] << " " << strides[2] << std::endl;

	// Initialize the processes
	//auto processes = getProcesses("../Cards/param_card.dat");

	auto start_time = std::chrono::high_resolution_clock::now();
	// Collect the results
	double* results = new double[shape[0] * nprocesses];

    std::vector<std::thread> threads;
    int chunkSize = shape[0] / numThreads;
    Barrier syncPoint(numThreads);
	
	// Launch threads
    for (int i = 0; i < numThreads; ++i) {
        int start = i * chunkSize;
        int end = (i == numThreads - 1) ? shape[0] : start + chunkSize;
        threads.emplace_back(loop_parallel, std::cref(ptr), std::ref(results), shape[1], strides[0], strides[1], start, end, std::ref(syncPoint));
    }

    // Join threads
    for (auto& t : threads) {
        t.join();
    }
	
	auto end_time = std::chrono::high_resolution_clock::now();
	// Compute the elapsed time
	auto duration = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time);
	std::cout << "Elapsed time for " << shape[0] << " events =" << duration.count() << " us" << std::endl;
	// Time by event
	std::cout << "Time by event: " << duration.count() / shape[0] << " us" << std::endl;

	py::ssize_t out_shape[] = {shape[0], nprocesses};
	py::ssize_t out_strides[] = {nprocesses * sizeof(double), sizeof(double)};
	py::capsule free_when_done(results, [](void* p) { delete[] reinterpret_cast<double*>(p); });
	py::array_t<double> result(out_shape, out_strides, results, free_when_done);
    
    return result;
	
    }


// Pybind11 module definition
PYBIND11_MODULE(mem_eval_lin, m) {
    m.def("compute_mem", &compute_mem, "Compute mem on input");
}
