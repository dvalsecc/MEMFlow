import numpy as np
from mem_eval import compute_mem
from mem_eval_old import compute_mem_old
import sys


# Create a 3D NumPy array
arr = np.random.rand(1000000, 6, 4).astype(np.float64)

#print(arr)
# Process the array
out = compute_mem(arr, 8)
out = np.sort(out, axis=None)
print(out.shape)
print()

out_old = compute_mem_old(arr)
out_old = np.sort(out_old, axis=None)
print(out_old.shape)
print()

print(np.all(out_old == out))
print(np.max(out_old - out))
print(np.min(out_old - out))

