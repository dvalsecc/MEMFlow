from comet_ml import Experiment
from comet_ml.integration.pytorch import log_model
from random import randint

from datetime import datetime

import torch
from memflow.read_data.dataset_all import DatasetCombined

from memflow.unfolding_flow.unfolding_flow_v2_onlyPropag import UnfoldingFlow_v2_onlyPropag
from memflow.unfolding_flow.utils import *
from memflow.unfolding_flow.mmd_loss import MMD
from memflow.unfolding_flow.utils import Compute_ParticlesTensor

import numpy as np
from torch import optim
from torch.utils.data import DataLoader
import torch.nn as nn
from torch.nn.functional import normalize
from torch.optim.lr_scheduler import CosineAnnealingLR

import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from math import floor

import mdmm
# from tensorboardX import SummaryWriter
from omegaconf import OmegaConf
import sys
import argparse
import os
from pynvml import *
import vector

from memflow.phasespace.utils import *

from earlystop import EarlyStopper
from memflow.unfolding_flow.utils import Compute_ParticlesTensor

import torch.multiprocessing as mp
from torch.utils.data.distributed import DistributedSampler
from torch.nn.parallel import DistributedDataParallel as DDP
from torch.distributed import init_process_group, destroy_process_group
from torch.profiler import profile, record_function, ProfilerActivity

from random import randint
PI = torch.pi

def attach_position(input_tensor, position):
    position = position.expand(input_tensor.shape[0], -1, -1)
    input_tensor = torch.cat((input_tensor, position), dim=2)
    
    return input_tensor

def ddp_setup(rank, world_size, port):
    """
    Args:
        rank: Unique identifier of each process
        world_size: Total number of processes
    """
    import socket
    print("Setting up ddp for device: ", rank)
    os.environ["MASTER_ADDR"] = socket.gethostname()
    os.environ["MASTER_PORT"] = f"{port}"
    init_process_group(backend="nccl", rank=rank, world_size=world_size)
    torch.cuda.set_device(rank)


def sinusoidal_positional_embedding(token_sequence_size, token_embedding_dim, device, n=10000.0):

    if token_embedding_dim % 2 != 0:
        raise ValueError("Sinusoidal positional embedding cannot apply to odd token embedding dim (got dim={:d})".format(token_embedding_dim))

    T = token_sequence_size
    d = token_embedding_dim #d_model=head_num*d_k, not d_q, d_k, d_v

    positions = torch.arange(0, T).unsqueeze_(1)
    embeddings = torch.zeros(T, d, device=device)

    denominators = torch.pow(n, 2*torch.arange(0, d//2)/d) # 10000^(2i/d_model), i is the index of embedding
    embeddings[:, 0::2] = torch.sin(positions/denominators) # sin(pos/10000^(2i/d_model))
    embeddings[:, 1::2] = torch.cos(positions/denominators) # cos(pos/10000^(2i/d_model))

    return embeddings

def train(device, path_to_dir, path_to_model, config, dtype, validation, test, chunk,
           world_size=None, device_ids=None):
    # device is device when not distributed and rank when distributed
    print("START OF RANK:", device)
    if world_size is not None:
        ddp_setup(device, world_size, config.ddp_port)

    device_id = device_ids[device] if device_ids is not None else device

    dataset = config.input_dataset_EFT
    if validation:
        print('use validation dataset')
        dataset = config.input_dataset_validation
    if test:
        print('use test dataset')
        dataset = config.input_dataset_test

    test_dataset = DatasetCombined(dataset, dev=device,
                                    dtype=dtype, datasets=['partons_lab', 'reco_lab'],
                                   reco_list_lab=['scaledLogReco_sortedBySpanet',
                                                  'mask_scaledLogReco_sortedBySpanet',
                                                  'mask_boost', 'scaledLogBoost'],
                                   parton_list_lab=['logScaled_data_higgs_t_tbar_ISR',
                                                   'lepton_partons_pdgID'])

    no_recoObjs = test_dataset.reco_lab.scaledLogReco_sortedBySpanet.shape[1]
    #charge_leptons = test_dataset.partons_lab.lepton_partons_pdgID.cpu()
    #negative_charge_1 = charge_leptons[:,0] == 13
    #negative_charge_2 = charge_leptons[:,0] == 11
    #positive_charge_1 = charge_leptons[:,0] == -13
    #positive_charge_2 = charge_leptons[:,0] == -11
    
    #negative_charge = torch.logical_or(negative_charge_1, negative_charge_2)
    #positive_charge = torch.logical_or(positive_charge_1, positive_charge_2)

    #if torch.count_nonzero(negative_charge).item() + torch.count_nonzero(positive_charge).item() != charge_leptons.shape[0]:
    #    raise Exception('Not only electrons and muons on first position')
    

    # VAlues from test dataset
    log_mean_parton_Hthad = torch.Tensor([4.6722, -6.6288e-03,  1.3556e-03])
    log_std_parton_Hthad = torch.Tensor([0.9196, 1.5784, 1.8139])
    log_mean_boost_parton = torch.Tensor([7.1853, -6.9543])
    log_std_boost_parton = torch.Tensor([4.1579e-01, 1.0232e+03])
    
    mean_ps = torch.Tensor([2.4564e+00,  1.3425e+00, -4.6606e-03,  7.8860e-04, -2.1456e-03,
         2.7660e-04, -6.6241e-03,  5.0267e-03,  4.5121e-01,  5.0199e-01])
    
    scale_ps = torch.Tensor([9.8929, 10.0761, 10.1887,  9.0733, 10.9712,  9.0767, 13.9509,  9.0681,
         3.5302,  3.6533])
    
    if device == torch.device('cuda'):
        log_mean_parton_Hthad = log_mean_parton_Hthad.cuda()
        log_std_parton_Hthad = log_std_parton_Hthad.cuda()
        log_mean_boost_parton = log_mean_boost_parton.cuda()
        log_std_boost_parton = log_std_boost_parton.cuda()

        mean_ps = mean_ps.cuda()
        scale_ps = scale_ps.cuda()

    # Initialize model
    model = UnfoldingFlow_v2_onlyPropag(scaling_partons_CM_ps=[mean_ps, scale_ps],

                                 regression_hidden_features=config.conditioning_transformer.hidden_features,
                                 regression_DNN_input=config.conditioning_transformer.hidden_features + 1,
                                 regression_dim_feedforward=config.conditioning_transformer.dim_feedforward_transformer,
                                 regression_nhead_encoder=config.conditioning_transformer.nhead_encoder,
                                 regression_noLayers_encoder=config.conditioning_transformer.no_layers_encoder,
                                 regression_noLayers_decoder=config.conditioning_transformer.no_layers_decoder,
                                 regression_DNN_layers=config.conditioning_transformer.DNN_layers,
                                 regression_DNN_nodes=config.conditioning_transformer.DNN_nodes,
                                 regression_aggregate=config.conditioning_transformer.aggregate,
                                 regression_atanh=True,
                                 regression_angles_CM=True,
                                 
                                 flow_nfeatures=config.unfolding_flow.nfeatures,
                                 flow_ncond=config.unfolding_flow.ncond, 
                                 flow_ntransforms=config.unfolding_flow.ntransforms,
                                 flow_hiddenMLP_NoLayers=config.unfolding_flow.hiddenMLP_NoLayers,
                                 flow_hiddenMLP_LayerDim=config.unfolding_flow.hiddenMLP_LayerDim,
                                 flow_bins=config.unfolding_flow.bins,
                                 flow_autoregressive=config.unfolding_flow.autoregressive, 
                                 flow_base=config.unfolding_flow.base,
                                 flow_base_first_arg=config.unfolding_flow.base_first_arg,
                                 flow_base_second_arg=config.unfolding_flow.base_second_arg,
                                 flow_bound=config.unfolding_flow.bound,
                                 randPerm=True,

                                 DNN_condition=False,
                                 DNN_layers=2,
                                 DNN_dim=256,
                                 DNN_output_dim=3,
                                 
                                 device=device,
                                 dtype=dtype,
                                 pretrained_model='a',
                                 load_conditioning_model=False)

    # Setting up DDP
    model = model.to(device)

    state_dict = torch.load(path_to_model, map_location=device)
    model.load_state_dict(state_dict['model_state_dict'])

    

    if world_size is not None:
        ddp_model = DDP(
            model,
            device_ids=[device],
            output_device=device,
            # find_unused_parameters=True,
        )
        #print(ddp_model)
        model = ddp_model.module
    else:
        ddp_model = model

    batch_size = 8 # works with 512
    total_No_samples = 10
    No_samples = 10
    fileName = f'sample_many_events_EFT_faster_{chunk}_0.pt'
    
    if few_events:
        batch_size = 2
        No_samples = 20000
        fileName = 'sample_few_events_EFT.pt'

    if validation:
        fileName = 'valid.pt'
    if test:
        fileName = 'sample_many_events_EFT_test_goodEvents.pt'
    
    # Datasets
    testLoader = DataLoader(
        test_dataset,
        batch_size= batch_size,
        shuffle=False,
        sampler=DistributedSampler(test_dataset) if world_size is not None else None,
        drop_last=False
    )
    
    # attach one-hot encoded position for jets
    pos_jets_lepton_MET = [pos for pos in range(8)] # 6 jets + lepton + MET
    pos_other_jets = [8 for pos in range(no_recoObjs - 8)]
    
    pos_jets_lepton_MET = torch.tensor(pos_jets_lepton_MET, device=device, dtype=dtype)
    pos_other_jets = torch.tensor(pos_other_jets, device=device, dtype=dtype)
    pos_logScaledReco = torch.cat((pos_jets_lepton_MET, pos_other_jets), dim=0).unsqueeze(dim=1)

    # attach one-hot encoded position for partons
    pos_partons = torch.tensor([pos for pos in range(4)], device=device, dtype=dtype).unsqueeze(dim=1) # higgs, t1, t2, ISR

    # sin_cos embedding
    pos_logScaledReco = sinusoidal_positional_embedding(token_sequence_size=no_recoObjs,
                                                        token_embedding_dim=config.conditioning_transformer.hidden_features,
                                                        device=device,
                                                        n=10000.0)

    No_regressed_vars = 4
    # 9 partons
    pos_partons = sinusoidal_positional_embedding(token_sequence_size=No_regressed_vars,
                                                  token_embedding_dim=config.conditioning_transformer.hidden_features,
                                                  device=device,
                                                  n=10000.0)

    
    # new order: lepton MET higgs1 higgs2 etc
    new_order_list = [6,7,0,1,2,3,4,5]
    lastElems = [i+8 for i in range(no_recoObjs - 8)]
    new_order_list = new_order_list + lastElems
    new_order = torch.LongTensor(new_order_list)

    partons_order = [0,1,2,3]

    partons_name = ['higgs', 'thad', 'tlep', 'ISR']
    partons_var = ['pt','eta','phi']
    boost_var = ['E', 'pz']

    E_CM = 13000
    rambo = PhaseSpace(E_CM, [21,21], [25,6,-6,21], dev=device) 
    
    posLogParton = torch.linspace(0, 1, No_regressed_vars, device=device, dtype=dtype)

    no_chunks = 15

    if not test:

        full_flow_cond_vector = torch.empty((no_chunks * batch_size, 10), device=torch.device('cuda'), dtype=dtype)
        unscaled_sampledTensor = torch.empty((no_chunks * batch_size, total_No_samples + 1, 6, 4), device=torch.device('cuda'), dtype=dtype)
        full_log_prob_samples = torch.empty((no_chunks * batch_size, total_No_samples + 1, 3), device=torch.device('cuda'), dtype=dtype)

    else:
        full_flow_cond_vector = torch.empty((test_dataset.reco_lab.scaledLogReco_sortedBySpanet.shape[0], 10), device=torch.device('cuda'), dtype=dtype)
        unscaled_sampledTensor = torch.empty((test_dataset.reco_lab.scaledLogReco_sortedBySpanet.shape[0], total_No_samples + 1, 6, 4), device=torch.device('cuda'), dtype=dtype)
        full_log_prob_samples = torch.empty((test_dataset.reco_lab.scaledLogReco_sortedBySpanet.shape[0], total_No_samples + 1, 3), device=torch.device('cuda'), dtype=dtype)

    print("Before sampling loop")
    model.eval()
    ddp_model.eval()

    First_event_processed = no_chunks*chunk*batch_size
    print(First_event_processed)

    finish_flag = 0

    for index_samples in range(floor(total_No_samples / No_samples)):

        samples_start = index_samples * No_samples + 1 # + 1 because on first position we have the target
        samples_end = (index_samples + 1) * No_samples + 1 # + 1 because on first position we have the target
    
        for i, data_batch in enumerate(testLoader):
            # Move data to device
            with torch.no_grad():
    
                if (i % 10 == 0):
                    print(f'batch {i} from {len(testLoader)}')

                (logScaled_partons, charge_leptons,
                logScaled_reco_sortedBySpanet, mask_recoParticles,
                mask_boost_reco, data_boost_reco) = data_batch

                negative_charge_1 = charge_leptons[:,0] == 13
                negative_charge_2 = charge_leptons[:,0] == 11
                positive_charge_1 = charge_leptons[:,0] == -13
                positive_charge_2 = charge_leptons[:,0] == -11
                
                negative_charge = torch.logical_or(negative_charge_1, negative_charge_2)
                positive_charge = torch.logical_or(positive_charge_1, positive_charge_2)

                if torch.count_nonzero(negative_charge).item() + torch.count_nonzero(positive_charge).item() != charge_leptons.shape[0]:
                    raise Exception('Not only electrons and muons on first position')

                # get good events: to have the tagged jets in the first 9 positions (not necessary assigned correctly by spanet)
                mask_goodEvents = torch.count_nonzero(recoTarget[:,:9,-2] > 0, dim=1) == 7
                no_goodEvents = torch.count_nonzero(mask_goodEvents)
                if (no_goodEvents == 0):
                    continue

                event_start = i*batch_size - First_event_processed
                event_end = event_start + no_goodEvents
                
                if event_end > unscaled_sampledTensor.shape[0]:
                    event_end = test_dataset.reco_lab.scaledLogReco_sortedBySpanet.shape[0]
                    finish_flag = 1
                    
                    indices_true = torch.where(mask_goodEvents)[0]
                    mask_goodEvents[indices_true[event_end-event_start:]] = False # if there are more good events than we need -> set the last of them to False

                logScaled_partons = logScaled_partons[mask_goodEvents]
                charge_leptons = charge_leptons[mask_goodEvents]
                logScaled_reco_sortedBySpanet = logScaled_reco_sortedBySpanet[mask_goodEvents]
                mask_recoParticles = mask_recoParticles[mask_goodEvents]
                mask_boost_reco = mask_boost_reco[mask_goodEvents]
                data_boost_reco = data_boost_reco[mask_goodEvents]
                    
                if index_samples == 0: # compute the regression only for the first model
        
                     # exist + 3-mom
                    logScaled_reco_sortedBySpanet = logScaled_reco_sortedBySpanet[:,:,[0,1,2,3]]
                    # The provenance is remove in the model
        
                    # put the lepton first:
                    logScaled_reco_sortedBySpanet = logScaled_reco_sortedBySpanet[:,new_order,:]
                    mask_recoParticles = mask_recoParticles[:,new_order]
        
                    # same order for partons:
                    logScaled_partons = logScaled_partons[:,partons_order,:]
        
                    # remove prov from partons
                    logScaled_partons = logScaled_partons[:,:,[0,1,2]] # [pt,eta,phi,parton_id, type] -> skip type=1/2 for partons/leptons
                    
                    flow_cond_vector, \
                    sampled_HthadtlepISR_lab_ptetaphi_scaled, \
                    log_prob_samples, rambo_weight, sigmoid_jacobian  = ddp_model.sample_EFT(logScaled_reco_sortedBySpanet,
                                                                                              data_boost_reco,
                                                                                                mask_recoParticles, mask_boost_reco,
                                                                                                                                                
                                                                                                log_mean_boost_parton=log_mean_boost_parton,
                                                                                                log_std_boost_parton=log_std_boost_parton,
                                                                                                log_mean_parton_Hthad=log_mean_parton_Hthad,
                                                                                                log_std_parton_Hthad=log_std_parton_Hthad,
                                                                                                                 
                                                                                                order=[0,1,2,3],
                                                                                                disableGradConditioning=True,
                                                                                                flow_eval="both",
                                                                                                Nsamples=No_samples, No_regressed_vars=No_regressed_vars,
                                                                                                sin_cos_embedding=True, sin_cos_reco=pos_logScaledReco,
                                                                                                sin_cos_partons=pos_partons,
                                                                                                attach_position_regression=posLogParton,
                                                                                                rambo=rambo)

                    # without phi
                    unscaledTarget = logScaled_partons[...,0:3]
                    unscaledTarget[...,[0,1]] = (unscaledTarget[...,[0,1]]*log_std_parton_Hthad[:2] + log_mean_parton_Hthad[:2])
                    unscaledTarget[...,0] = torch.exp(unscaledTarget[...,0]) - 1 # unscale pt
        
                    higgs_target_cartesian = Compute_ParticlesTensor.get_cartesian_comp(unscaledTarget[:,0], mass=125.25)
                    thad_target_cartesian = Compute_ParticlesTensor.get_cartesian_comp(unscaledTarget[:,1], mass=172.5)
                    tlep_target_cartesian = Compute_ParticlesTensor.get_cartesian_comp(unscaledTarget[:,2], mass=172.5)
                    ISR_target_cartesian = Compute_ParticlesTensor.get_cartesian_comp(unscaledTarget[:,3], mass=0.0)
                    padding_x1x2 = -1*torch.ones((ISR_target_cartesian.shape[0], 1, 2, 4), device=device, dtype=dtype)

                    target_event = torch.cat((padding_x1x2, higgs_target_cartesian[:,None,None], thad_target_cartesian[:,None,None],
                                          tlep_target_cartesian[:,None,None], ISR_target_cartesian[:,None,None]),
                                           dim=2)

                    unscaled_sampledTensor[event_start:event_end, 0:1] = torch.where(negative_charge[:, None, None, None],
                                                                                    target_event[:,:,[0,1,3,4,2,5]],
                                                                                    target_event[:,:,[0,1,4,3,2,5]])
                    
                    full_flow_cond_vector[event_start:event_end] = flow_cond_vector

                else:
                    sampled_HthadtlepISR_lab_ptetaphi_scaled, \
                    log_prob_samples, rambo_weight, sigmoid_jacobian  = ddp_model.sample_EFT_noRegr(full_flow_cond_vector[event_start: event_end],
                                                                                                    No_samples = No_samples,
                                                                                                    rambo=rambo)


                no_ev = event_end - event_start
                fullGeneratedEvent = torch.reshape(sampled_HthadtlepISR_lab_ptetaphi_scaled, (no_ev, No_samples, 6, 4))
                rambo_weight = torch.reshape(rambo_weight, (no_ev, No_samples, 1))
                sigmoid_jacobian = torch.reshape(sigmoid_jacobian, (no_ev, No_samples, 1))
                log_prob_samples = torch.reshape(log_prob_samples, (no_ev, No_samples, 1))
    
                sigmoid_jacobian = sigmoid_jacobian * torch.prod(scale_ps) # jacobian of scaling = constant
                log_prob_samples = torch.exp(log_prob_samples) # from log space to normal space
    
                probabilities = torch.cat((log_prob_samples, sigmoid_jacobian, rambo_weight), dim=-1)
                
                unscaled_sampledTensor[event_start:event_end, samples_start:samples_end] = torch.where(negative_charge[:, None, None, None],
                                                                                                        fullGeneratedEvent[:,:,[0,1,3,4,2,5]],
                                                                                                        fullGeneratedEvent[:,:,[0,1,4,3,2,5]])
                
                full_log_prob_samples[event_start:event_end, samples_start:samples_end] = probabilities

                if finish_flag == 1:
                    break


    # if negative charge => top = top hadronic and antitop = top leptonic
    # if positive charge => top = top leptonic and antitop = top hadronic
    #events_processed_start = no_chunks*chunk*batch_size
    #events_processed_end = no_chunks*(chunk+1)*batch_size
    #neg_charge = negative_charge[events_processed_start: events_processed_end, None, None, None]

    #unscaled_sampledTensor = unscaled_sampledTensor.cpu()
    #unscaled_sampledTensor = torch.where(neg_charge,
    #                                     unscaled_sampledTensor[:,:,[0,1,3,4,2,5]], # x1x2, top hadronic, top leptonic, H, g
    #                                     unscaled_sampledTensor[:,:,[0,1,4,3,2,5]]) # x1x2, top leptonic, top hadronic, H, g

    torch.save((unscaled_sampledTensor, full_flow_cond_vector, full_log_prob_samples), f'{path_to_dir}/{fileName}') # todo: save good files
    # exp_log.end()
    destroy_process_group()
    print('Sampling finished!!')
        

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--path-model', type=str, required=True, help='path to model directory')
    parser.add_argument('--chunk', type=int, default=0, help='chunk number')
    parser.add_argument('--few-events',action="store_true",  help='sample 20k reco events for only 50 gen-level events')
    parser.add_argument('--on-GPU', action="store_true",  help='run on GPU boolean')
    parser.add_argument('--validation', action="store_true",  help='use validation dataset')
    parser.add_argument('--test', action="store_true",  help='use test dataset')
    parser.add_argument('--distributed', action="store_true")
    args = parser.parse_args()
    
    path_to_dir = args.path_model
    chunk = args.chunk
    few_events = args.few_events
    on_GPU = args.on_GPU # by default run on CPU
    validation = args.validation
    test = args.test

    path_to_conf = path_to_dir + '/config_spanet_labframe_v2-gluon-dist-redo5.yaml'
    path_to_model = path_to_dir + '/model_spanet_labframe_v2-gluon-dist-redo5.pt'

    # Read config file in 'conf'
    with open(path_to_conf) as f:
        conf = OmegaConf.load(path_to_conf)
    
    print("Training with cfg: \n", OmegaConf.to_yaml(conf))

    env_var = os.environ.get("CUDA_VISIBLE_DEVICES")
    if env_var:
        actual_devices = env_var.split(",")
    else:
        actual_devices = list(range(torch.cuda.device_count()))
    print("Actual devices: ", actual_devices)
    world_size = len(actual_devices)

    if conf.training_params.dtype == "float32":
        dtype = torch.float32
    elif conf.training_params.dtype == "float64":
        dtype = torch.float64
    else:
        dtype = None
    
    if len(actual_devices) > 1 and args.distributed:
        # make a dictionary with k: rank, v: actual device
        dev_dct = {i: actual_devices[i] for i in range(world_size)}
        print(f"Devices dict: {dev_dct}")
        mp.spawn(
            train,
            args=(device, path_to_dir, path_to_model, conf, dtype, validation, test, chunk,
                    world_size, dev_dct),
            nprocs=world_size,
            # join=True
        )
    else:
        device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
        train(device, path_to_dir, path_to_model, conf, dtype, validation, test, chunk, None, None)
    
    print(f"Sampled succesfully! Version: {conf.version}")
    
    
    
    
    
    
    
    
    
