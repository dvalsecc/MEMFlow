from comet_ml import Experiment
from comet_ml.integration.pytorch import log_model
from random import randint

from datetime import datetime

import torch
from memflow.read_data.dataset_all import DatasetCombined

from memflow.unfolding_flow.unfolding_flow_v2_onlyPropag import UnfoldingFlow_v2_onlyPropag
from memflow.unfolding_flow.utils import *
from memflow.unfolding_flow.mmd_loss import MMD
from memflow.unfolding_flow.utils import Compute_ParticlesTensor

import numpy as np
from torch import optim
from torch.utils.data import DataLoader
import torch.nn as nn
from torch.nn.functional import normalize
from torch.optim.lr_scheduler import CosineAnnealingLR

import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from math import floor

import mdmm
# from tensorboardX import SummaryWriter
from omegaconf import OmegaConf
import sys
import argparse
import os
from pynvml import *
import vector

from memflow.phasespace.utils import *

from earlystop import EarlyStopper
from memflow.unfolding_flow.utils import Compute_ParticlesTensor

import torch.multiprocessing as mp
from torch.utils.data.distributed import DistributedSampler
from torch.nn.parallel import DistributedDataParallel as DDP
from torch.distributed import init_process_group, destroy_process_group
from torch.profiler import profile, record_function, ProfilerActivity

from random import randint
PI = torch.pi

def attach_position(input_tensor, position):
    position = position.expand(input_tensor.shape[0], -1, -1)
    input_tensor = torch.cat((input_tensor, position), dim=2)
    
    return input_tensor

def ddp_setup(rank, world_size, port):
    """
    Args:
        rank: Unique identifier of each process
        world_size: Total number of processes
    """
    import socket
    print("Setting up ddp for device: ", rank)
    os.environ["MASTER_ADDR"] = socket.gethostname()
    os.environ["MASTER_PORT"] = f"{port}"
    init_process_group(backend="nccl", rank=rank, world_size=world_size)
    torch.cuda.set_device(rank)

def train(device, path_to_dir, path_to_model, config, dtype, validation,
           world_size=None, device_ids=None):
    # device is device when not distributed and rank when distributed
    print("START OF RANK:", device)
    if world_size is not None:
        ddp_setup(device, world_size, config.ddp_port)

    device_id = device_ids[device] if device_ids is not None else device

    dataset = config.input_dataset_test
    if validation:
        print('use validation dataset')
        dataset = config.input_dataset_validation
 
    test_dataset = DatasetCombined(dataset, dev=device,
                                    dtype=dtype, datasets=['partons_lab', 'reco_lab'],
                                   reco_list_lab=['scaledLogReco_sortedBySpanet',
                                                  'mask_scaledLogReco_sortedBySpanet',
                                                  'mask_boost', 'scaledLogBoost'],
                                   parton_list_cm=['phasespace_intermediateParticles_onShell_logit_scaled'],
                                                   #'phasespace_rambo_detjacobian_onShell'],
                                   parton_list_lab=['logScaled_data_higgs_t_tbar_ISR',
                                                   'logScaled_data_boost'])

    no_recoObjs = train_dataset.reco_lab.scaledLogReco_sortedBySpanet.shape[1]

    log_mean_reco = train_dataset.reco_lab.meanRecoParticles
    log_std_reco = train_dataset.reco_lab.stdRecoParticles
    log_mean_parton_Hthad = train_dataset.partons_lab.mean_log_data_higgs_t_tbar_ISR
    log_std_parton_Hthad = train_dataset.partons_lab.std_log_data_higgs_t_tbar_ISR
    log_mean_boost_parton = train_dataset.partons_lab.mean_log_data_boost
    log_std_boost_parton = train_dataset.partons_lab.std_log_data_boost
    mean_ps = train_dataset.partons_CM.mean_phasespace_intermediateParticles_onShell_logit
    scale_ps = train_dataset.partons_CM.std_phasespace_intermediateParticles_onShell_logit
    
    if device == torch.device('cuda'):
        log_mean_reco = log_mean_reco.cuda()
        log_std_reco = log_std_reco.cuda()

        log_mean_parton_Hthad = log_mean_parton_Hthad.cuda()
        log_std_parton_Hthad = log_std_parton_Hthad.cuda()
        log_mean_boost_parton = log_mean_boost_parton.cuda()
        log_std_boost_parton = log_std_boost_parton.cuda()

        mean_ps = mean_ps.cuda()
        scale_ps = scale_ps.cuda()

    # Initialize model
    model = UnfoldingFlow_v2_onlyPropag(scaling_partons_CM_ps=[mean_ps, scale_ps],

                                 regression_hidden_features=config.conditioning_transformer.hidden_features,
                                 regression_DNN_input=config.conditioning_transformer.hidden_features + 1,
                                 regression_dim_feedforward=config.conditioning_transformer.dim_feedforward_transformer,
                                 regression_nhead_encoder=config.conditioning_transformer.nhead_encoder,
                                 regression_noLayers_encoder=config.conditioning_transformer.no_layers_encoder,
                                 regression_noLayers_decoder=config.conditioning_transformer.no_layers_decoder,
                                 regression_DNN_layers=config.conditioning_transformer.DNN_layers,
                                 regression_DNN_nodes=config.conditioning_transformer.DNN_nodes,
                                 regression_aggregate=config.conditioning_transformer.aggregate,
                                 regression_atanh=True,
                                 regression_angles_CM=True,
                                 
                                 flow_nfeatures=config.unfolding_flow.nfeatures,
                                 flow_ncond=config.unfolding_flow.ncond, 
                                 flow_ntransforms=config.unfolding_flow.ntransforms,
                                 flow_hiddenMLP_NoLayers=config.unfolding_flow.hiddenMLP_NoLayers,
                                 flow_hiddenMLP_LayerDim=config.unfolding_flow.hiddenMLP_LayerDim,
                                 flow_bins=config.unfolding_flow.bins,
                                 flow_autoregressive=config.unfolding_flow.autoregressive, 
                                 flow_base=config.unfolding_flow.base,
                                 flow_base_first_arg=config.unfolding_flow.base_first_arg,
                                 flow_base_second_arg=config.unfolding_flow.base_second_arg,
                                 flow_bound=config.unfolding_flow.bound,
                                 randPerm=True,

                                 DNN_condition=False,
                                 DNN_layers=2,
                                 DNN_dim=256,
                                 DNN_output_dim=3,
                                 
                                 device=device,
                                 dtype=dtype,
                                 pretrained_model=path_regression,
                                 #load_conditioning_model=False) #todo
                                 load_conditioning_model=True)

    # Setting up DDP
    model = model.to(device)

    state_dict = torch.load(path_to_model, map_location=device)
    model.load_state_dict(state_dict['model_state_dict'])

    if world_size is not None:
        ddp_model = DDP(
            model,
            device_ids=[device],
            output_device=device,
            # find_unused_parameters=True,
        )
        #print(ddp_model)
        model = ddp_model.module
    else:
        ddp_model = model

    batch_size = config.training_params.batch_size_training
    No_samples = 30
    fileName = 'sample_many_events.pt'
    No_eventsSampled = test_dataset.reco_lab.scaledLogReco_sortedBySpanet.shape[0] / batch_size
    
    if few_events:
        batch_size = 2
        No_samples = 20000
        fileName = 'sample_few_events.pt'
        No_eventsSampled = 50

    if validation:
        fileName = 'valid.pt'
        
    # Datasets
    testLoader = DataLoader(
        test_dataset,
        batch_size= batch_size,
        shuffle=False if world_size is not None else True,
        sampler=DistributedSampler(test_dataset) if world_size is not None else None,
        drop_last=True
    )
    
    # attach one-hot encoded position for jets
    pos_jets_lepton_MET = [pos for pos in range(8)] # 6 jets + lepton + MET
    pos_other_jets = [8 for pos in range(no_recoObjs - 8)]
    
    pos_jets_lepton_MET = torch.tensor(pos_jets_lepton_MET, device=device, dtype=dtype)
    pos_other_jets = torch.tensor(pos_other_jets, device=device, dtype=dtype)
    pos_logScaledReco = torch.cat((pos_jets_lepton_MET, pos_other_jets), dim=0).unsqueeze(dim=1)

    # attach one-hot encoded position for partons
    pos_partons = torch.tensor([pos for pos in range(4)], device=device, dtype=dtype).unsqueeze(dim=1) # higgs, t1, t2, ISR

    # sin_cos embedding
    pos_logScaledReco = sinusoidal_positional_embedding(token_sequence_size=no_recoObjs,
                                                        token_embedding_dim=config.conditioning_transformer.hidden_features,
                                                        device=device,
                                                        n=10000.0)

    No_regressed_vars = 4
    # 9 partons
    pos_partons = sinusoidal_positional_embedding(token_sequence_size=No_regressed_vars,
                                                  token_embedding_dim=config.conditioning_transformer.hidden_features,
                                                  device=device,
                                                  n=10000.0)

    
    # new order: lepton MET higgs1 higgs2 etc
    new_order_list = [6,7,0,1,2,3,4,5]
    lastElems = [i+8 for i in range(no_recoObjs - 8)]
    new_order_list = new_order_list + lastElems
    new_order = torch.LongTensor(new_order_list)

    partons_order = [0,1,2,3]

    partons_name = ['higgs', 'thad', 'tlep', 'ISR']
    partons_var = ['pt','eta','phi']
    boost_var = ['E', 'pz']

    E_CM = 13000
    rambo = PhaseSpace(E_CM, [21,21], [25,6,-6,21], dev=device) 
    
    posLogParton = torch.linspace(0, 1, No_regressed_vars, device=device, dtype=dtype)

    unscaled_sampledTensor = torch.empty((0, No_samples + 1, 4, 3), device=device, dtype=dtype)
               
    print("Before sampling loop")
    model.eval()
    ddp_model.eval()
    
    for i, data_batch in enumerate(testLoader):
        # Move data to device
        with torch.no_grad():

            if (few_events and batch_size*i > 50):
                break

            if (i % 50 == 0):
                print(f'batch {i} from {len(testLoader)}')

            (logScaled_partons,
             logScaled_parton_boost,
             logScaled_reco_sortedBySpanet, mask_recoParticles,
             mask_boost_reco, data_boost_reco,
             ps_onShell_logit_scaled) = data_batch

             # exist + 3-mom
            logScaled_reco_sortedBySpanet = logScaled_reco_sortedBySpanet[:,:,[0,1,2,3]]
            # The provenance is remove in the model

            # put the lepton first:
            logScaled_reco_sortedBySpanet = logScaled_reco_sortedBySpanet[:,new_order,:]
            mask_recoParticles = mask_recoParticles[:,new_order]

            # same order for partons:
            logScaled_partons = logScaled_partons[:,partons_order,:]

            # remove prov from partons
            logScaled_partons = logScaled_partons[:,:,[0,1,2]] # [pt,eta,phi,parton_id, type] -> skip type=1/2 for partons/leptons
    
            regressed_HthadtlepISR_lab_ptetaphi_scaled, boost_regressed_Epz_scaled, flow_prob, \
            sampled_HthadtlepISR_lab_ptetaphi_scaled, boost_sampled_scaled  = ddp_model(logScaled_reco_sortedBySpanet, data_boost_reco,
                                                                    mask_recoParticles, mask_boost_reco,
                                                                    logit_ps_scaled_target = ps_onShell_logit_scaled,
                                                                                                                    
                                                                    log_mean_boost_parton=log_mean_boost_parton,
                                                                    log_std_boost_parton=log_std_boost_parton,
                                                                    log_mean_parton_Hthad=log_mean_parton_Hthad,
                                                                    log_std_parton_Hthad=log_std_parton_Hthad,
                                                                                     
                                                                    order=[0,1,2,3],
                                                                    disableGradConditioning=disable_grad_conditioning,
                                                                    flow_eval="both",
                                                                    Nsamples=No_samples, No_regressed_vars=No_regressed_vars,
                                                                    sin_cos_embedding=True, sin_cos_reco=pos_logScaledReco,
                                                                    sin_cos_partons=pos_partons,
                                                                    attach_position_regression=posLogParton,
                                                                    rambo=rambo)

            print(sampled_HthadtlepISR_lab_ptetaphi_scaled.shape)
                    
            fullGeneratedEvent = torch.reshape(sampled_HthadtlepISR_lab_ptetaphi_scaled, (No_samples, batch_size, config.transferFlow.no_max_objects, 5))
            fullGeneratedEvent = torch.transpose(fullGeneratedEvent, 0, 1)

            # without phi
            fullGeneratedEvent[...,[0,1]] = (fullGeneratedEvent[...,[0,1]]*log_std_parton_Hthad[:2] + log_mean_parton_Hthad[:2])
            fullGeneratedEvent[...,0] = (torch.exp(fullGeneratedEvent[...,1]) - 1) # unscale pt

            # without phi
            unscaledTarget = torch.clone(logScaled_partons[...,0:3])
            unscaledTarget[...,[0,1]] = (unscaledTarget[...,[0,1]]*log_std_parton_Hthad[:2] + log_mean_parton_Hthad[:2])
            unscaledTarget[...,0] = (torch.exp(unscaledTarget[...,0]) - 1) # unscale pt

            fullGeneratedEvent_withFirstTarget = torch.cat((unscaledTarget[:,:,:config.transferFlow.no_max_objects],
                                                           fullGeneratedEvent[...,0:3]),
                                                           dim=1)
            
            unscaled_sampledTensor = torch.cat((unscaled_sampledTensor, fullGeneratedEvent_withFirstTarget), dim=0)            
            
            
    torch.save(unscaled_sampledTensor, f'{path_to_dir}/{fileName}')
    # exp_log.end()
    destroy_process_group()
    print('Sampling finished!!')
        

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--path-model', type=str, required=True, help='path to model directory')
    parser.add_argument('--few-events',action="store_true",  help='sample 20k reco events for only 50 gen-level events')
    parser.add_argument('--on-GPU', action="store_true",  help='run on GPU boolean')
    parser.add_argument('--validation', action="store_true",  help='use validation dataset')
    parser.add_argument('--distributed', action="store_true")
    args = parser.parse_args()
    
    path_to_dir = args.path_model
    few_events = args.few_events
    on_GPU = args.on_GPU # by default run on CPU
    validation = args.validation

    path_to_conf = path_to_dir + '/config_spanet_labframe_v2-gluon-dist-redo5.yaml'
    path_to_model = path_to_dir + '/model_spanet_labframe_v2-gluon-dist-redo5.pt'

    # Read config file in 'conf'
    with open(path_to_conf) as f:
        conf = OmegaConf.load(path_to_conf)
    
    print("Training with cfg: \n", OmegaConf.to_yaml(conf))

    env_var = os.environ.get("CUDA_VISIBLE_DEVICES")
    if env_var:
        actual_devices = env_var.split(",")
    else:
        actual_devices = list(range(torch.cuda.device_count()))
    print("Actual devices: ", actual_devices)
    world_size = len(actual_devices)

    if conf.training_params.dtype == "float32":
        dtype = torch.float32
    elif conf.training_params.dtype == "float64":
        dtype = torch.float64
    else:
        dtype = None
    
    if len(actual_devices) > 1 and args.distributed:
        # make a dictionary with k: rank, v: actual device
        dev_dct = {i: actual_devices[i] for i in range(world_size)}
        print(f"Devices dict: {dev_dct}")
        mp.spawn(
            train,
            args=(device, path_to_dir, path_to_model, conf, dtype, validation,
                    world_size, dev_dct),
            nprocs=world_size,
            # join=True
        )
    else:
        device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
        train(device, path_to_dir, path_to_model, conf, dtype, validation, None, None)
    
    print(f"Sampled succesfully! Version: {conf.version}")
    
    
    
    
    
    
    
    
    
