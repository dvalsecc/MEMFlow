from comet_ml import Experiment
from comet_ml.integration.pytorch import log_model
from random import randint

import memflow.phasespace.utils as ps_utils

from datetime import datetime

import torch
from memflow.read_data.dataset_all import DatasetCombined
import netcal

import pickle
from torch import nn

from memflow.unfolding_flow.unfolding_flow_v2_onlyPropag import UnfoldingFlow_v2_onlyPropag
from memflow.unfolding_flow.utils import *
from memflow.unfolding_flow.utils import Compute_ParticlesTensor

from memflow.transfer_flow.transfer_flow_paper_AllPartons_Nobtag_autoreg_latentSpace_gaussian_v3 import TransferFlow_Paper_AllPartons_Nobtag_autoreg_latentSpace_gaussian_v3

import numpy as np
from torch.utils.data import DataLoader

import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from math import floor

from omegaconf import OmegaConf
import sys
import argparse
import os
from pynvml import *
import vector

from memflow.phasespace.utils import *

from memflow.unfolding_flow.utils import Compute_ParticlesTensor


from memflow.phasespace.phasespace import PhaseSpace

from random import randint
PI = torch.pi

def compute_fullTF(log_prob_classifier_perObj, log_prob_kin_perObj, mask_ExistReco, 
                   NoJets_classifier, NoJets_total, No_maxJets_cut,
                   Calibration_dir, unscaled_jetsPt, std_pt_Jets=0.5822, std_eta_Jets=0.9795,
                   device=torch.device('cuda'), dtype=torch.float64,):
    
    # calibrate probabilities
    for i in range(NoJets_classifier):
        with open(Calibration_dir + f'/isoRegr_jet{i}.pkl', 'rb') as f:
            temperature_loaded = pickle.load(f)
        calibrated_LogProbs = torch.log(torch.tensor(temperature_loaded.transform(torch.flatten(torch.exp(log_prob_classifier_perObj[:,:,i].cpu())))))
        log_prob_classifier_perObj[:,:,i] = torch.reshape(calibrated_LogProbs, log_prob_classifier_perObj[:,:,i].shape).to(device)

    # compute TF_perEv
    TF_perEv = torch.zeros((log_prob_kin_perObj.shape[:2]), device=device, dtype=dtype)
    
    for i in range(NoJets_total):
        if i >= No_maxJets_cut:
            break
        if i < NoJets_total - NoJets_classifier:
            TF_perEv += torch.log(1. / (std_pt_Jets * std_eta_Jets) / unscaled_jetsPt[:,i,None]) + log_prob_kin_perObj[:,:,i] # always exist --> divide by pt of jet

        else:
            # sum is performed inside torch.where()
            epsilon = 1e-4
            
            mask_min = torch.isinf(log_prob_classifier_perObj[:,:,i - NoJets_total + NoJets_classifier])
            log_prob_classifier_perObj[:,:,i - NoJets_total + NoJets_classifier][mask_min] = -100
            
            mask_max = log_prob_classifier_perObj[:,:,i - NoJets_total + NoJets_classifier] == 0.0
            log_prob_classifier_perObj[:,:,i - NoJets_total + NoJets_classifier][mask_max] = 0.0 - epsilon
        
            TF_perEv = torch.where(mask_ExistReco[...,i],
                                   TF_perEv + torch.log(1. / (std_pt_Jets * std_eta_Jets) / unscaled_jetsPt[:,i,None]) + log_prob_kin_perObj[:,:,i] + log_prob_classifier_perObj[:,:,i - NoJets_total + NoJets_classifier],
                                   TF_perEv + torch.log(1 - torch.exp(log_prob_classifier_perObj[:,:,i - NoJets_total + NoJets_classifier])))
        

    return torch.exp(TF_perEv)

    
    

def attach_position(input_tensor, position, dim=2):
    position = position.expand(input_tensor.shape[0], -1, -1)
    input_tensor = torch.cat((input_tensor, position), dim=dim)
    
    return input_tensor

def ddp_setup(rank, world_size, port):
    """
    Args:
        rank: Unique identifier of each process
        world_size: Total number of processes
    """
    import socket
    print("Setting up ddp for device: ", rank)
    os.environ["MASTER_ADDR"] = socket.gethostname()
    os.environ["MASTER_PORT"] = f"{port}"
    init_process_group(backend="nccl", rank=rank, world_size=world_size)
    torch.cuda.set_device(rank)




def train(device, path_to_dir, path_to_model, path_to_transferFlow, path_calibration, config, config_transferFlow, dtype, validation, test, chunk,
           world_size=None, device_ids=None):
    # device is device when not distributed and rank when distributed
    print("START OF RANK:", device)
    if world_size is not None:
        ddp_setup(device, world_size, config.ddp_port)

    device_id = device_ids[device] if device_ids is not None else device

    
    if validation:
        print('use validation dataset')
        dataset = config.input_dataset_validation
    if test:
        print('use test dataset')
        dataset = config.input_dataset_test


    else:
        print('use eft dataset')
        dataset = config.input_dataset_EFT

    #test_dataset = DatasetCombined(dataset, dev=device,
    test_dataset = DatasetCombined(dataset, dev=torch.device('cpu'),
                                    dtype=dtype, datasets=['partons_lab', 'reco_lab'],
                                   reco_list_lab=['scaledLogReco_sortedBySpanet',
                                                  'mask_scaledLogReco_sortedBySpanet',
                                                  'mask_boost', 'scaledLogBoost'],
                                   parton_list_lab=['logScaled_data_higgs_t_tbar_ISR',
                                                   'lepton_partons_pdgID'])


    #no_recoObjs = test_dataset.reco_lab.scaledLogReco_sortedBySpanet.shape[1]
    no_recoObjs = 9


    log_mean_reco_spanet = test_dataset.reco_lab.meanRecoParticles
    log_std_reco_spanet = test_dataset.reco_lab.stdRecoParticles

    # VAlues from test dataset
    log_mean_parton_Hthad = torch.Tensor([4.6722, -6.6288e-03,  1.3556e-03])
    log_std_parton_Hthad = torch.Tensor([0.9196, 1.5784, 1.8139])
    log_mean_boost_parton = torch.Tensor([7.1853, -6.9543])
    log_std_boost_parton = torch.Tensor([4.1579e-01, 1.0232e+03])
    
    mean_ps = torch.Tensor([2.4564e+00,  1.3425e+00, -4.6606e-03,  7.8860e-04, -2.1456e-03,
         2.7660e-04, -6.6241e-03,  5.0267e-03,  4.5121e-01,  5.0199e-01])
    
    scale_ps = torch.Tensor([9.8929, 10.0761, 10.1887,  9.0733, 10.9712,  9.0767, 13.9509,  9.0681,
         3.5302,  3.6533])
    
    if device == torch.device('cuda'):
        log_mean_parton_Hthad = log_mean_parton_Hthad.cuda()
        log_std_parton_Hthad = log_std_parton_Hthad.cuda()
        log_mean_boost_parton = log_mean_boost_parton.cuda()
        log_std_boost_parton = log_std_boost_parton.cuda()

        mean_ps = mean_ps.cuda()
        scale_ps = scale_ps.cuda()

        log_mean_reco_spanet = log_mean_reco_spanet.cuda()
        log_std_reco_spanet = log_std_reco_spanet.cuda()

    # Initialize model
    model = UnfoldingFlow_v2_onlyPropag(scaling_partons_CM_ps=[mean_ps, scale_ps],

                                 regression_no_boostVars=2,
                                 regression_hidden_features=config.conditioning_transformer.hidden_features,
                                 regression_DNN_input=config.conditioning_transformer.hidden_features + 1,
                                 regression_dim_feedforward=config.conditioning_transformer.dim_feedforward_transformer,
                                 regression_nhead_encoder=config.conditioning_transformer.nhead_encoder,
                                 regression_noLayers_encoder=config.conditioning_transformer.no_layers_encoder,
                                 regression_noLayers_decoder=config.conditioning_transformer.no_layers_decoder,
                                 regression_DNN_layers=config.conditioning_transformer.DNN_layers,
                                 regression_DNN_nodes=config.conditioning_transformer.DNN_nodes,
                                 regression_aggregate=config.conditioning_transformer.aggregate,
                                 regression_atanh=True,
                                 regression_angles_CM=True,
                                 
                                 flow_nfeatures=config.unfolding_flow.nfeatures,
                                 flow_ncond=config.unfolding_flow.ncond, 
                                 flow_ntransforms=config.unfolding_flow.ntransforms,
                                 flow_hiddenMLP_NoLayers=config.unfolding_flow.hiddenMLP_NoLayers,
                                 flow_hiddenMLP_LayerDim=config.unfolding_flow.hiddenMLP_LayerDim,
                                 flow_bins=config.unfolding_flow.bins,
                                 flow_autoregressive=config.unfolding_flow.autoregressive, 
                                 flow_base=config.unfolding_flow.base,
                                 flow_base_first_arg=config.unfolding_flow.base_first_arg,
                                 flow_base_second_arg=config.unfolding_flow.base_second_arg,
                                 flow_bound=config.unfolding_flow.bound,
                                 randPerm=True,

                                 DNN_condition=False,
                                 DNN_layers=2,
                                 DNN_dim=256,
                                 DNN_output_dim=3,
                                 
                                 device=device,
                                 dtype=dtype,
                                 pretrained_model='a',
                                 load_conditioning_model=False)

    # Setting up DDP
    model = model.to(device)

    state_dict = torch.load(path_to_model, map_location=device)
    model.load_state_dict(state_dict['model_state_dict'])


    # Initialize model
    transfer_flow = TransferFlow_Paper_AllPartons_Nobtag_autoreg_latentSpace_gaussian_v3(no_recoVars=5, # exist + 3-mom + encoded_position,
                                 no_partonVars=4,
                                 no_recoObjects=no_recoObjs,
                
                                 no_transformers=config_transferFlow.transformerConditioning.no_transformers,
                                 transformer_input_features=config_transferFlow.transformerConditioning.input_features,
                                 transformer_nhead=config_transferFlow.transformerConditioning.nhead,
                                 transformer_num_encoder_layers=config_transferFlow.transformerConditioning.no_encoder_layers,
                                 transformer_num_decoder_layers=config_transferFlow.transformerConditioning.no_decoder_layers,
                                 transformer_dim_feedforward=config_transferFlow.transformerConditioning.dim_feedforward,
                                 transformer_activation=nn.GELU(),

                                 flow_lepton_hiddenMLP_NoLayers=config_transferFlow.transferFlow_lepton.hiddenMLP_NoLayers,
                                 flow_lepton_hiddenMLP_LayerDim=config_transferFlow.transferFlow_lepton.hiddenMLP_LayerDim,
                                 flow_lepton_bins=config_transferFlow.transferFlow_lepton.bins,
                                     
                                 flow_nfeatures=1, #pt/eta/phi
                                 flow_ntransforms=config_transferFlow.transferFlow.ntransforms,
                                 flow_hiddenMLP_NoLayers=config_transferFlow.transferFlow.hiddenMLP_NoLayers,
                                 flow_hiddenMLP_LayerDim=config_transferFlow.transferFlow.hiddenMLP_LayerDim,
                                 flow_bins=config_transferFlow.transferFlow.bins,
                                 flow_autoregressive=config_transferFlow.transferFlow.autoregressive,
                                 flow_base=config_transferFlow.transferFlow.base,
                                 flow_base_first_arg=config_transferFlow.transferFlow.base_first_arg,
                                 flow_base_second_arg=config_transferFlow.transferFlow.base_second_arg,
                                 flow_bound=config_transferFlow.transferFlow.bound,
                                 randPerm=config_transferFlow.transferFlow.randPerm,
                                 no_max_objects=config_transferFlow.transferFlow.no_max_objects,
                
                                 DNN_nodes=config_transferFlow.DNN.nodes, DNN_layers=config_transferFlow.DNN.layers,
                                 pretrained_classifier=config_transferFlow.DNN.path_pretraining,
                                 dropout=False, # to be modified with dropout or not
                                 load_classifier=False,
                                 encode_position=True,
                                 
                                 device=device,
                                 dtype=dtype,
                                 eps=1e-4)

    # Setting up DDP
    transfer_flow = transfer_flow.to(device)

    state_dict = torch.load(path_to_transferFlow, map_location=device)
    transfer_flow.load_state_dict(state_dict['model_state_dict'])

    

    if world_size is not None:
        ddp_model = DDP(
            model,
            device_ids=[device],
            output_device=device,
            # find_unused_parameters=True,
        )
        #print(ddp_model)
        model = ddp_model.module
    else:
        ddp_model = model

    batch_size = 1
    total_No_samples = 1000000 # 1 million
    No_samples = 10000
    no_chunks = 1
    fileName = f'checkAlignment_noChunks:{no_chunks}_noSamples:{total_No_samples}_noBatches:{batch_size}.pt'
    
    if few_events:
        batch_size = 2
        No_samples = 20000
        fileName = 'sample_few_events_EFT_TF.pt'

    if validation:
        fileName = 'valid.pt'
    if test:
        print('TEST DATASET')
        fileName = f'checkAlignment_test_noChunks:{no_chunks}_noSamples:{total_No_samples}_noBatches:{batch_size}.pt'
    
    # Datasets
    testLoader = DataLoader(
        test_dataset,
        batch_size= batch_size,
        shuffle=False,
        sampler=DistributedSampler(test_dataset) if world_size is not None else None,
        drop_last=False
    )
    
    # attach one-hot encoded position for jets
    pos_jets_lepton_MET = [pos for pos in range(8)] # 6 jets + lepton + MET
    pos_other_jets = [8 for pos in range(no_recoObjs - 8)]
    
    pos_jets_lepton_MET = torch.tensor(pos_jets_lepton_MET, device=device, dtype=dtype)
    pos_other_jets = torch.tensor(pos_other_jets, device=device, dtype=dtype)
    pos_logScaledReco = torch.cat((pos_jets_lepton_MET, pos_other_jets), dim=0).unsqueeze(dim=1)

    # attach one-hot encoded position for partons
    pos_partons = torch.tensor([pos for pos in range(4)], device=device, dtype=dtype)[:,None] # higgs, t1, t2, ISR

    No_regressed_vars = 4
    
    # new order: lepton MET higgs1 higgs2 etc
    new_order_list = [6,7,0,1,2,3,4,5]
    lastElems = [i+8 for i in range(no_recoObjs - 8)]
    new_order_list = new_order_list + lastElems
    new_order = torch.LongTensor(new_order_list)

    partons_order = [0,1,2,3]

    partons_name = ['higgs', 'thad', 'tlep', 'ISR']
    partons_var = ['pt','eta','phi']
    boost_var = ['E', 'pz']

    E_CM = 13000
    rambo = PhaseSpace(E_CM, [21,21], [25,6,-6,21], dev=device) 
    
    posLogParton = torch.linspace(0, 1, No_regressed_vars, device=device, dtype=dtype)

    mask_partons = torch.ones((4, 1), device=device, dtype=dtype)

    E_CM = 13000
    
    phasespace = PhaseSpace(E_CM, [21,21], [25,6,-6, 21])

    with torch.no_grad():
        ps_rand, momenta, weight, x1, x2 =  phasespace.generate_random_phase_space_points(No_samples)
        momenta = momenta[:,2:]
        full_likelihood_unfolding = torch.empty((no_chunks * batch_size, total_No_samples), device=torch.device('cuda'), dtype=dtype)
        full_TF = torch.empty((no_chunks * batch_size, total_No_samples), device=torch.device('cuda'), dtype=dtype) # the last element is the output of TF

    print("Before sampling loop")
    model.eval()
    ddp_model.eval()
    transfer_flow.eval()

    with torch.no_grad():

        for index_samples in range(floor(total_No_samples / No_samples)):
    
            event_start = 0
            finish_flag = 0
            samples_start = index_samples * No_samples  # + 1 because on first position we have the target
            samples_end = (index_samples + 1) * No_samples  # + 1 because on first position we have the target
    
            
    
            # get good events: to have the tagged jets in the first 9 positions (not necessary assigned correctly by spanet)
            mask_goodEvents = torch.count_nonzero(test_dataset.reco_lab.scaledLogReco_sortedBySpanet[:,:9,-2] > 0, dim=1) == 7
    
            charge_leptons = test_dataset.partons_lab.lepton_partons_pdgID[mask_goodEvents][:batch_size].to(device)
            logScaled_reco_sortedBySpanet = test_dataset.reco_lab.scaledLogReco_sortedBySpanet[mask_goodEvents][:batch_size].to(device)
            mask_recoParticles = test_dataset.reco_lab.mask_scaledLogReco_sortedBySpanet[mask_goodEvents][:batch_size].bool().to(device)
            mask_boost_reco = test_dataset.reco_lab.mask_boost[mask_goodEvents][:batch_size].to(device)
            data_boost_reco = test_dataset.reco_lab.scaledLogBoost[mask_goodEvents][:batch_size].to(device)
    
            negative_charge_1 = charge_leptons[:,0] == 13
            negative_charge_2 = charge_leptons[:,0] == 11
            positive_charge_1 = charge_leptons[:,0] == -13
            positive_charge_2 = charge_leptons[:,0] == -11
            
            negative_charge = torch.logical_or(negative_charge_1, negative_charge_2)
            positive_charge = torch.logical_or(positive_charge_1, positive_charge_2)
    
             # exist + 3-mom
            logScaled_reco_sortedBySpanet = logScaled_reco_sortedBySpanet[:,:,[0,1,2,3]]
            # The provenance is remove in the model
            # put the lepton first:
            logScaled_reco_sortedBySpanet = logScaled_reco_sortedBySpanet[:,new_order,:]
            mask_recoParticles = mask_recoParticles[:,new_order]
            # attach 1 hot-encoded position
            logScaled_reco_sortedBySpanet = attach_position(logScaled_reco_sortedBySpanet, pos_logScaledReco)

            # scaledJets[...,0] = existence flag --> 1 is the pt
            pt_unscaledJets = torch.exp(logScaled_reco_sortedBySpanet[:,:,1] * log_std_reco_spanet[0] + log_mean_reco_spanet[0]) - 1
    
            x1_iter = x1[samples_start:samples_end]
            x2_iter = x2[samples_start:samples_end]
            momenta_iter = momenta[samples_start:samples_end]
    
            zeros_pxpy = torch.zeros((x1_iter.shape), device=model.device, dtype=model.dtype)
            boost_sampled_Epz_unscaled = torch.stack((rambo.collider_energy*(x1_iter+x2_iter)/2, zeros_pxpy, zeros_pxpy, rambo.collider_energy*(x1_iter-x2_iter)/2), dim=1)
    
            boost_vectors_B  = ps_utils.boostVector_t(boost_sampled_Epz_unscaled).unsqueeze(dim=1)  
    
            # + here not '-'
            propagators_unscaled_cartesian_lab_sampled = ps_utils.boost_tt(momenta_iter , boost_vectors_B) #
    
            # get pt/eta/phi components and scale it and take the log for H/thad/tlep/ISR
            H_thad_tlep_ISR_sampled_unscaled_Eptetaphi_lab = Compute_ParticlesTensor.get_ptetaphi_comp_batch(propagators_unscaled_cartesian_lab_sampled)
            H_thad_tlep_ISR_sampled_scaled_ptetaphi_lab = H_thad_tlep_ISR_sampled_unscaled_Eptetaphi_lab[...,1:].clone() 
            H_thad_tlep_ISR_sampled_scaled_ptetaphi_lab[...,0] = torch.log(H_thad_tlep_ISR_sampled_unscaled_Eptetaphi_lab[...,1] + 1)
            H_thad_tlep_ISR_sampled_scaled_ptetaphi_lab[...,0:2] = (H_thad_tlep_ISR_sampled_scaled_ptetaphi_lab[...,0:2] - log_mean_reco_spanet[:2])/log_std_reco_spanet[:2]

            H_thad_tlep_ISR_sampled_scaled_ptetaphi_lab = attach_position(H_thad_tlep_ISR_sampled_scaled_ptetaphi_lab, pos_partons, dim=2)
            
            logScaled_reco_sortedBySpanet = torch.repeat_interleave(logScaled_reco_sortedBySpanet, No_samples, dim=0)
            mask_recoParticles = torch.repeat_interleave(mask_recoParticles, No_samples, dim=0)

            # GET TF KIN AND EXIST
            log_prob_TF_kin, log_prob_TF_exist = transfer_flow.inference(logScaled_reco_sortedBySpanet, H_thad_tlep_ISR_sampled_scaled_ptetaphi_lab,
                                                                                data_boost_reco, mask_partons[None,...],
                                                                                mask_recoParticles, mask_boost_reco)


            log_prob_TF_kin = torch.reshape(log_prob_TF_kin, (batch_size, No_samples, 9))
            log_prob_TF_exist = torch.reshape(log_prob_TF_exist, (batch_size, No_samples, 5))

            # COMPUTE FULL TF
            full_TF[:, samples_start:samples_end] = compute_fullTF(log_prob_TF_exist, log_prob_TF_kin, mask_recoParticles,
                                                           NoJets_classifier=5, NoJets_total=9, No_maxJets_cut=100,
                                                           Calibration_dir=path_calibration, unscaled_jetsPt=pt_unscaledJets,
                                                            std_pt_Jets=log_std_reco_spanet[0], std_eta_Jets=log_std_reco_spanet[1],
                                                            device=device, dtype=dtype)
    
            print(H_thad_tlep_ISR_sampled_scaled_ptetaphi_lab[0:2])
            exit(0)
    
            
    
            print(f'index_samples = {index_samples}')
        
            for i, data_batch in enumerate(testLoader):
                # Move data to device
                with torch.no_grad():
    
                    (logScaled_partons, charge_leptons,
                    logScaled_reco_sortedBySpanet, mask_recoParticles,
                    mask_boost_reco, data_boost_reco) = data_batch
                    
                    negative_charge_1 = charge_leptons[:,0] == 13
                    negative_charge_2 = charge_leptons[:,0] == 11
                    positive_charge_1 = charge_leptons[:,0] == -13
                    positive_charge_2 = charge_leptons[:,0] == -11
                    
                    negative_charge = torch.logical_or(negative_charge_1, negative_charge_2)
                    positive_charge = torch.logical_or(positive_charge_1, positive_charge_2)
    
                    if torch.count_nonzero(negative_charge).item() + torch.count_nonzero(positive_charge).item() != charge_leptons.shape[0]:
                        raise Exception('Not only electrons and muons on first position')
    
    
                    
        
                    if (i % 10 == 0):
                        print(f'batch {i} from {len(testLoader)}')                
    
                    
                    
    
                    
                    exit(0)
    
                    # compute TF
                    
    
                    
                    
    
                    if index_samples == 0: # compute the regression only for the first model
            
                        # same order for partons:
                        logScaled_partons = logScaled_partons[:,partons_order,:]
            
                        # remove prov from partons
                        logScaled_partons = logScaled_partons[:,:,[0,1,2]] # [pt,eta,phi,parton_id, type] -> skip type=1/2 for partons/leptons
    
                        jets_distrib[event_start:event_end] = logScaled_reco_sortedBySpanet[...,:4]                    
                                
                        flow_cond_vector, \
                        sampled_pdfHthadtlepISR_CM_pxpypz, \
                        H_thad_tlep_ISR_sampled_scaled_ptetaphi_lab, \
                        flow_x1_x2, \
                        log_prob_samples, rambo_weight, sigmoid_jacobian  = model.sample_EFT(logScaled_reco_sortedBySpanet[...,[0,1,2,3]],
                                                                                             data_boost_reco,
                                                                                             mask_recoParticles, mask_boost_reco,
                                                                                                                                                    
                                                                                            log_mean_boost_parton=log_mean_boost_parton,
                                                                                            log_std_boost_parton=log_std_boost_parton,
                                                                                            log_mean_parton_Hthad=log_mean_parton_Hthad,
                                                                                            log_std_parton_Hthad=log_std_parton_Hthad,
                                                                                                             
                                                                                            order=[0,1,2,3],
                                                                                            disableGradConditioning=True,
                                                                                            flow_eval="both",
                                                                                            Nsamples=No_samples, No_regressed_vars=No_regressed_vars,
                                                                                            sin_cos_embedding=True, sin_cos_reco=pos_logScaledReco_sincos,
                                                                                            sin_cos_partons=pos_partons_sincos,
                                                                                            attach_position_regression=posLogParton,
                                                                                            rambo=rambo)
    
                        # without phi
                        unscaledTarget = logScaled_partons[...,0:3]
                        unscaledTarget[...,[0,1]] = (unscaledTarget[...,[0,1]]*log_std_parton_Hthad[:2] + log_mean_parton_Hthad[:2])
                        unscaledTarget[...,0] = torch.exp(unscaledTarget[...,0]) - 1 # unscale pt
            
                        higgs_target_cartesian = Compute_ParticlesTensor.get_cartesian_comp(unscaledTarget[:,0], mass=125.25)
                        thad_target_cartesian = Compute_ParticlesTensor.get_cartesian_comp(unscaledTarget[:,1], mass=172.5)
                        tlep_target_cartesian = Compute_ParticlesTensor.get_cartesian_comp(unscaledTarget[:,2], mass=172.5)
                        ISR_target_cartesian = Compute_ParticlesTensor.get_cartesian_comp(unscaledTarget[:,3], mass=0.0)
                        padding_x1x2 = -1*torch.ones((ISR_target_cartesian.shape[0], 1, 2, 4), device=device, dtype=dtype)
    
                        target_event = torch.cat((padding_x1x2, higgs_target_cartesian[:,None,None], thad_target_cartesian[:,None,None],
                                              tlep_target_cartesian[:,None,None], ISR_target_cartesian[:,None,None]),
                                               dim=2)
    
                        unscaled_sampledTensor[event_start:event_end, 0:1] = torch.where(negative_charge[:, None, None, None],
                                                                                        target_event[:,:,[0,1,3,4,2,5]],
                                                                                        target_event[:,:,[0,1,4,3,2,5]])
    
                        full_flow_cond_vector[event_start:event_end] = flow_cond_vector
                        x1_x2_samples[event_start:event_end, 0:1] = padding_x1x2[:,:,0,:2]
                        mask_ExistReco[event_start:event_end] = logScaled_reco_sortedBySpanet[:,:no_recoObjs,0]
    
                    else:
                        sampled_pdfHthadtlepISR_CM_pxpypz, \
                        H_thad_tlep_ISR_sampled_scaled_ptetaphi_lab, \
                        flow_x1_x2, \
                        log_prob_samples, rambo_weight, sigmoid_jacobian  = model.sample_EFT_noRegr(full_flow_cond_vector[event_start: event_end],
                                                                                                    log_mean_parton_Hthad=log_mean_parton_Hthad,
                                                                                                    log_std_parton_Hthad=log_std_parton_Hthad,
                                                                                                    No_samples = No_samples,
                                                                                                    rambo=rambo)
    
    
                    H_thad_tlep_ISR_sampled_scaled_ptetaphi_lab = attach_position(H_thad_tlep_ISR_sampled_scaled_ptetaphi_lab, pos_partons, dim=2)
            
                    # TODO: check this repeat is correct --> such that it is alligned with the flatten
                    # seems this version is correct: logScaled_reco_sortedBySpanet[0] = logScaled_reco_sortedBySpanet[1] = ... = logScaled_reco_sortedBySpanet[No_samples - 1]
                    logScaled_reco_sortedBySpanet = torch.repeat_interleave(logScaled_reco_sortedBySpanet, No_samples, dim=0)
                    mask_recoParticles = torch.repeat_interleave(mask_recoParticles, No_samples, dim=0)
    
                    
    
                    no_ev = event_end - event_start
                    fullGeneratedEvent = torch.reshape(sampled_pdfHthadtlepISR_CM_pxpypz, (no_ev, No_samples, 6, 4))
                    rambo_weight = torch.reshape(rambo_weight, (no_ev, No_samples, 1))
                    sigmoid_jacobian = torch.reshape(sigmoid_jacobian, (no_ev, No_samples, 1))
                    log_prob_samples = torch.reshape(log_prob_samples, (no_ev, No_samples, 1))
                    flow_x1_x2 = torch.reshape(flow_x1_x2, (no_ev, No_samples, 2))
    
                    # TODO: since I used everything as before --> I expect this reshape is working fine
                    log_prob_TF_kin = torch.reshape(log_prob_TF_kin, (no_ev, No_samples, no_recoObjs))
                    log_prob_TF_exist = torch.reshape(log_prob_TF_exist, (no_ev, No_samples, no_recoObjs - 4))
        
                    sigmoid_jacobian = sigmoid_jacobian * torch.prod(scale_ps) # jacobian of scaling = constant
                    log_prob_samples = torch.exp(log_prob_samples) # from log space to normal space
        
                    probabilities = torch.cat((log_prob_samples, sigmoid_jacobian, rambo_weight, log_prob_TF_kin, log_prob_TF_exist), dim=-1)
                    
                    unscaled_sampledTensor[event_start:event_end, samples_start:samples_end] = torch.where(negative_charge[:, None, None, None],
                                                                                                            fullGeneratedEvent[:,:,[0,1,3,4,2,5]],
                                                                                                            fullGeneratedEvent[:,:,[0,1,4,3,2,5]])
                    
                    full_log_prob_samples[event_start:event_end, samples_start:samples_end] = probabilities
    
                    x1_x2_samples[event_start:event_end, samples_start:samples_end] = flow_x1_x2
    
                    event_start = event_end
    
                    if finish_flag:
                        break


    new_path_TF = '/'.join(path_to_transferFlow.split('/')[-3:-1])
    print(new_path_TF)
    if not os.path.exists(f'{path_to_dir}/{new_path_TF}'):
        os.makedirs(f'{path_to_dir}/{new_path_TF}')
    torch.save((jets_distrib, unscaled_sampledTensor, full_flow_cond_vector, full_log_prob_samples, x1_x2_samples, mask_ExistReco), f'{path_to_dir}/{new_path_TF}/{fileName}') # todo: save good files
    # exp_log.end()
    destroy_process_group()
    print('Sampling finished!!')
        

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--path-model', type=str, required=True, help='path to model directory')
    parser.add_argument('--path-transfer-flow', type=str, required=True, help='path to model directory')
    parser.add_argument('--chunk', type=int, default=0, help='chunk number')
    parser.add_argument('--few-events',action="store_true",  help='sample 20k reco events for only 50 gen-level events')
    parser.add_argument('--on-GPU', action="store_true",  help='run on GPU boolean')
    parser.add_argument('--validation', action="store_true",  help='use validation dataset')
    parser.add_argument('--test', action="store_true",  help='use test dataset')
    parser.add_argument('--distributed', action="store_true")
    args = parser.parse_args()
    
    path_to_dir = args.path_model
    path_to_dir_transferFlow = args.path_transfer_flow
    chunk = args.chunk
    few_events = args.few_events
    on_GPU = args.on_GPU # by default run on CPU
    validation = args.validation
    test = args.test

    path_to_conf = path_to_dir + '/config_spanet_labframe_v2-gluon-dist-redo5.yaml'
    path_to_model = path_to_dir + '/model_spanet_labframe_v2-gluon-dist-redo5.pt'
    path_to_conf_transferFlow = path_to_dir_transferFlow + '/config_transfer_flow_2nd_2nd.yaml'
    path_to_transferFlow = path_to_dir_transferFlow + '/model_transfer_flow_2nd_2nd.pt'
    path_to_calibDir = path_to_dir_transferFlow + '/calibration'

    # Read config file in 'conf'
    with open(path_to_conf) as f:
        conf = OmegaConf.load(path_to_conf)

    # Read config file in 'conf'
    with open(path_to_conf_transferFlow) as f:
        conf_transferFlow = OmegaConf.load(path_to_conf_transferFlow)
    
    print("Training with cfg: \n", OmegaConf.to_yaml(conf))

    env_var = os.environ.get("CUDA_VISIBLE_DEVICES")
    if env_var:
        actual_devices = env_var.split(",")
    else:
        actual_devices = list(range(torch.cuda.device_count()))
    print("Actual devices: ", actual_devices)
    world_size = len(actual_devices)

    if conf.training_params.dtype == "float32":
        dtype = torch.float32
    elif conf.training_params.dtype == "float64":
        dtype = torch.float64
    else:
        dtype = None
    
    if len(actual_devices) > 1 and args.distributed:
        # make a dictionary with k: rank, v: actual device
        dev_dct = {i: actual_devices[i] for i in range(world_size)}
        print(f"Devices dict: {dev_dct}")
        mp.spawn(
            train,
            args=(device, path_to_dir, path_to_model, path_to_transferFlow, path_to_calibDir, conf, conf_transferFlow, dtype, validation, test, chunk,
                    world_size, dev_dct),
            nprocs=world_size,
            # join=True
        )
    else:
        device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
        train(device, path_to_dir, path_to_model, path_to_transferFlow, path_to_calibDir, conf, conf_transferFlow, dtype, validation, test, chunk, None, None)
    
    print(f"Sampled succesfully! Version: {conf.version}")
    
    
    
    
    
    
    
    
    
