import sys
import os

import pickle
import torch.nn as nn
import torch
import numpy as np
from memflow.mem_computation_noExist.mem_computation import *
from memflow.mem_computation_noExist.mem_plots import *


from pdfflow import mkPDFs
import tensorflow as tf
import importlib.util

import argparse

def get_ptetaphi_comp_batch(particles):
    #print(particles.shape)
    particle_pt = (particles[...,1]**2 + particles[...,2]**2)**0.5
    particle_eta = -torch.log(torch.tan(torch.atan2(particle_pt, particles[...,3])/2))
    particle_phi = torch.atan2(particles[...,2], particles[...,1])   
    return torch.stack((particles[...,0], particle_pt, particle_eta, particle_phi), dim=3) # E/pt/eta/phi

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--path-file', type=str, required=True, help='path to model directory')
    parser.add_argument('--path-outDir', type=str, required=True, help='path to output directory')
    parser.add_argument('--path-calib', type=str, required=True, help='path to calib directory')
    parser.add_argument('--events', nargs="+", type=int, help='list of events')
    parser.add_argument('--SM', action="store_true",  help='ME: SM')
    parser.add_argument('--lin', action="store_true",  help='ME: lin')
    parser.add_argument('--quad', action="store_true",  help='ME: quad')
    parser.add_argument('--on-GPU', action="store_true",  help='run on GPU boolean')
    args = parser.parse_args()
    
    path_to_file = args.path_file
    on_GPU = args.on_GPU # by default run on CPU
    path_out_dir = args.path_outDir
    calib_dir = args.path_calib
    events = args.events

    device = torch.device("cuda") if torch.cuda.is_available() and on_GPU else torch.device("cpu")

    print(f"Arguments were passed")

    cwd = os.getcwd() # current directory
    path_lin = '/afs/cern.ch/user/a/adpetre/public/memflow/MEMFlow/madgraph/ttHbb-p1j-signal-SMEFTsim-topU3l-5F-tbarqqtlnu-madspin_EFTcenter_gridpack/work/ttHbb-p1j-lin-ctHre-converted-final2/'
    path_quad = '/afs/cern.ch/user/a/adpetre/public/memflow/MEMFlow/madgraph/ttHbb-p1j-signal-SMEFTsim-topU3l-5F-tbarqqtlnu-madspin_EFTcenter_gridpack/work/ttHbb-p1j-quad-ctHre-converted-final2/'
    path_SM = '/afs/cern.ch/user/a/adpetre/public/memflow/MEMFlow/madgraph/ttHbb-p1j-signal-SMEFTsim-topU3l-5F-tbarqqtlnu-madspin_EFTcenter_gridpack/work/ttHbb-p1j-SM-converted-final2/'

    sys.path.append(path_lin + 'python/')
    sys.path.append(path_quad + 'python/')
    sys.path.append(path_SM + 'python/')

    path_ME_evaluation = []
    evaluate_ME = []
    objects_ME_evaluation = []

    if args.SM:
        spec_SM = importlib.util.spec_from_file_location("mem_eval_old", path_SM + "python/mem_eval_old.so")
        mem_eval_SM = importlib.util.module_from_spec(spec_SM)
        sys.modules["mem_eval_old"] = mem_eval_SM
        spec_SM.loader.exec_module(mem_eval_SM)
        
        path_ME_evaluation.append( path_SM + 'python/' )
        evaluate_ME.append('SM')
        objects_ME_evaluation.append( mem_eval_SM )

    if args.lin:
        spec_lin = importlib.util.spec_from_file_location("mem_eval_old", path_lin + "python/mem_eval_old.so")
        mem_eval_lin = importlib.util.module_from_spec(spec_lin)
        sys.modules["mem_eval_old"] = mem_eval_lin
        spec_lin.loader.exec_module(mem_eval_lin)
        
        path_ME_evaluation.append( path_lin + 'python/' )
        evaluate_ME.append('lin')
        objects_ME_evaluation.append( mem_eval_lin )

    if args.quad:
        spec_quad = importlib.util.spec_from_file_location("mem_eval", path_quad + "python/mem_eval.so")
        mem_eval_quad = importlib.util.module_from_spec(spec_quad)
        sys.modules["mem_eval"] = mem_eval_quad
        spec_quad.loader.exec_module(mem_eval_quad)
        
        path_ME_evaluation.append( path_quad + 'python/' )
        evaluate_ME.append('quad')
        objects_ME_evaluation.append( mem_eval_quad )

    if len(objects_ME_evaluation) == 0:
        raise Exception ('No ME provided: SM/lin/quad')


    scaled_jets, unscaled_partons, full_flow_cond_vector, full_log_prob_samples, x1_x2_samples, mask_ExistReco = torch.load(path_to_file, map_location=device)

    if events != None:
        scaled_jets = scaled_jets[events]
        unscaled_partons = unscaled_partons[events]
        full_log_prob_samples = full_log_prob_samples[events]
        x1_x2_samples = x1_x2_samples[events]
        mask_ExistReco = mask_ExistReco[events]
    else:
        events = [i for i in range(scaled_jets.shape[0])]

    unscaled_partons_ptetaphi = get_ptetaphi_comp_batch(unscaled_partons[:,:,2:])
    jacobians = full_log_prob_samples[:,1:,:3].numpy()

    #print(test_dataset.reco_lab.meanRecoParticles) --> need these values
    #print(test_dataset.reco_lab.stdRecoParticles) --> need these values
    meanRecoParticles = [ 4.3282e+00, -3.5510e-03,  1.0714e-02]
    stdRecoParticles = [0.5821, 0.9797, 1.8153]

    # scaledJets[...,0] = existence flag --> 1 is the pt
    pt_unscaledJets = torch.exp(scaled_jets[:,:,0] * stdRecoParticles[0] + meanRecoParticles[0]) - 1

    #unscaled_jets = scaled_jets.clone()
    #unscaled_jets[:,:,1:3] = unscaled_jets[:,:,1:3] * torch.tensor(stdRecoParticles[:2]) + torch.tensor(meanRecoParticles[:2])
    #unscaled_jets[:,:,1] = torch.exp(unscaled_jets[:,:,1]) - 1

    #pt_unscaledJets = unscaled_jets[:,:,1]

    flavours = [[21, 21], [1, -1], [2, -2], [3, -3], [4, -4], [5, -5], [-1, 1], [-2, 2], [-3, 3], [-4, 4], [-5, 5], [1, 21], [2, 21], [3, 21], \
            [4, 21], [5, 21], [21, 1], [21, 2], [21, 3], [21, 4], [21, 5], [-1, 21], [-2, 21], [-3, 21], [-4, 21], [-5, 21], [21, -1], \
            [21, -2], [21, -3], [21, -4], [21, -5]]


    out = mem_computation(unscaled_jetsPt=pt_unscaledJets,
                    unscaled_PartonsMomenta=unscaled_partons, LogProbSamples=full_log_prob_samples,
                    x1_x2_samples=x1_x2_samples, mask_ExistReco=mask_ExistReco,
                    flavours=flavours,
                    path_ME_evaluation=path_ME_evaluation,
                    evaluate_ME=evaluate_ME,
                    objects_ME_evaluation=objects_ME_evaluation,
                    std_pt_Jets=0.5822, std_eta_Jets=0.9795,  # std for logScaled_jets_OrderBySPANET
                    scaling_jac = [ 9.8929, 10.0761, 10.1887,  9.0733, 10.9712,  9.0767, 13.9509,  9.0681, 3.5302,  3.6533], # std for logit(Rambo) transformation
                    Calibration_dir=calib_dir, NoJets_total=11, NoJets_classifier=5, No_maxJets_cut=9)

    # first element: SM, 2nd: lin, 3rd: quad
    ME_PerPartonSample_list = out[0]
    MEandPDF_PerPartonEvent_list = out[1]
    TF_perEv = out[2]
    fullSample_prob = out[3]
    MEandTF_PerPartonSample_list = out[4]
    TFandProbSample_list = out[5]
    MEM_PerPartonSample_list = out[6]
    MEM_PerRecoEvent_list = out[7]
    std_MEM_PerRecoEvent_list = out[8]

    #print()
    #print(MEM_PerRecoEvent_list[0])
    #print(std_MEM_PerRecoEvent_list[0])
    #print(std_MEM_PerRecoEvent_list[0] / MEM_PerRecoEvent_list[0])

    label_kinematics = ["pt", "eta", "phi"]
    range_kinematics = [(0,1500), (-5,5), (-3.15, 3.15)]

    No_recoEvents = ME_PerPartonSample_list[0].shape[0]

    os.chdir(cwd) # go back to the initial directory

    for i, ME_component in enumerate(evaluate_ME):
        if not os.path.exists(path_out_dir + f'/general_results/{ME_component}/'):
            os.makedirs(path_out_dir + f'/general_results/{ME_component}/')
            
        plot_MEM_fewEvents(MEM_PerRecoEvent_list[i], std_MEM_PerRecoEvent_list[i], save_dir=path_out_dir + f'/general_results/{ME_component}/')

        for j, reco_event in enumerate(events):
            if not os.path.exists(path_out_dir + f'/recoEvent_{reco_event}/{ME_component}/'):
                os.makedirs(path_out_dir + f'/recoEvent_{reco_event}/{ME_component}/')

            dir_name = path_out_dir + f'/recoEvent_{reco_event}/{ME_component}/'

            # pt eta phi
            for kinematics in range(3):
                plot_var_weight(unscaled_partons_ptetaphi, event=j, var=kinematics+1, range_var=range_kinematics[kinematics], label=label_kinematics[kinematics],
                                weights=None, figSize=15, log=True, bins=50,
                                save_dir=dir_name + f'{label_kinematics[kinematics]}.png')
                plot_var_weight(unscaled_partons_ptetaphi, event=j, var=kinematics+1, range_var=range_kinematics[kinematics], label=label_kinematics[kinematics],
                                weights=1./fullSample_prob[j], figSize=15, log=True, bins=50,
                                save_dir=dir_name + f'{label_kinematics[kinematics]}_weightProbSample.png')
                
                plot_var_weight(unscaled_partons_ptetaphi, event=j, var=kinematics+1, range_var=range_kinematics[kinematics], label=label_kinematics[kinematics],
                                weights=TF_perEv[j], figSize=15, log=True, bins=50,
                                save_dir=dir_name + f'{label_kinematics[kinematics]}_weightTF.png')
                plot_var_weight(unscaled_partons_ptetaphi, event=j, var=kinematics+1, range_var=range_kinematics[kinematics], label=label_kinematics[kinematics],
                                weights=TF_perEv[j] / fullSample_prob[j], figSize=15, log=True, bins=50,
                                save_dir=dir_name + f'{label_kinematics[kinematics]}_weightTFandProbSample.png')
                plot_var_weight(unscaled_partons_ptetaphi, event=j, var=kinematics+1, range_var=range_kinematics[kinematics], label=label_kinematics[kinematics],
                                weights=MEM_PerPartonSample_list[i][j], figSize=15, log=True, bins=50,
                                save_dir=dir_name + f'{label_kinematics[kinematics]}_weightFullMEM.png')


            hist1d_var(np.log(ME_PerPartonSample_list[i][j]), log=True, label='log(ME)', bins=100, save_dir=dir_name + '1d_logME.png')
            hist1d_var(np.log(MEandPDF_PerPartonEvent_list[i][j]), log=True, label='log(ME * pdf1 * pdf2)', bins=100, save_dir=dir_name + '1d_logMEandPDF.png')
            hist1d_var(np.log(TF_perEv[j]), log=True, label='log(TF)', bins=100, save_dir=dir_name + '1d_logTF.png')
            hist1d_var(np.log(fullSample_prob[j]), log=True, label='log(prob sample)', bins=100, save_dir=dir_name + '1d_logProbSample.png')

            hist1d_var(np.log(MEM_PerPartonSample_list[i][j]), log=True, label='log(Full MEM)', bins=100, save_dir=dir_name + '1d_logFullMEM.png')
            hist1d_var(MEM_PerPartonSample_list[i][j], log=True, label='Full MEM', bins=100, save_dir=dir_name + '1d_FullMEM.png')

            hist1d_var(np.log(jacobians[j,:,0]), log=True, label='log(prob sample flow)', bins=100, save_dir=dir_name + '1d_logJacobianFlow.png')
            hist1d_var(np.log(jacobians[j,:,1]), log=True, label='log(sigmoid (derivative))', bins=100, save_dir=dir_name + '1d_logJacobianSigmoid.png')
            hist1d_var(np.log(jacobians[j,:,2]), log=True, label='log(Rambo weight)', bins=100, save_dir=dir_name + '1d_logJacobianRambo.png')

            plot_profile1d_max(list_arrays=[np.log(ME_PerPartonSample_list[i][j]), np.log(TF_perEv[j]), np.log(fullSample_prob[j])],
                               list_labels=['ME', 'TF', 'prob sample'], log=True, bins=100, save_dir=dir_name + 'profile_1.png')
            
            plot_profile1d_max(list_arrays=[np.log(ME_PerPartonSample_list[i][j]), np.log(TFandProbSample_list[i][j])],
                               list_labels=['ME', 'TF / prob sample'], log=True, bins=100, save_dir=dir_name + 'profile_2.png')

            plot_profile1d_max(list_arrays=[np.log(MEandPDF_PerPartonEvent_list[i][j]), np.log(TFandProbSample_list[i][j])],
                               list_labels=['ME * pdf1 * pdf2', 'TF / prob sample'], log=True, bins=100, save_dir=dir_name + 'profile_3.png')

            hist2d_var(np.log(fullSample_prob[j]), np.log(ME_PerPartonSample_list[i][j]),
                       log=True, label_1='log[prob(sample)]', label_2='log(ME)', bins=100, save_dir=dir_name + '2d_MEandProb.png')
            hist2d_var(np.log(fullSample_prob[j]), np.log(TF_perEv[j]),
                       log=True, label_1='log[prob(sample)]', label_2='log(TF)', bins=100, save_dir=dir_name + '2d_TFandProb.png')
            hist2d_var(np.log(fullSample_prob[j]), np.log(MEandPDF_PerPartonEvent_list[i][j]),
                       log=True, label_1='log[prob(sample)]', label_2='log(ME * pdf1 * pdf2)', bins=100, save_dir=dir_name + '2d_MEPDFandProb.png')
            hist2d_var(np.log(fullSample_prob[j]), np.log(MEM_PerPartonSample_list[i][j]),
                       log=True, label_1='log[prob(sample)]', label_2='log(full MEM)', bins=100, save_dir=dir_name + '2d_FullMEMandProb.png')

            banana_plots(np.log(fullSample_prob[j]), np.log(TF_perEv[j] / fullSample_prob[j]), bins=80, label_1='log[prob(sample)]', label_2='log[TF / prob(sample)]',
                         save_dir=dir_name + 'banana_ProbandTF.png')
            banana_plots(np.log(fullSample_prob[j]), np.log(MEM_PerPartonSample_list[i][j]), bins=80, label_1='log[prob(sample)]', label_2='log[Full MEM]',
                         save_dir=dir_name + 'banana_ProbandFullMEM.png')
                
            
    
    print('Finished')






