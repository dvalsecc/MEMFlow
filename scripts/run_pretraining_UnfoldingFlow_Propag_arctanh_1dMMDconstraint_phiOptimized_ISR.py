from comet_ml import Experiment
from comet_ml.integration.pytorch import log_model
from random import randint

from datetime import datetime

import torch
from memflow.read_data.dataset_all import DatasetCombined

from memflow.unfolding_network.conditional_transformer_v3_OnlyPropagators import ConditioningTransformerLayer_v3_Propag
from memflow.unfolding_flow.utils import *
from memflow.unfolding_flow.mmd_loss import MMD
from memflow.unfolding_flow.utils import Compute_ParticlesTensor
from memflow.unfolding_flow.utils import Compute_ParticlesTensor as particle_tools

import numpy as np
from torch import optim
from torch.utils.data import DataLoader
import torch.nn as nn
from torch.nn.functional import normalize
from torch.optim.lr_scheduler import CosineAnnealingLR

import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from math import floor

import mdmm
# from tensorboardX import SummaryWriter
from omegaconf import OmegaConf
import sys
import argparse
import os
from pynvml import *
import vector

from utils import constrain_energy
from utils import total_mom

from memflow.phasespace.utils import *

from earlystop import EarlyStopper
from memflow.unfolding_flow.utils import Compute_ParticlesTensor

import torch.multiprocessing as mp
from torch.utils.data.distributed import DistributedSampler
from torch.nn.parallel import DistributedDataParallel as DDP
from torch.distributed import init_process_group, destroy_process_group
from torch.profiler import profile, record_function, ProfilerActivity

from random import randint
PI = torch.pi

# torch.autograd.set_detect_anomaly(True)

def ddp_setup(rank, world_size, port):
    """
    Args:
        rank: Unique identifier of each process
        world_size: Total number of processes
    """
    import socket
    print("Setting up ddp for device: ", rank)
    os.environ["MASTER_ADDR"] = socket.gethostname()
    os.environ["MASTER_PORT"] = f"{port}"
    init_process_group(backend="nccl", rank=rank, world_size=world_size)
    torch.cuda.set_device(rank)

def sinusoidal_positional_embedding(token_sequence_size, token_embedding_dim, device, n=10000.0):

    if token_embedding_dim % 2 != 0:
        raise ValueError("Sinusoidal positional embedding cannot apply to odd token embedding dim (got dim={:d})".format(token_embedding_dim))

    T = token_sequence_size
    d = token_embedding_dim #d_model=head_num*d_k, not d_q, d_k, d_v

    positions = torch.arange(0, T).unsqueeze_(1)
    embeddings = torch.zeros(T, d, device=device)

    denominators = torch.pow(n, 2*torch.arange(0, d//2)/d) # 10000^(2i/d_model), i is the index of embedding
    embeddings[:, 0::2] = torch.sin(positions/denominators) # sin(pos/10000^(2i/d_model))
    embeddings[:, 1::2] = torch.cos(positions/denominators) # cos(pos/10000^(2i/d_model))

    return embeddings

def loss_fn_periodic(inp, target, loss_fn, device, dtype):

    deltaPhi = torch.abs(target - inp)
    loss_total_1 = loss_fn(deltaPhi, torch.zeros(deltaPhi.shape, device=device, dtype=dtype))
    loss_total_2 = loss_fn(2*np.pi - deltaPhi, torch.zeros(deltaPhi.shape, device=device, dtype=dtype))
    loss_total = torch.minimum(loss_total_1, loss_total_2)
    
    return loss_total # + overflow_delta*0.5

def compute_regr_losses(MMD_input, MMD_target, boost_input, boost_target,
                        cartesian, loss_fn,
                        device, dtype, split=False):

    loss_pt = 4*loss_fn(MMD_input[...,0], MMD_target[...,0]) # reduction = none
    loss_eta = 2*loss_fn(MMD_input[...,1], MMD_target[...,1])
    loss_phi = 5*loss_fn_periodic(MMD_input[...,2], MMD_target[...,2], loss_fn, device, dtype)

    loss_boost_E = loss_fn(boost_input[...,0], boost_target[...,0])
    loss_boost_pz = loss_fn(boost_input[...,1], boost_target[...,1])

    loss_Event = torch.cat((loss_pt, loss_eta, loss_phi, loss_boost_E[:,None], loss_boost_pz[:,None]), dim=1)

    if split:
        loss_higgs = torch.sum(loss_Event[:,[0,4,8]], dim=0) # pt eta phi
        loss_thad = torch.sum(loss_Event[:,[1,5,9]], dim=0) # pt eta phi
        loss_tlep = torch.sum(loss_Event[:,[2,6,10]], dim=0) # pt eta phi
        loss_ISR = torch.sum(loss_Event[:,[3,7,11]], dim=0) # pt eta phi
        loss_boost = torch.sum(loss_Event[:,[12,13]], dim=0) # E pz
        
        return loss_higgs.mean() / MMD_input.shape[0], \
                loss_thad.mean() / MMD_input.shape[0], \
                loss_tlep.mean() / MMD_input.shape[0], \
                loss_ISR.mean() / MMD_input.shape[0], \
                loss_boost.mean() / MMD_input.shape[0]
    else:
        loss = torch.sum(loss_Event, dim=1).mean()
        return loss


def compute_mmd_loss(mmd_input, mmd_target, kernel, device, dtype, total=False, split=False):
    mmds = []
    for particle in range(len(mmd_input)):
        for feature in range(mmd_input[particle].shape[-1]):
            mmds.append(MMD(mmd_input[particle][...,feature:feature+1], mmd_target[particle][...,feature:feature+1], kernel, device, dtype))
    # total MMD
    if total:
        mmds.append(MMD(torch.cat(mmd_input, dim=1), torch.cat(mmd_target, dim=1), kernel, device, dtype))

    if split:
        return mmds
    else:
        return sum(mmds)/len(mmds)

    

def train( device, name_dir, config,  outputDir, dtype,
           world_size=None, device_ids=None):
    # device is device when not distributed and rank when distributed
    print("START OF RANK:", device)
    if world_size is not None:
        ddp_setup(device, world_size, config.ddp_port)

    device_id = device_ids[device] if device_ids is not None else device

    train_dataset = DatasetCombined(config.input_dataset_train, dev=device, new_higgs=True,
                                    dtype=dtype, datasets=['partons_lab', 'reco_lab'],
                           reco_list_lab=['scaledLogReco_sortedBySpanet',
                                          'mask_scaledLogReco_sortedBySpanet',
                                          'mask_boost', 'scaledLogBoost'],
                           parton_list_lab=['logScaled_data_higgs_t_tbar_ISR',
                                           'logScaled_data_boost'])

    val_dataset = DatasetCombined(config.input_dataset_validation,dev=device, new_higgs=True,
                                  dtype=dtype, datasets=['partons_lab', 'reco_lab'],
                           reco_list_lab=['scaledLogReco_sortedBySpanet',
                                          'mask_scaledLogReco_sortedBySpanet',
                                          'mask_boost', 'scaledLogBoost'],
                           parton_list_lab=['logScaled_data_higgs_t_tbar_ISR',
                                           'logScaled_data_boost'])

    no_recoObjs = train_dataset.reco_lab.scaledLogReco_sortedBySpanet.shape[1]

    log_mean_reco = train_dataset.reco_lab.meanRecoParticles
    log_std_reco = train_dataset.reco_lab.stdRecoParticles
    log_mean_parton_Hthad = train_dataset.partons_lab.mean_log_data_higgs_t_tbar_ISR
    log_std_parton_Hthad = train_dataset.partons_lab.std_log_data_higgs_t_tbar_ISR
    log_mean_boost_parton = train_dataset.partons_lab.mean_log_data_boost
    log_std_boost_parton = train_dataset.partons_lab.std_log_data_boost
    
    if device == torch.device('cuda'):
        log_mean_reco = log_mean_reco.cuda()
        log_std_reco = log_std_reco.cuda()

        log_mean_parton_Hthad = log_mean_parton_Hthad.cuda()
        log_std_parton_Hthad = log_std_parton_Hthad.cuda()
        log_mean_boost_parton = log_mean_boost_parton.cuda()
        log_std_boost_parton = log_std_boost_parton.cuda()
        
    model = ConditioningTransformerLayer_v3_Propag(no_recoVars=4, # exist + 3-mom
                                            no_partonVars=3, # 
                                            hidden_features=config.conditioning_transformer.hidden_features,
                                            DNN_input=config.conditioning_transformer.hidden_features + 1,
                                            dim_feedforward_transformer= config.conditioning_transformer.dim_feedforward_transformer,
                                            nhead_encoder=config.conditioning_transformer.nhead_encoder,
                                            no_layers_encoder=config.conditioning_transformer.no_layers_encoder,
                                            no_layers_decoder=config.conditioning_transformer.no_layers_decoder,
                                            transformer_activation=nn.GELU(),
                                            DNN_layers=config.conditioning_transformer.DNN_layers,
                                            DNN_nodes=config.conditioning_transformer.DNN_nodes,
                                            aggregate=config.conditioning_transformer.aggregate,
                                            arctanh=True,
                                            dtype=dtype,
                                            device=device) 

    model = model.to(device)

    if world_size is not None:
        ddp_model = DDP(
            model,
            device_ids=[device],
            output_device=device,
            # find_unused_parameters=True,
        )
        #print(ddp_model)
        model = ddp_model.module
    else:
        ddp_model = model

    
    modelName = f"{name_dir}/model_{config.name}_{config.version}.pt"

    if device == 0 or world_size is None:
        # Loading comet_ai logging
        exp = Experiment(
            api_key=config.comet_token,
            project_name="memflow",
            workspace="antoniopetre",
            auto_output_logging = "simple",
            # disabled=True
        )
        exp.add_tags([config.name,config.version, 'isr', 'onlypropag', 'phi optimized', '1D MMD constraints', 'moreMMD'])
        exp.log_parameters({"model_param_tot": count_parameters(model)})
        exp.log_parameters(config)
        
        exp.set_name(f"CondTransformer_Propag_1dMMDconstraints_phiOptimized_ISR_{randint(0, 1000)}")
    else:
        exp = None

    # Datasets
    trainingLoader = DataLoader(
        train_dataset,
        batch_size= config.training_params.batch_size_training,
        shuffle=False if world_size is not None else True,
        sampler=DistributedSampler(train_dataset) if world_size is not None else None,
    )
    validLoader = DataLoader(
        val_dataset,
        batch_size=config.training_params.batch_size_training,
        shuffle=False,        
    )
        
    constraints = []
    for i in range(5):
        for j in range(2):
            print(config.MDMM.max_mmd_1d[j])
            constraints.append(mdmm.MaxConstraint(
                                        compute_mmd_loss,
                                        config.MDMM.max_mmd_1d[j], # to be modified based on the regression
                                        scale=config.MDMM.scale_mmd,
                                        damping=config.MDMM.dumping_mmd,
                                        ))
        
    # Create the optimizer
    if dtype == torch.float32:
        MDMM_module = mdmm.MDMM(constraints).float() # support many constraints TODO: use a constraint for every particle
    else:
        MDMM_module = mdmm.MDMM(constraints)

    loss_fn = torch.nn.HuberLoss(delta=config.training_params.huber_delta, reduction='none')

    # optimizer = optim.Adam(list(model.parameters()) , lr=config.training_params.lr)
    optimizer = MDMM_module.make_optimizer(model.parameters(), lr=config.training_params.lr)

    # Scheduler selection
    scheduler_type = config.training_params.scheduler
     
    if scheduler_type == "cosine_scheduler":
        scheduler = CosineAnnealingLR(optimizer,
                                  T_max=config.training_params.cosine_scheduler.Tmax,
                                  eta_min=config.training_params.cosine_scheduler.eta_min)
    elif scheduler_type == "reduce_on_plateau":
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer,
                                                              factor=config.training_params.reduce_on_plateau.factor,
                                                              patience=config.training_params.reduce_on_plateau.patience,
                                                              threshold=config.training_params.reduce_on_plateau.threshold,
                                                               min_lr=config.training_params.reduce_on_plateau.get("min_lr", 1e-7),
                                                               verbose=True)
    elif scheduler_type == "cyclic_lr":
        scheduler = torch.optim.lr_scheduler.CyclicLR(optimizer,
                                                     base_lr= config.training_params.cyclic_lr.base_lr,
                                                     max_lr= config.training_params.cyclic_lr.max_lr,
                                                     step_size_up=config.training_params.cyclic_lr.step_size_up,
                                                     step_size_down=None,
                                                      cycle_momentum=False,
                                                     gamma=config.training_params.cyclic_lr.gamma,
                                                     mode=config.training_params.cyclic_lr.mode,
                                                      verbose=False)
    
    early_stopper = EarlyStopper(patience=config.training_params.nEpochsPatience, min_delta=0.0001)

    # attach one-hot encoded position for jets
    pos_jets_lepton_MET = [pos for pos in range(8)] # 6 jets + lepton + MET
    pos_other_jets = [8 for pos in range(no_recoObjs - 8)]
    
    pos_jets_lepton_MET = torch.tensor(pos_jets_lepton_MET, device=device, dtype=dtype)
    pos_other_jets = torch.tensor(pos_other_jets, device=device, dtype=dtype)
    pos_logScaledReco = torch.cat((pos_jets_lepton_MET, pos_other_jets), dim=0).unsqueeze(dim=1)

    # attach one-hot encoded position for partons
    pos_partons = torch.tensor([pos for pos in range(4)], device=device, dtype=dtype).unsqueeze(dim=1) # higgs, t1, t2, ISR

    # sin_cos embedding
    pos_logScaledReco = sinusoidal_positional_embedding(token_sequence_size=no_recoObjs,
                                                        token_embedding_dim=config.conditioning_transformer.hidden_features,
                                                        device=device,
                                                        n=10000.0)

    # 9 partons
    pos_partons = sinusoidal_positional_embedding(token_sequence_size=4,
                                                  token_embedding_dim=config.conditioning_transformer.hidden_features,
                                                  device=device,
                                                  n=10000.0)
    
    # new order: lepton MET higgs1 higgs2 etc
    new_order_list = [6,7,0,1,2,3,4,5]
    lastElems = [i+8 for i in range(no_recoObjs - 8)]
    new_order_list = new_order_list + lastElems
    new_order = torch.LongTensor(new_order_list)

    # [higgs, top_hadronic, top_leptonic, gluon_ISR]
    partons_order = [0,1,2,3]

    partons_name = ['higgs', 'thad', 'tlep', 'ISR']
    partons_var = ['pt','eta','phi']
    boost_var = ['E', 'pz']

    No_regressed_vars = 4
    posLogParton = torch.linspace(0, 1, No_regressed_vars, device=device, dtype=dtype)

    ii = 0
    for e in range(config.training_params.nepochs):
          
        N_train = 0
        N_valid = 0
        if world_size is not None:
            print(
                f"[GPU{device_id}] | Rank {device} | Epoch {e} | Batchsize: {config.training_params.batch_size_training*len(device_ids)} | Steps: {len(trainingLoader)}"
            )
            trainingLoader.sampler.set_epoch(e)
            
        sum_loss = 0.

        # training loop    
        print("Before training loop")
        ddp_model.train()

        for i, data_batch in enumerate(trainingLoader):
            N_train += 1
            ii+=1

            optimizer.zero_grad()
            
            (logScaled_partons,
             logScaled_parton_boost,
             logScaled_reco_sortedBySpanet, mask_recoParticles,
             mask_boost_reco, data_boost_reco) = data_batch
                                        
            # exist + 3-mom
            logScaled_reco_sortedBySpanet = logScaled_reco_sortedBySpanet[:,:,[0,1,2,3]]
            # The provenance is remove in the model

            # put the lepton first:
            logScaled_reco_sortedBySpanet = logScaled_reco_sortedBySpanet[:,new_order,:]
            mask_recoParticles = mask_recoParticles[:,new_order]

            # same order for partons:
            logScaled_partons = logScaled_partons[:,partons_order,:]

            # remove prov from partons
            logScaled_partons = logScaled_partons[:,:,[0,1,2]] # [pt,eta,phi] -> skip type=1/2 for partons/leptons

            propagators_momenta, boost_regressed, free_latent_space = ddp_model(logScaled_reco_sortedBySpanet,
                                                    data_boost_reco[:,:,[0,3]], mask_recoParticles, mask_boost_reco,
                                                    No_regressed_vars=No_regressed_vars, sin_cos_reco=pos_logScaledReco, sin_cos_partons=pos_partons,
                                                    sin_cos_embedding=True, attach_position=posLogParton, eps_arctanh=0.) # use only log(E)/pz

            H_thad_tlep_boost = [propagators_momenta[:,0], propagators_momenta[:,1],
                                 propagators_momenta[:,2], boost_regressed[...,1]]

            # data_regressed_lab_ptetaphi = H thad tlep ISR pt eta phi
            (propagators_momenta_lab_ptetaphi,
             boost_regressed_lab) = particle_tools.get_HttISR_fromlab(H_thad_tlep_boost,
                                                                  log_mean_parton_Hthad,
                                                                  log_std_parton_Hthad,
                                                                  log_mean_boost_parton,
                                                                  log_std_boost_parton,
                                                                  device, cartesian=False,
                                                                  eps=1e-5,
                                                                  return_both=False,
                                                                  unscale_phi=False)

            HthadtlepISR_lab_ptetaphi_scaled = propagators_momenta_lab_ptetaphi.clone()
            HthadtlepISR_lab_ptetaphi_scaled[...,0] = torch.log(propagators_momenta_lab_ptetaphi[...,0] + 1)
            HthadtlepISR_lab_ptetaphi_scaled[...,:2] = (HthadtlepISR_lab_ptetaphi_scaled[...,:2] - log_mean_parton_Hthad[:2])/log_std_parton_Hthad[:2]
    
            mask = abs(propagators_momenta - HthadtlepISR_lab_ptetaphi_scaled[:,:3]) > 1e-3
            if mask.any():
                raise Exception('Function `get_HttISR_fromlab` is not good')
                        
            MMD_input = [HthadtlepISR_lab_ptetaphi_scaled[:,i] for i in range(4)] # pt eta phi
            MMD_input.append(boost_regressed[:,0])

            MMD_target = [logScaled_partons[:,i] for i in range(4)]
            MMD_target.append(logScaled_parton_boost)

            regr_loss =  compute_regr_losses(HthadtlepISR_lab_ptetaphi_scaled, logScaled_partons,
                                            boost_regressed[:,0], logScaled_parton_boost,
                                            config.cartesian, loss_fn,
                                            split=False, dtype=dtype,
                                            device=device)

            for particle in range(4):
                # phi for higgs/thad/tlep/ISR
                regr_loss += compute_mmd_loss([MMD_input[particle][...,2:3]], [MMD_target[particle][...,2:3]],
                                                  kernel=config.training_params.mmd_kernel,
                                                  device=device, total=False, dtype=dtype, split=False)

            # boost E
            regr_loss += compute_mmd_loss([MMD_input[3][...,0:1]], [MMD_target[3][...,0:1]],
                                                  kernel=config.training_params.mmd_kernel,
                                                  device=device, total=False, dtype=dtype, split=False)

            # boost pz
            regr_loss += compute_mmd_loss([MMD_input[3][...,1:2]], [MMD_target[3][...,1:2]],
                                                  kernel=config.training_params.mmd_kernel,
                                                  device=device, total=False, dtype=dtype, split=False)

            mdmm_return = MDMM_module(regr_loss,
                            [([MMD_input[particle][...,feature:feature+1]], [MMD_target[particle][...,feature:feature+1]], config.training_params.mmd_kernel, device, dtype, False, False) for feature in range(2) for particle in range(4)])
            
            loss_final = mdmm_return.value

            loss_final.backward()
            optimizer.step()

            with torch.no_grad():

                mmds = compute_mmd_loss(MMD_input, MMD_target,
                                      kernel=config.training_params.mmd_kernel,
                                      device=device, total=False, dtype=dtype, split=True)

                loss_higgs, loss_thad, loss_tlep, loss_ISR, loss_boost = compute_regr_losses(HthadtlepISR_lab_ptetaphi_scaled, logScaled_partons,
                                                                                boost_regressed[:,0], logScaled_parton_boost,
                                                                                config.cartesian, loss_fn,
                                                                                split=True, dtype=dtype,
                                                                                device=device)
                
                if exp is not None and device==0 or world_size is None:
                    if i % 10 == 0:
                        for kk in range(14):
                            exp.log_metric(f"loss_mmd_{kk}", mmds[kk].item(),step=ii)

                        exp.log_metric('loss_regr_0', loss_higgs,step=ii)
                        exp.log_metric('loss_regr_1', loss_thad,step=ii)
                        exp.log_metric('loss_regr_2', loss_tlep,step=ii)
                        exp.log_metric('loss_regr_3', loss_ISR,step=ii)
                        exp.log_metric('loss_regr_4', loss_boost,step=ii)
                        
                        
                        exp.log_metric('loss_mmd_tot', sum(mmds)/14,step=ii)
                        exp.log_metric('loss_regr_tot', regr_loss.item(),step=ii)
                        exp.log_metric('loss_tot_train', loss_final.item(),step=ii)
                
                sum_loss += loss_final.item()

        ### END of training 
        if exp is not None and device==0 or world_size is None:
            exp.log_metric("loss_epoch_total_train", sum_loss/N_train, epoch=e, step=ii)
            exp.log_metric("learning_rate", optimizer.param_groups[0]['lr'], epoch=e, step=ii)

        
        valid_loss_regr_perObj = torch.zeros(5, device=device, dtype=dtype)
        valid_loss_mmd_perObj = [0. for i in range(14)]
        valid_loss_regr_total = valid_loss_mmd_total = valid_loss_total = 0.
        
        # validation loop (don't update weights and gradients)
        print("Before validation loop")
        ddp_model.eval()
        
        for i, data_batch in enumerate(validLoader):
            N_valid +=1
            # Move data to device
            with torch.no_grad():
                    
                (logScaled_partons,
                 logScaled_parton_boost,
                 logScaled_reco_sortedBySpanet, mask_recoParticles,
                 mask_boost_reco, data_boost_reco) = data_batch
                                            
                # exist + 3-mom
                logScaled_reco_sortedBySpanet = logScaled_reco_sortedBySpanet[:,:,[0,1,2,3]]
                # The provenance is remove in the model
    
                # put the lepton first:
                logScaled_reco_sortedBySpanet = logScaled_reco_sortedBySpanet[:,new_order,:]
                mask_recoParticles = mask_recoParticles[:,new_order]
    
                # same order for partons:
                logScaled_partons = logScaled_partons[:,partons_order,:]
    
                # remove prov from partons
                logScaled_partons = logScaled_partons[:,:,[0,1,2]] # [pt,eta,phi] -> skip type=1/2 for partons/leptons
    
                propagators_momenta, boost_regressed, free_latent_space = ddp_model(logScaled_reco_sortedBySpanet,
                                                        data_boost_reco[:,:,[0,3]], mask_recoParticles, mask_boost_reco,
                                                        No_regressed_vars=No_regressed_vars, sin_cos_reco=pos_logScaledReco, sin_cos_partons=pos_partons,
                                                        sin_cos_embedding=True, attach_position=posLogParton, eps_arctanh=0.) # use only log(E)/pz

                H_thad_tlep_boost = [propagators_momenta[:,0], propagators_momenta[:,1],
                                     propagators_momenta[:,2], boost_regressed[...,1]]
                            
                # data_regressed_lab_ptetaphi = H thad tlep ISR pt eta phi
                (propagators_momenta_lab_ptetaphi,
                 boost_regressed_lab) = particle_tools.get_HttISR_fromlab(H_thad_tlep_boost,
                                                                      log_mean_parton_Hthad,
                                                                      log_std_parton_Hthad,
                                                                      log_mean_boost_parton,
                                                                      log_std_boost_parton,
                                                                      device, cartesian=False,
                                                                      eps=1e-5,
                                                                      return_both=False,
                                                                      unscale_phi=False)
    
                HthadtlepISR_lab_ptetaphi_scaled = propagators_momenta_lab_ptetaphi.clone()
                HthadtlepISR_lab_ptetaphi_scaled[...,0] = torch.log(propagators_momenta_lab_ptetaphi[...,0] + 1)
                HthadtlepISR_lab_ptetaphi_scaled[...,:2] = (HthadtlepISR_lab_ptetaphi_scaled[...,:2] - log_mean_parton_Hthad[:2])/log_std_parton_Hthad[:2]
        
                mask = abs(propagators_momenta - HthadtlepISR_lab_ptetaphi_scaled[:,:3]) > 1e-3
                if mask.any():
                    raise Exception('Function `get_HttISR_fromlab` is not good')
                            
                MMD_input = [HthadtlepISR_lab_ptetaphi_scaled[:,i] for i in range(4)] # pt eta phi
                MMD_input.append(boost_regressed[:,0])
    
                MMD_target = [logScaled_partons[:,i] for i in range(4)]
                MMD_target.append(logScaled_parton_boost)
    
                regr_loss =  compute_regr_losses(HthadtlepISR_lab_ptetaphi_scaled, logScaled_partons,
                                                boost_regressed[:,0], logScaled_parton_boost,
                                                config.cartesian, loss_fn,
                                                split=False, dtype=dtype,
                                                device=device)
    
                for particle in range(4):
                    # phi for higgs/thad/tlep/ISR
                    regr_loss += compute_mmd_loss([MMD_input[particle][...,2:3]], [MMD_target[particle][...,2:3]],
                                                      kernel=config.training_params.mmd_kernel,
                                                      device=device, total=False, dtype=dtype, split=False)
    
                # boost E
                regr_loss += compute_mmd_loss([MMD_input[3][...,0:1]], [MMD_target[3][...,0:1]],
                                                      kernel=config.training_params.mmd_kernel,
                                                      device=device, total=False, dtype=dtype, split=False)
    
                # boost pz
                regr_loss += compute_mmd_loss([MMD_input[3][...,1:2]], [MMD_target[3][...,1:2]],
                                                      kernel=config.training_params.mmd_kernel,
                                                      device=device, total=False, dtype=dtype, split=False)
    
                mdmm_return = MDMM_module(regr_loss,
                                [([MMD_input[particle][...,feature:feature+1]], [MMD_target[particle][...,feature:feature+1]], config.training_params.mmd_kernel, device, dtype, False, False) for feature in range(2) for particle in range(4)])
                
                loss_final = mdmm_return.value

                mmds = compute_mmd_loss(MMD_input, MMD_target,
                                          kernel=config.training_params.mmd_kernel,
                                          device=device, total=False, dtype=dtype, split=True)

                loss_higgs, loss_thad, loss_tlep, loss_ISR, loss_boost = compute_regr_losses(HthadtlepISR_lab_ptetaphi_scaled, logScaled_partons,
                                                                                boost_regressed[:,0], logScaled_parton_boost,
                                                                                config.cartesian, loss_fn,
                                                                                split=True, dtype=dtype,
                                                                                device=device)

                valid_loss_regr_perObj[0] += loss_higgs
                valid_loss_regr_perObj[1] += loss_thad
                valid_loss_regr_perObj[2] += loss_tlep
                valid_loss_regr_perObj[3] += loss_boost
                valid_loss_regr_perObj[4] += loss_ISR
                valid_loss_mmd_perObj = [x + y for x, y in zip(valid_loss_mmd_perObj, mmds)]
                valid_loss_regr_total += regr_loss
                valid_loss_mmd_total += sum(mmds)/14
                valid_loss_total += loss_final
            
                if i == 0 and ( exp is not None and device==0 or world_size is None):
                    for particle in range(4):

                        # PLOT DECAY PRODUCTS -> PT/eta/phi
                        for feature in range(3):  
                            fig, ax = plt.subplots(figsize=(7,6), dpi=100)
                            h = ax.hist2d(MMD_input[particle][:,feature].detach().cpu().numpy().flatten(),
                                          MMD_target[particle][:,feature].cpu().detach().numpy().flatten(),
                                          bins=40, range=((-4, 4),(-4, 4)), cmin=1)
                            fig.colorbar(h[3], ax=ax)
                            ax.set_xlabel(f"regressed {partons_var[feature]}")
                            ax.set_ylabel(f"target {partons_var[feature]}")
                            ax.set_title(f"particle {partons_name[particle]} feature {partons_var[feature]}")
                            exp.log_figure(f"2D_{partons_name[particle]}_{partons_var[feature]}", fig, step=e)

                    
                            fig, ax = plt.subplots(figsize=(7,6), dpi=100)
                            ax.hist(MMD_input[particle][:,feature].detach().cpu().numpy().flatten(),
                                          bins=30, range=(-3.2, 3.2), label="regressed", histtype="step")
                            ax.hist(MMD_target[particle][:,feature].cpu().detach().numpy().flatten(),
                                          bins=30, range=(-3.2, 3.2), label="target",histtype="step")
                            ax.legend()
                            ax.set_xlabel(f"{partons_name[particle]} feature {partons_var[feature]}")
                            exp.log_figure(f"1D_{partons_name[particle]}_{partons_var[feature]}", fig, step=e)
                    
                    # PLOT BOOST
                    for feature in range(2):  
                        fig, ax = plt.subplots(figsize=(7,6), dpi=100)
                        h = ax.hist2d(MMD_input[-1][:,feature].detach().cpu().numpy().flatten(),
                                      MMD_target[-1][:,feature].cpu().detach().numpy().flatten(),
                                      bins=40, range=((-3, 3),(-3, 3)), cmin=1)
                        fig.colorbar(h[3], ax=ax)
                        ax.set_xlabel(f"regressed {boost_var[feature]}")
                        ax.set_ylabel(f"target {boost_var[feature]}")
                        ax.set_title(f"boost: feature {boost_var[feature]}")
                        exp.log_figure(f"boost_2D_{boost_var[feature]}", fig,step=e)

                
                        fig, ax = plt.subplots(figsize=(7,6), dpi=100)
                        ax.hist(MMD_input[-1][:,feature].detach().cpu().numpy().flatten(),
                                      bins=30, range=(-2, 2), label="regressed", histtype="step")
                        ax.hist(MMD_target[-1][:,feature].cpu().detach().numpy().flatten(),
                                      bins=30, range=(-2, 2), label="target",histtype="step")
                        ax.legend()
                        ax.set_xlabel(f"boost: feature {boost_var[feature]}")
                        exp.log_figure(f"boost_1D_{boost_var[feature]}", fig, step=e)
                  

        if exp is not None and device==0 or world_size is None:
            for kk in range(5):
                exp.log_metric(f"valid_loss_regr_perObj_{kk}", valid_loss_regr_perObj[kk]/N_valid, epoch=e )
            for kk in range(14):
                exp.log_metric(f"loss_mmd_val_{kk}", valid_loss_mmd_perObj[kk]/N_valid,epoch= e)
            
            exp.log_metric('valid_loss_regr_total', valid_loss_regr_total/N_valid,epoch= e)
            exp.log_metric('valid_loss_mmd_total', valid_loss_mmd_total/N_valid,epoch= e)
            exp.log_metric('valid_loss_total', valid_loss_total/N_valid,epoch= e)
            

        if device == 0 or world_size is None:
            if early_stopper.early_stop(valid_loss_total/N_valid,
                                    model.state_dict(), optimizer.state_dict(), modelName, exp):
                print(f"Model converges at epoch {e} !!!")         
                break

        # Step the scheduler at the end of the val
        # after_N_epochs = config.training_params.cosine_scheduler.get("after_N_epochs", 0)
        # if e > after_N_epochs:
        #     scheduler.step()
        
        scheduler.step(valid_loss_total/N_valid) # reduce lr if the model is not improving anymore
        

    # writer.close()
    # exp_log.end()
    destroy_process_group()
    print('preTraining finished!!')
    

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--path-config', type=str, required=True, help='path to config.yaml File')
    parser.add_argument('--output-dir', type=str, required=True, help='Output directory')
    parser.add_argument('--on-GPU', action="store_true",  help='run on GPU boolean')
    parser.add_argument('--distributed', action="store_true")
    args = parser.parse_args()
    
    path_to_conf = args.path_config
    on_GPU = args.on_GPU # by default run on CPU
    outputDir = args.output_dir

    # Read config file in 'conf'
    with open(path_to_conf) as f:
        conf = OmegaConf.load(path_to_conf)
    
    print("Training with cfg: \n", OmegaConf.to_yaml(conf))

    env_var = os.environ.get("CUDA_VISIBLE_DEVICES")
    if env_var:
        actual_devices = env_var.split(",")
    else:
        actual_devices = list(range(torch.cuda.device_count()))
    print("Actual devices: ", actual_devices)
    world_size = len(actual_devices)

    
    outputDir = os.path.abspath(outputDir)
    latentSpace = conf.conditioning_transformer.use_latent
    name_dir = f'{outputDir}/preTraining_condTransformer_Propag__1dMMDconstraints_phiOptimized_ISR_date&Hour_{datetime.now().strftime("%d_%m_%Y_%H_%M_%S")}'

    os.makedirs(name_dir, exist_ok=True)
    
    with open(f"{name_dir}/config_{conf.name}_{conf.version}.yaml", "w") as fo:
        fo.write(OmegaConf.to_yaml(conf)) 

    if conf.training_params.dtype == "float32":
        dtype = torch.float32
    elif conf.training_params.dtype == "float64":
        dtype = torch.float64
        
    
    if args.distributed:
        
        # make a dictionary with k: rank, v: actual device
        dev_dct = {i: actual_devices[i] for i in range(world_size)}
        print(f"Devices dict: {dev_dct}")
        mp.spawn(
            train,
            args=(name_dir, conf,  outputDir, dtype,
                    world_size, dev_dct),
            nprocs=world_size,
            # join=True
        )
    else:
        device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
        train(device,name_dir, conf,  outputDir, dtype)
    
    print(f"preTraining finished succesfully! Version: {conf.version}")