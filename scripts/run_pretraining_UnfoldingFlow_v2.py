from comet_ml import Experiment
from comet_ml.integration.pytorch import log_model

import torch
from memflow.read_data.dataset_all import DatasetCombined

from memflow.unfolding_network.conditional_transformer_v2 import ConditioningTransformerLayer_v2
from memflow.unfolding_flow.utils import *
from memflow.unfolding_flow.mmd_loss import MMD
from memflow.unfolding_flow.utils import Compute_ParticlesTensor

import numpy as np
from torch import optim
from torch.utils.data import DataLoader
import torch.nn as nn
from torch.nn.functional import normalize
from torch.optim.lr_scheduler import CosineAnnealingLR

import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
from math import floor

import mdmm
# from tensorboardX import SummaryWriter
from omegaconf import OmegaConf
import sys
import argparse
import os
from pynvml import *
import vector

from utils import constrain_energy
from utils import total_mom

from earlystop import EarlyStopper
from memflow.unfolding_flow.utils import Compute_ParticlesTensor

import torch.multiprocessing as mp
from torch.utils.data.distributed import DistributedSampler
from torch.nn.parallel import DistributedDataParallel as DDP
from torch.distributed import init_process_group, destroy_process_group
from torch.profiler import profile, record_function, ProfilerActivity

from random import randint
PI = torch.pi

# torch.autograd.set_detect_anomaly(True)

def ddp_setup(rank, world_size, port):
    """
    Args:
        rank: Unique identifier of each process
        world_size: Total number of processes
    """
    import socket
    print("Setting up ddp for device: ", rank)
    os.environ["MASTER_ADDR"] = socket.gethostname()
    os.environ["MASTER_PORT"] = f"{port}"
    init_process_group(backend="nccl", rank=rank, world_size=world_size)
    torch.cuda.set_device(rank)

def sinusoidal_positional_embedding(token_sequence_size, token_embedding_dim, device, n=10000.0):

    if token_embedding_dim % 2 != 0:
        raise ValueError("Sinusoidal positional embedding cannot apply to odd token embedding dim (got dim={:d})".format(token_embedding_dim))

    T = token_sequence_size
    d = token_embedding_dim #d_model=head_num*d_k, not d_q, d_k, d_v

    positions = torch.arange(0, T).unsqueeze_(1)
    embeddings = torch.zeros(T, d, device=device)

    denominators = torch.pow(n, 2*torch.arange(0, d//2)/d) # 10000^(2i/d_model), i is the index of embedding
    embeddings[:, 0::2] = torch.sin(positions/denominators) # sin(pos/10000^(2i/d_model))
    embeddings[:, 1::2] = torch.cos(positions/denominators) # cos(pos/10000^(2i/d_model))

    return embeddings

def loss_fn_periodic(inp, target, loss_fn, device, dtype):

    # rescale to pi
    # overflow_delta = (inp[inp>PI]- PI).mean()+(inp[inp<-PI]+PI).mean()
    inp[inp>PI] = inp[inp>PI]-2*PI
    inp[inp<-PI] = inp[inp<-PI] + 2*PI
    
    deltaPhi = target - inp
    deltaPhi = torch.where(deltaPhi > PI, deltaPhi - 2*PI, deltaPhi)
    deltaPhi = torch.where(deltaPhi <= -PI, deltaPhi + 2*PI, deltaPhi)
    
    return loss_fn(deltaPhi, torch.zeros(deltaPhi.shape, device=device, dtype=dtype))# + overflow_delta*0.5


def compute_regr_losses(logScaled_partons, logScaled_boost, higgs, thad, tlep, gluon, boost,
                        higgs_b_angles, lepton_angles, tlep_b_angles, thad_light_angles, thad_b_angles,
                        cartesian, loss_fn,
                        scaling_phi_intermediate, scaling_phi_decay, device, dtype, split=False):
    lossH = 0.
    lossThad = 0.
    lossTlep = 0.
    lossGluon = 0.
    
    lossH_angles = 0.
    loss_lepton_angles = 0.
    loss_tlep_b_angles = 0.
    loss_thad_light_angles = 0.
    loss_thad_b_angles = 0.

    if cartesian:
        lossH = loss_fn(logScaled_partons[:,-3], higgs)
        lossThad =  loss_fn(logScaled_partons[:,-2], thad)
        lossTlep =  loss_fn(logScaled_partons[:,-1], tlep)
    else:
        for feature in range(higgs.shape[1]):
            # if feature != phi
            if feature != 2:
                lossH += loss_fn(logScaled_partons[:,-3,feature], higgs[:,feature])
                lossThad +=  loss_fn(logScaled_partons[:,-2,feature], thad[:,feature])
                lossTlep +=  loss_fn(logScaled_partons[:,-1,feature], tlep[:,feature])
                lossGluon +=  loss_fn(logScaled_partons[:,-4,feature], gluon[:,feature])
            # case when feature is equal to phi (phi is periodic variable)
            else:
                lossH_phi = loss_fn_periodic(higgs[:,feature]*scaling_phi_intermediate[1] + scaling_phi_intermediate[0],
                                          logScaled_partons[:,-3,feature]*scaling_phi_intermediate[1] + scaling_phi_intermediate[0], loss_fn, device, dtype)
                
                lossThad_phi =  loss_fn_periodic(thad[:,feature]*scaling_phi_intermediate[1] + scaling_phi_intermediate[0],
                                              logScaled_partons[:,-2,feature]*scaling_phi_intermediate[1] + scaling_phi_intermediate[0], loss_fn, device, dtype)
                
                lossTlep_phi =  loss_fn_periodic(tlep[:,feature]*scaling_phi_intermediate[1] + scaling_phi_intermediate[0],
                                              logScaled_partons[:,-1,feature]*scaling_phi_intermediate[1] + scaling_phi_intermediate[0], loss_fn, device, dtype)
                
                lossGluon_phi =  loss_fn_periodic(gluon[:,feature]*scaling_phi_intermediate[1] + scaling_phi_intermediate[0],
                                               logScaled_partons[:,-4,feature]*scaling_phi_intermediate[1] + scaling_phi_intermediate[0], loss_fn, device, dtype)

                lossH += (lossH_phi - scaling_phi_intermediate[0])/scaling_phi_intermediate[1]
                lossThad += (lossThad_phi - scaling_phi_intermediate[0])/scaling_phi_intermediate[1]
                lossTlep += (lossTlep_phi - scaling_phi_intermediate[0])/scaling_phi_intermediate[1]
                lossGluon += (lossGluon_phi - scaling_phi_intermediate[0])/scaling_phi_intermediate[1]

        for feature in range(higgs_b_angles.shape[1]):
            # if feature != phi
            if feature != 1:
                lossH_angles += loss_fn(logScaled_partons[:,2,feature], higgs_b_angles[:,feature])
                loss_lepton_angles +=  loss_fn(logScaled_partons[:,0,feature], lepton_angles[:,feature])
                loss_tlep_b_angles +=  loss_fn(logScaled_partons[:,7,feature], tlep_b_angles[:,feature])
                loss_thad_light_angles +=  loss_fn(logScaled_partons[:,5,feature], thad_light_angles[:,feature])
                loss_thad_b_angles +=  loss_fn(logScaled_partons[:,4,feature], thad_b_angles[:,feature])
            # case when feature is equal to phi (phi is periodic variable)
            else:

                lossH_angles_phi = loss_fn_periodic(higgs_b_angles[:,feature]*scaling_phi_decay[1] + scaling_phi_decay[0],
                                                  logScaled_partons[:,2,feature]*scaling_phi_decay[1] + scaling_phi_decay[0], loss_fn, device, dtype)
                
                loss_lepton_angles_phi =  loss_fn_periodic(lepton_angles[:,feature]*scaling_phi_decay[1] + scaling_phi_decay[0],
                                                  logScaled_partons[:,0,feature]*scaling_phi_decay[1] + scaling_phi_decay[0], loss_fn, device, dtype)

                loss_tlep_b_angles_phi =  loss_fn_periodic(tlep_b_angles[:,feature]*scaling_phi_decay[1] + scaling_phi_decay[0],
                                                  logScaled_partons[:,7,feature]*scaling_phi_decay[1] + scaling_phi_decay[0], loss_fn, device, dtype)
                
                loss_thad_light_angles_phi =  loss_fn_periodic(thad_light_angles[:,feature]*scaling_phi_decay[1] + scaling_phi_decay[0],
                                                  logScaled_partons[:,5,feature]*scaling_phi_decay[1] + scaling_phi_decay[0], loss_fn, device, dtype)                
                
                loss_thad_b_angles_phi =  loss_fn_periodic(thad_b_angles[:,feature]*scaling_phi_decay[1] + scaling_phi_decay[0],
                                                  logScaled_partons[:,4,feature]*scaling_phi_decay[1] + scaling_phi_decay[0], loss_fn, device, dtype)

                lossH_angles += (lossH_angles_phi - scaling_phi_decay[0])/scaling_phi_decay[1]
                loss_lepton_angles += (loss_lepton_angles_phi - scaling_phi_decay[0])/scaling_phi_decay[1]
                loss_tlep_b_angles += (loss_tlep_b_angles_phi - scaling_phi_decay[0])/scaling_phi_decay[1]
                loss_thad_light_angles += (loss_thad_light_angles_phi - scaling_phi_decay[0])/scaling_phi_decay[1]
                loss_thad_b_angles += (loss_thad_b_angles_phi - scaling_phi_decay[0])/scaling_phi_decay[1]

        lossBoost = loss_fn(logScaled_boost, boost)

    if split:
        return lossH, lossThad, lossTlep, lossGluon, lossBoost, lossH_angles, loss_lepton_angles, loss_tlep_b_angles, loss_thad_light_angles, loss_thad_b_angles
    else:
        return  (lossH + lossThad + lossTlep + lossGluon + lossBoost + lossH_angles + loss_lepton_angles + loss_thad_light_angles + loss_tlep_b_angles + loss_thad_b_angles)/25


def compute_mmd_loss(mmd_input, mmd_target, kernel, device, dtype, total=False, split=False):
    mmds = []
    for particle in range(len(mmd_input)):
        mmds.append(MMD(mmd_input[particle], mmd_target[particle], kernel, device, dtype))
    # total MMD
    if total:
        mmds.append(MMD(torch.cat(mmd_input, dim=1), torch.cat(mmd_target, dim=1), kernel, device, dtype))

    if split:
        return mmds
    else:
        return sum(mmds)/len(mmds)

    

def train( device, name_dir, config,  outputDir, dtype,
           world_size=None, device_ids=None):
    # device is device when not distributed and rank when distributed
    print("START OF RANK:", device)
    if world_size is not None:
        ddp_setup(device, world_size, config.ddp_port)

    device_id = device_ids[device] if device_ids is not None else device

    train_dataset = DatasetCombined(config.input_dataset_train, dev=device,
                                    dtype=dtype, datasets=['partons_lab', 'reco_lab'],
                           reco_list_lab=['scaledLogReco_sortedBySpanet',
                                          'mask_scaledLogReco_sortedBySpanet',
                                          'mask_boost', 'scaledLogBoost'],
                           parton_list_lab=['tensor_AllPartons',
                                           'mask_AllPartons',
                                           'logScaled_data_boost'])

    val_dataset = DatasetCombined(config.input_dataset_validation,dev=device,
                                  dtype=dtype, datasets=['partons_lab', 'reco_lab'],
                           reco_list_lab=['scaledLogReco_sortedBySpanet',
                                          'mask_scaledLogReco_sortedBySpanet',
                                          'mask_boost', 'scaledLogBoost'],
                           parton_list_lab=['tensor_AllPartons',
                                           'mask_AllPartons',
                                           'logScaled_data_boost'])

    no_recoObjs = train_dataset.reco_lab.scaledLogReco_sortedBySpanet_ptcut.shape[1]

    log_mean_reco = train_dataset.reco_lab.meanRecoParticles_ptcut
    log_std_reco = train_dataset.reco_lab.stdRecoParticles_ptcut
    log_mean_parton = train_dataset.partons_lab.mean_log_partonsLeptons
    log_std_parton = train_dataset.partons_lab.std_log_partonsLeptons
    log_mean_parton_Hthad = train_dataset.partons_lab.mean_log_data_higgs_t_tbar_ISR
    log_std_parton_Hthad = train_dataset.partons_lab.std_log_data_higgs_t_tbar_ISR
    log_mean_boost_parton = train_dataset.partons_lab.mean_log_data_boost
    log_std_boost_parton = train_dataset.partons_lab.std_log_data_boost
    
    minPt_eachObj = train_dataset.reco_lab.min_pt_eachReco

    if device == torch.device('cuda'):
        log_mean_reco = log_mean_reco.cuda()
        log_std_reco = log_std_reco.cuda()
        log_mean_parton = log_mean_parton.cuda()
        log_std_parton = log_std_parton.cuda()

        log_mean_parton_Hthad = log_mean_parton_Hthad.cuda()
        log_std_parton_Hthad = log_std_parton_Hthad.cuda()
        log_mean_boost_parton = log_mean_boost_parton.cuda()
        log_std_boost_parton = log_std_boost_parton.cuda()
        
        minPt_eachObj = minPt_eachObj.cuda()

    model = ConditioningTransformerLayer_v2(no_recoVars=4, # exist + 3-mom
                                            no_partonVars=5, # 
                                            hidden_features=config.conditioning_transformer.hidden_features,
                                            dim_feedforward_transformer= config.conditioning_transformer.dim_feedforward_transformer,
                                            nhead_encoder=config.conditioning_transformer.nhead_encoder,
                                            no_layers_encoder=config.conditioning_transformer.no_layers_encoder,
                                            no_layers_decoder=config.conditioning_transformer.no_layers_decoder,
                                            transformer_activation=nn.GELU(),
                                            aggregate=config.conditioning_transformer.aggregate,
                                            use_latent=config.conditioning_transformer.use_latent,
                                            out_features_latent=config.conditioning_transformer.out_features_latent,
                                            no_layers_decoder_latent=config.conditioning_transformer.no_layers_decoder_latent,     
                                            dtype=dtype,
                                            device=device) 

    model = model.to(device)
    #model.disable_latent_training()

    if world_size is not None:
        ddp_model = DDP(
            model,
            device_ids=[device],
            output_device=device,
            # find_unused_parameters=True,
        )
        #print(ddp_model)
        model = ddp_model.module
    else:
        ddp_model = model

    
    modelName = f"{name_dir}/model_{config.name}_{config.version}.pt"

    if device == 0 or world_size is None:
        # Loading comet_ai logging
        exp = Experiment(
            api_key=config.comet_token,
            project_name="memflow",
            workspace="antoniopetre",
            auto_output_logging = "simple",
            # disabled=True
        )
        exp.add_tags([config.name,config.version, 'preTrainUnfolding', 'NewVersion'])
        exp.log_parameters(config.training_params)
        exp.log_parameters(config.conditioning_transformer)
        exp.log_parameters(config.MDMM) 
    else:
        exp = None

    # Datasets
    trainingLoader = DataLoader(
        train_dataset,
        batch_size= config.training_params.batch_size_training,
        shuffle=False if world_size is not None else True,
        sampler=DistributedSampler(train_dataset) if world_size is not None else None,
        #pin_memory=True,
        # collate_fn=my_collate, # use custom collate function here
        #pin_memory=True,
    )
    validLoader = DataLoader(
        val_dataset,
        batch_size=config.training_params.batch_size_training,
        shuffle=False,
        # collate_fn=my_collate,
        
    )
        
    constraint = mdmm.MaxConstraint(
                    compute_mmd_loss,
                    config.MDMM.max_mmd, # to be modified based on the regression
                    scale=config.MDMM.scale_mmd,
                    damping=config.MDMM.dumping_mmd,
                    )

    # Create the optimizer
    if dtype == torch.float32:
        MDMM_module = mdmm.MDMM([constraint]).float() # support many constraints TODO: use a constraint for every particle
    else:
        MDMM_module = mdmm.MDMM([constraint])

    loss_fn = torch.nn.HuberLoss(delta=config.training_params.huber_delta)

    # optimizer = optim.Adam(list(model.parameters()) , lr=config.training_params.lr)
    optimizer = MDMM_module.make_optimizer(model.parameters(), lr=config.training_params.lr)

    # Scheduler selection
    scheduler_type = config.training_params.scheduler
     
    if scheduler_type == "cosine_scheduler":
        scheduler = CosineAnnealingLR(optimizer,
                                  T_max=config.training_params.cosine_scheduler.Tmax,
                                  eta_min=config.training_params.cosine_scheduler.eta_min)
    elif scheduler_type == "reduce_on_plateau":
        scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer,
                                                              factor=config.training_params.reduce_on_plateau.factor,
                                                              patience=config.training_params.reduce_on_plateau.patience,
                                                              threshold=config.training_params.reduce_on_plateau.threshold,
                                                               min_lr=config.training_params.reduce_on_plateau.get("min_lr", 1e-7),
                                                               verbose=True)
    elif scheduler_type == "cyclic_lr":
        scheduler = torch.optim.lr_scheduler.CyclicLR(optimizer,
                                                     base_lr= config.training_params.cyclic_lr.base_lr,
                                                     max_lr= config.training_params.cyclic_lr.max_lr,
                                                     step_size_up=config.training_params.cyclic_lr.step_size_up,
                                                     step_size_down=None,
                                                      cycle_momentum=False,
                                                     gamma=config.training_params.cyclic_lr.gamma,
                                                     mode=config.training_params.cyclic_lr.mode,
                                                      verbose=False)
    
    early_stopper = EarlyStopper(patience=config.training_params.nEpochsPatience, min_delta=0.0001)

    # attach one-hot encoded position for jets
    pos_jets_lepton_MET = [pos for pos in range(8)] # 6 jets + lepton + MET
    pos_other_jets = [8 for pos in range(no_recoObjs - 8)]
    
    pos_jets_lepton_MET = torch.tensor(pos_jets_lepton_MET, device=device, dtype=dtype)
    pos_other_jets = torch.tensor(pos_other_jets, device=device, dtype=dtype)
    pos_logScaledReco = torch.cat((pos_jets_lepton_MET, pos_other_jets), dim=0).unsqueeze(dim=1)

    # attach one-hot encoded position for partons
    pos_partons = torch.tensor([pos for pos in range(9)], device=device, dtype=dtype).unsqueeze(dim=1) # higgs, t1, t2, ISR

    # sin_cos embedding
    pos_logScaledReco = sinusoidal_positional_embedding(token_sequence_size=no_recoObjs + 1, # + 1 for boost
                                                        token_embedding_dim=config.conditioning_transformer.hidden_features,
                                                        device=device,
                                                        n=10000.0)

    # 9 partons
    pos_partons = sinusoidal_positional_embedding(token_sequence_size=9,
                                                  token_embedding_dim=config.conditioning_transformer.hidden_features,
                                                  device=device,
                                                  n=10000.0)
    
    # new order: lepton MET higgs1 higgs2 etc
    new_order_list = [6,7,0,1,2,3,4,5]
    lastElems = [i+8 for i in range(no_recoObjs - 8)]
    new_order_list = new_order_list + lastElems
    new_order = torch.LongTensor(new_order_list)

    # lepton1/neutrino/higgs 1,2/thad 1,2,3/tlep/ISR/...
    # higgs and thad ordered by pt
    partons_order = [7,8,0,1,2,4,5,3,6]

    ii = 0
    for e in range(config.training_params.nepochs):
          
        N_train = 0
        N_valid = 0
        if world_size is not None:
            print(
                f"[GPU{device_id}] | Rank {device} | Epoch {e} | Batchsize: {config.training_params.batch_size_training*len(device_ids)} | Steps: {len(trainingLoader)}"
            )
            trainingLoader.sampler.set_epoch(e)
            
        sum_loss = 0.
    
        # training loop    
        print("Before training loop")
        ddp_model.train()

        for i, data_batch in enumerate(trainingLoader):
            N_train += 1
            ii+=1

            optimizer.zero_grad()
            
            (logScaled_partons,
             mask_partonsLeptons,
             logScaled_parton_boost,
             logScaled_reco_sortedBySpanet, mask_recoParticles,
             mask_boost_reco, data_boost_reco) = data_batch

            if i == 10:
                break
                            
            # exist + 3-mom
            logScaled_reco_sortedBySpanet = logScaled_reco_sortedBySpanet[:,:,[0,1,2,3]]
            # The provenance is remove in the model

            # put the lepton first:
            logScaled_reco_sortedBySpanet = logScaled_reco_sortedBySpanet[:,new_order,:]
            mask_recoParticles = mask_recoParticles[:,new_order]

            # same order for partons:
            logScaled_partons = logScaled_partons[:,partons_order,:]
            mask_partonsLeptons = mask_partonsLeptons[:,partons_order]

            # attach 1 hot-encoded position
            logScaled_reco_sortedBySpanet = logScaled_reco_sortedBySpanet

            # remove prov from partons
            logScaled_partons = logScaled_partons[:,:,[0,1,2,3,4]] # [pt,eta,phi,parton_id, type] -> skip type=1/2 for partons/leptons

            intermediate_propagators, boost_scaled, decay_angles = ddp_model(logScaled_reco_sortedBySpanet, data_boost_reco, mask_recoParticles, mask_boost_reco, No_regressed_vars=9, sin_cos_reco=pos_logScaledReco, sin_cos_partons=pos_partons)

            higgs = intermediate_propagators[:,0] 
            thad = intermediate_propagators[:,1]
            tlep = intermediate_propagators[:,2]
            
            higgs_b_angles = decay_angles[:,0]
            lepton_angles = decay_angles[:,1]
            tlep_b_angles = decay_angles[:,2]
            thad_light_angles = decay_angles[:,3]
            thad_b_angles = decay_angles[:,4]

            out = [higgs, thad, tlep, boost_scaled]
            
            data_regressed, boost_regressed = Compute_ParticlesTensor.get_HttISR_fromlab(out, log_mean_parton,
                                                                  log_std_parton,
                                                                  log_mean_boost_parton, log_std_boost_parton,
                                                                  device, cartesian=False, eps=0.0)

            boost_notscaled = boost_regressed[:, [0,-1]]
            boost = boost_notscaled.clone()
            boost[:,0] = torch.log(boost_notscaled[:,0] + 1)
            boost = (boost - log_mean_boost_parton)/log_std_boost_parton

            gluon_toscale = data_regressed[:,3] #pt, eta, phi
            gluon = gluon_toscale.clone()
            gluon[:,0] = torch.log(gluon_toscale[:,0] + 1) # No need for abs and sign
            gluon = (gluon - log_mean_parton) / log_std_parton
                        
            MMD_input = [higgs, thad, tlep, gluon, boost, higgs_b_angles, lepton_angles, tlep_b_angles, thad_light_angles, thad_b_angles]
            
            MMD_target = [logScaled_partons[:,-3, :3],
                          logScaled_partons[:,-2, :3],
                          logScaled_partons[:,-1, :3],
                          logScaled_partons[:,-4, :3],
                          logScaled_parton_boost,
                          logScaled_partons[:,2,1:3],
                          logScaled_partons[:,0,1:3],
                          logScaled_partons[:,7,1:3],
                          logScaled_partons[:,5,1:3],
                          logScaled_partons[:,4,1:3]]

            lossH, lossThad, lossTlep, lossGluon, lossBoost, \
            lossH_angles, loss_lepton_angles, loss_tlep_b_angles, \
            loss_thad_light_angles, loss_thad_b_angles =  compute_regr_losses(logScaled_partons,
                                                                              logScaled_parton_boost,
                                                                              higgs, thad, tlep, gluon, boost,
                                                                              higgs_b_angles, lepton_angles, tlep_b_angles, thad_light_angles, thad_b_angles,
                                                                              config.cartesian, loss_fn,
                                                                              scaling_phi_intermediate=[log_mean_parton[2], log_std_parton[2]], # scaling for phi
                                                                              scaling_phi_decay=[log_mean_parton_Hthad[2], log_std_parton_Hthad[2]], # scaling for phi
                                                                              split=True, dtype=dtype,
                                                                              device=device)

            
            regr_loss =  (lossH + lossThad + lossTlep + lossGluon + lossBoost + lossH_angles + loss_lepton_angles + loss_tlep_b_angles + loss_thad_light_angles + loss_thad_b_angles)/25
            
            mdmm_return = MDMM_module(regr_loss, [(MMD_input, MMD_target, config.training_params.mmd_kernel, device, dtype, True, False)])

            loss_final = mdmm_return.value

            #print(f"MMD loss: {mdmm_return.fn_values}, huber loss {regr_loss.item()}, loss tot{loss_final.item()}")

            loss_final.backward()
            optimizer.step()

            with torch.no_grad():

                (mmd_loss_H,
                 mmd_loss_thad,
                 mmd_loss_tlep,
                 mmd_loss_gluon,
                 mmd_loss_boost,
                 mmd_loss_H_angles,
                 mmd_loss_lepton_angles,
                 mmd_loss_tlep_b_angles,
                 mmd_loss_thad_light_angles,
                 mmd_loss_thad_b_angles,
                 mmd_loss_all) = compute_mmd_loss(MMD_input, MMD_target,
                                                  kernel=config.training_params.mmd_kernel,
                                                  device=device, total=True, dtype=dtype, split=True)
                
                if exp is not None and device==0 or world_size is None:
                    if i % 10 == 0:
                        exp.log_metric("loss_mmd_H", mmd_loss_H.item(),step=ii)
                        exp.log_metric('loss_mmd_thad', mmd_loss_thad.item(),step=ii)
                        exp.log_metric('loss_mmd_tlep', mmd_loss_tlep.item(),step=ii)
                        exp.log_metric('loss_mmd_gluon', mmd_loss_gluon.item(),step=ii)
                        exp.log_metric('loss_mmd_boost', mmd_loss_boost.item(),step=ii)

                        exp.log_metric("loss_mmd_H_angles", mmd_loss_H_angles.item(),step=ii)
                        exp.log_metric('loss_mmd_lepton_angles', mmd_loss_lepton_angles.item(),step=ii)
                        exp.log_metric('loss_mmd_tlep_b_angles', mmd_loss_tlep_b_angles.item(),step=ii)
                        exp.log_metric('loss_mmd_thad_lightQ_angles', mmd_loss_thad_light_angles.item(),step=ii)
                        exp.log_metric('loss_mmd_thad_b_angles', mmd_loss_thad_b_angles.item(),step=ii)
                        
                        exp.log_metric('loss_mmd_all', mmd_loss_all.item(),step=ii)
                        exp.log_metric('loss_mmd_tot', (mmd_loss_H + mmd_loss_thad + mmd_loss_tlep + mmd_loss_boost + mmd_loss_gluon +  mmd_loss_all + \
                                                       mmd_loss_H_angles + mmd_loss_lepton_angles + mmd_loss_tlep_b_angles + mmd_loss_thad_light_angles + \
                                                       mmd_loss_thad_b_angles).item()/11,step=ii)

                        
                        exp.log_metric('loss_huber_H', lossH.item()/3,step=ii)
                        exp.log_metric('loss_huber_Thad', lossThad.item()/3,step=ii)
                        exp.log_metric('loss_huber_Tlep', lossTlep.item()/3,step=ii)
                        exp.log_metric('loss_huber_gluon', lossGluon.item()/3,step=ii)
                        exp.log_metric('loss_huber_boost', lossBoost.item(),step=ii)

                        exp.log_metric('loss_huber_H_angles', lossH_angles.item()/2,step=ii)
                        exp.log_metric('loss_huber_lepton_angles', loss_lepton_angles.item()/2,step=ii)
                        exp.log_metric('loss_huber_tlep_b_angles', loss_tlep_b_angles.item()/2,step=ii)
                        exp.log_metric('loss_huber_thad_lightQ_angles', loss_thad_light_angles.item()/2,step=ii)
                        exp.log_metric('loss_huber_thad_b_angles', loss_thad_b_angles.item()/2,step=ii)
                        
                        exp.log_metric('loss_huber_tot', regr_loss.item(),step=ii)
                        
                        exp.log_metric('loss_tot_train', loss_final.item(),step=ii)
                
                sum_loss += loss_final.item()

        ### END of training 
        if exp is not None and device==0 or world_size is None:
            exp.log_metric("loss_epoch_total_train", sum_loss/N_train, epoch=e, step=ii)
            exp.log_metric("learning_rate", optimizer.param_groups[0]['lr'], epoch=e, step=ii)

        
        valid_loss_huber = valid_loss_mmd = valid_lossH = valid_lossTlep = valid_lossThad = valid_lossGluon = 0.
        valid_lossBoost = valid_loss_final = valid_mmd_H = valid_mmd_thad = valid_mmd_tlep = valid_mmd_gluon = 0.
        valid_mmd_boost = valid_mmd_all = 0.
        valid_loss_H_angles = valid_loss_lepton_angles = valid_loss_tlep_b_angles = valid_loss_thad_light_angles = 0.
        valid_loss_thad_b_angles = valid_mmd_loss_H_angles = valid_mmd_loss_lepton_angles = valid_mmd_loss_tlep_b_angles = 0.
        valid_mmd_loss_thad_light_angles = valid_mmd_loss_thad_b_angles = 0.
        
        # validation loop (don't update weights and gradients)
        print("Before validation loop")
        ddp_model.eval()
        
        for i, data_batch in enumerate(validLoader):
            N_valid +=1
            # Move data to device
            with torch.no_grad():

                (logScaled_partons,
                 mask_partonsLeptons,
                 logScaled_parton_boost,
                 logScaled_reco_sortedBySpanet, mask_recoParticles,
                 mask_boost_reco, data_boost_reco) = data_batch
                                
                # exist + 3-mom
                logScaled_reco_sortedBySpanet = logScaled_reco_sortedBySpanet[:,:,[0,1,2,3]]
                # The provenance is remove in the model
    
                # put the lepton first:
                logScaled_reco_sortedBySpanet = logScaled_reco_sortedBySpanet[:,new_order,:]
                mask_recoParticles = mask_recoParticles[:,new_order]
    
                # same order for partons:
                logScaled_partons = logScaled_partons[:,partons_order,:]
                mask_partonsLeptons = mask_partonsLeptons[:,partons_order]
    
                # attach 1 hot-encoded position
                logScaled_reco_sortedBySpanet = logScaled_reco_sortedBySpanet
    
                # remove prov from partons
                logScaled_partons = logScaled_partons[:,:,[0,1,2,3,4]] # [pt,eta,phi,parton_id, type] -> skip type=1/2 for partons/leptons
    
                intermediate_propagators, boost_scaled, decay_angles = ddp_model(logScaled_reco_sortedBySpanet, data_boost_reco, mask_recoParticles, mask_boost_reco, No_regressed_vars=9, sin_cos_reco=pos_logScaledReco, sin_cos_partons=pos_partons)
            
                higgs = intermediate_propagators[:,0] 
                thad = intermediate_propagators[:,1]
                tlep = intermediate_propagators[:,2]
                
                higgs_b_angles = decay_angles[:,0]
                lepton_angles = decay_angles[:,1]
                tlep_b_angles = decay_angles[:,2]
                thad_light_angles = decay_angles[:,3]
                thad_b_angles = decay_angles[:,4]
    
                out = [higgs, thad, tlep, boost_scaled]
                
                data_regressed, boost_regressed = Compute_ParticlesTensor.get_HttISR_fromlab(out, log_mean_parton,
                                                                      log_std_parton,
                                                                      log_mean_boost_parton, log_std_boost_parton,
                                                                      device, cartesian=False, eps=0.0)
    
                boost_notscaled = boost_regressed[:, [0,-1]]
                boost = boost_notscaled.clone()
                boost[:,0] = torch.log(boost_notscaled[:,0] + 1)
                boost = (boost - log_mean_boost_parton)/log_std_boost_parton
    
                gluon_toscale = data_regressed[:,3] #pt, eta, phi
                gluon = gluon_toscale.clone()
                gluon[:,0] = torch.log(gluon_toscale[:,0] + 1) # No need for abs and sign
                gluon = (gluon - log_mean_parton) / log_std_parton
                            
                MMD_input = [higgs, thad, tlep, gluon, boost, higgs_b_angles, lepton_angles, tlep_b_angles, thad_light_angles, thad_b_angles]
                
                MMD_target = [logScaled_partons[:,-3, :3],
                              logScaled_partons[:,-2, :3],
                              logScaled_partons[:,-1, :3],
                              logScaled_partons[:,-4, :3],
                              logScaled_parton_boost,
                              logScaled_partons[:,2,1:3],
                              logScaled_partons[:,0,1:3],
                              logScaled_partons[:,7,1:3],
                              logScaled_partons[:,5,1:3],
                              logScaled_partons[:,4,1:3]]

                lossH, lossThad, lossTlep, lossGluon, lossBoost, \
                lossH_angles, loss_lepton_angles, loss_tlep_b_angles, \
                loss_thad_light_angles, loss_thad_b_angles =  compute_regr_losses(logScaled_partons,
                                                                                  logScaled_parton_boost,
                                                                                  higgs, thad, tlep, gluon, boost,
                                                                                  higgs_b_angles, lepton_angles, tlep_b_angles, thad_light_angles, thad_b_angles,
                                                                                  config.cartesian, loss_fn,
                                                                                  scaling_phi_intermediate=[log_mean_parton[2], log_std_parton[2]], # scaling for phi
                                                                                  scaling_phi_decay=[log_mean_parton_Hthad[2], log_std_parton_Hthad[2]], # scaling for phi
                                                                                  split=True, dtype=dtype,
                                                                                  device=device)
    
                
                regr_loss =  (lossH + lossThad + lossTlep + lossGluon + lossBoost + lossH_angles + loss_lepton_angles + loss_tlep_b_angles + loss_thad_light_angles + loss_thad_b_angles)/25
                
                mdmm_return = MDMM_module(regr_loss, [(MMD_input, MMD_target, config.training_params.mmd_kernel, device, dtype, True, False)])
    
                loss_final = mdmm_return.value

                (mmd_loss_H,
                 mmd_loss_thad,
                 mmd_loss_tlep,
                 mmd_loss_gluon,
                 mmd_loss_boost,
                 mmd_loss_H_angles,
                 mmd_loss_lepton_angles,
                 mmd_loss_tlep_b_angles,
                 mmd_loss_thad_light_angles,
                 mmd_loss_thad_b_angles,
                 mmd_loss_all) = compute_mmd_loss(MMD_input, MMD_target,
                                                  kernel=config.training_params.mmd_kernel,
                                                  device=device, total=True, dtype=dtype, split=True)

                
                mmd_loss = (mmd_loss_H + mmd_loss_thad + mmd_loss_tlep + mmd_loss_boost + mmd_loss_gluon +  mmd_loss_all + \
                           mmd_loss_H_angles + mmd_loss_lepton_angles + mmd_loss_tlep_b_angles + mmd_loss_thad_light_angles + \
                           mmd_loss_thad_b_angles).item()/11
                                

                valid_loss_huber += regr_loss.item()
                valid_lossH += lossH.item()/3
                valid_lossTlep += lossTlep.item()/3
                valid_lossThad += lossThad.item()/3
                valid_lossBoost += lossBoost.item()
                valid_lossGluon += lossGluon.item()/3
                valid_loss_mmd += mmd_loss.item()
                valid_mmd_H += mmd_loss_H.item()
                valid_mmd_thad += mmd_loss_thad.item()
                valid_mmd_tlep += mmd_loss_tlep.item()
                valid_mmd_gluon += mmd_loss_gluon.item()
                valid_mmd_boost += mmd_loss_boost.item()
                valid_mmd_all += mmd_loss_all.item()
                valid_loss_final += regr_loss.item()   # using only the main loss, not MDMM
                
                valid_loss_H_angles += lossH_angles.item()/2
                valid_loss_lepton_angles += loss_lepton_angles.item()/2
                valid_loss_tlep_b_angles += loss_tlep_b_angles.item()/2
                valid_loss_thad_light_angles += loss_thad_light_angles.item()/2
                valid_loss_thad_b_angles += loss_thad_b_angles.item()/2
                valid_mmd_loss_H_angles += mmd_loss_H_angles.item()
                valid_mmd_loss_lepton_angles += mmd_loss_lepton_angles.item()
                valid_mmd_loss_tlep_b_angles += mmd_loss_tlep_b_angles.item()
                valid_mmd_loss_thad_light_angles += mmd_loss_thad_light_angles.item()
                valid_mmd_loss_thad_b_angles += mmd_loss_thad_b_angles.item()
                
                

                particle_list_predicted = MMD_input[:4] # higgs, thad, tlep, gluon
                particle_list_target = MMD_target[:4]

                boost_predicted = MMD_input[4] # boost
                boost_target = MMD_target[4]

                decay_particle_list_predicted = MMD_input[5:] # decay products
                decay_particle_list_target = MMD_target[5:]
                                
                if i == 0 and ( exp is not None and device==0 or world_size is None):
                    for particle in range(len(particle_list_predicted)): # 4 or 3 particles: higgs/thad/tlep/gluonISR
                        
                        # 4 or 3 features
                        for feature in range(3):  
                            fig, ax = plt.subplots(figsize=(7,6), dpi=100)
                            h = ax.hist2d(particle_list_target[particle][:,feature].detach().cpu().numpy().flatten(),
                                          particle_list_predicted[particle][:,feature].cpu().detach().numpy().flatten(),
                                          bins=40, range=((-3, 3),(-3, 3)), cmin=1)
                            fig.colorbar(h[3], ax=ax)
                            exp.log_figure(f"particle_2D_{particle}_{feature}", fig,step=e)

                    
                            fig, ax = plt.subplots(figsize=(7,6), dpi=100)
                            ax.hist(particle_list_target[particle][:,feature].detach().cpu().numpy().flatten(),
                                          bins=30, range=(-2, 2), label="truth", histtype="step")
                            ax.hist(particle_list_predicted[particle][:,feature].cpu().detach().numpy().flatten(),
                                          bins=30, range=(-2, 2), label="regressed",histtype="step")
                            ax.legend()
                            ax.set_xlabel(f"particle {particle} feature {feature}")
                            exp.log_figure(f"particle_1D_{particle}_{feature}", fig, step=e)

                    ranges_boost = [(-3,3),(-2,2)]
                    for feature in range(2):
                        fig, ax = plt.subplots(figsize=(7,6), dpi=100)
                        h = ax.hist2d(boost_target[:,feature].detach().cpu().numpy(),
                                      boost_predicted[:,feature].cpu().detach().numpy().flatten(),
                                      bins=40, range=(ranges_boost[feature],ranges_boost[feature]), cmin=1)
                        fig.colorbar(h[3], ax=ax)
                        exp.log_figure(f"boost_2D_{feature}", fig,step=e)


                        fig, ax = plt.subplots(figsize=(7,6), dpi=100)
                        ax.hist(boost_target[:,feature].detach().cpu().numpy().flatten(),
                                      bins=30, range=ranges_boost[feature], label="truth", histtype="step")
                        ax.hist(boost_predicted[:,feature].cpu().detach().numpy().flatten(),
                                      bins=30, range=ranges_boost[feature], label="regressed",histtype="step")
                        ax.legend()
                        ax.set_xlabel(f"boost feature {feature}")
                        exp.log_figure(f"boost_1D_{feature}", fig, step=e)

                    for particle in range(len(decay_particle_list_predicted)): # 5 particles

                        for feature in range(2):
                            fig, ax = plt.subplots(figsize=(7,6), dpi=100)
                            h = ax.hist2d(decay_particle_list_target[particle][:,feature].detach().cpu().numpy().flatten(),
                                          decay_particle_list_predicted[particle][:,feature].cpu().detach().numpy().flatten(),
                                          bins=40, range=((-3, 3),(-3, 3)), cmin=1)
                            fig.colorbar(h[3], ax=ax)
                            exp.log_figure(f"decay_particle_2D_{particle}_{feature}", fig,step=e)

                    
                            fig, ax = plt.subplots(figsize=(7,6), dpi=100)
                            ax.hist(decay_particle_list_target[particle][:,feature].detach().cpu().numpy().flatten(),
                                          bins=30, range=(-2, 2), label="truth", histtype="step")
                            ax.hist(decay_particle_list_predicted[particle][:,feature].cpu().detach().numpy().flatten(),
                                          bins=30, range=(-2, 2), label="regressed",histtype="step")
                            ax.legend()
                            ax.set_xlabel(f"decay_particle {particle} feature {feature}")
                            exp.log_figure(f"decay_particle_1D_{particle}_{feature}", fig, step=e)


        if exp is not None and device==0 or world_size is None:
            exp.log_metric("loss_total_val", valid_loss_final/N_valid, epoch=e )
            exp.log_metric("loss_mmd_val", valid_loss_mmd/N_valid,epoch= e)
            exp.log_metric('loss_huber_val', valid_loss_huber/N_valid,epoch= e)
            exp.log_metric('loss_huber_val_H', valid_lossH/N_valid,epoch= e)
            exp.log_metric('loss_huber_val_Tlep', valid_lossTlep/N_valid,epoch= e)
            exp.log_metric('loss_huber_val_Thad', valid_lossThad/N_valid,epoch= e)
            exp.log_metric('loss_huber_val_Gluon', valid_lossGluon/N_valid,epoch= e)
            exp.log_metric('loss_huber_val_boost', valid_lossBoost/N_valid,epoch= e)
            exp.log_metric("loss_mmd_val_H", valid_mmd_H/N_valid,epoch= e)
            exp.log_metric("loss_mmd_val_thad", valid_mmd_thad/N_valid,epoch= e)
            exp.log_metric("loss_mmd_val_tlep", valid_mmd_tlep/N_valid,epoch= e)
            exp.log_metric("loss_mmd_val_gluon", valid_mmd_gluon/N_valid,epoch= e)
            exp.log_metric("loss_mmd_val_boost", valid_mmd_boost/N_valid,epoch= e)
            exp.log_metric("loss_mmd_val_all", valid_mmd_all/N_valid,epoch= e)

            exp.log_metric('loss_huber_val_H_angles', valid_loss_H_angles/N_valid,epoch= e)
            exp.log_metric('loss_huber_val_lepton_angles', valid_loss_lepton_angles/N_valid,epoch= e)
            exp.log_metric('loss_huber_val_tlep_b_angles', valid_loss_tlep_b_angles/N_valid,epoch= e)
            exp.log_metric('loss_huber_val_thad_light_angles', valid_loss_thad_light_angles/N_valid,epoch= e)
            exp.log_metric('loss_huber_val_thad_b_angles', valid_loss_thad_b_angles/N_valid,epoch= e)
            exp.log_metric("loss_mmd_val_H_angles", valid_mmd_loss_H_angles/N_valid,epoch= e)
            exp.log_metric("loss_mmd_val_lepton_angles", valid_mmd_loss_lepton_angles/N_valid,epoch= e)
            exp.log_metric("loss_mmd_val_tlep_b_angles", valid_mmd_loss_tlep_b_angles/N_valid,epoch= e)
            exp.log_metric("loss_mmd_val_thad_light_angles", valid_mmd_loss_thad_light_angles/N_valid,epoch= e)
            exp.log_metric("loss_mmd_val_thad_b_angles", valid_mmd_loss_thad_b_angles/N_valid,epoch= e)

        if device == 0 or world_size is None:
            if early_stopper.early_stop(valid_loss_final/N_valid,
                                    model.state_dict(), optimizer.state_dict(), modelName, exp):
                print(f"Model converges at epoch {e} !!!")         
                break

        # Step the scheduler at the end of the val
        # after_N_epochs = config.training_params.cosine_scheduler.get("after_N_epochs", 0)
        # if e > after_N_epochs:
        #     scheduler.step()
        
        scheduler.step(valid_loss_final/N_valid) # reduce lr if the model is not improving anymore
        

    # writer.close()
    # exp_log.end()
    destroy_process_group()
    print('preTraining finished!!')
    

if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument('--path-config', type=str, required=True, help='path to config.yaml File')
    parser.add_argument('--output-dir', type=str, required=True, help='Output directory')
    parser.add_argument('--on-GPU', action="store_true",  help='run on GPU boolean')
    parser.add_argument('--distributed', action="store_true")
    args = parser.parse_args()
    
    path_to_conf = args.path_config
    on_GPU = args.on_GPU # by default run on CPU
    outputDir = args.output_dir

    # Read config file in 'conf'
    with open(path_to_conf) as f:
        conf = OmegaConf.load(path_to_conf)
    
    print("Training with cfg: \n", OmegaConf.to_yaml(conf))

    env_var = os.environ.get("CUDA_VISIBLE_DEVICES")
    if env_var:
        actual_devices = env_var.split(",")
    else:
        actual_devices = list(range(torch.cuda.device_count()))
    print("Actual devices: ", actual_devices)
    world_size = len(actual_devices)

    
    outputDir = os.path.abspath(outputDir)
    latentSpace = conf.conditioning_transformer.use_latent
    name_dir = f'{outputDir}/preTraining_Unfolding_NewVersion_{conf.name}_{conf.version}_hiddenFeatures:{conf.conditioning_transformer.hidden_features}_dimFeedForward:{conf.conditioning_transformer.dim_feedforward_transformer}_nheadEnc:{conf.conditioning_transformer.nhead_encoder}_LayersEnc:{conf.conditioning_transformer.no_layers_encoder}_nheadDec:{conf.conditioning_transformer.nhead_decoder}_LayersDec:{conf.conditioning_transformer.no_layers_decoder}'

    os.makedirs(name_dir, exist_ok=True)
    
    with open(f"{name_dir}/config_{conf.name}_{conf.version}.yaml", "w") as fo:
        fo.write(OmegaConf.to_yaml(conf)) 

    if conf.training_params.dtype == "float32":
        dtype = torch.float32
    elif conf.training_params.dtype == "float64":
        dtype = torch.float64
        
    
    if args.distributed:
        
        # make a dictionary with k: rank, v: actual device
        dev_dct = {i: actual_devices[i] for i in range(world_size)}
        print(f"Devices dict: {dev_dct}")
        mp.spawn(
            train,
            args=(name_dir, conf,  outputDir, dtype,
                    world_size, dev_dct),
            nprocs=world_size,
            # join=True
        )
    else:
        device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
        train(device,name_dir, conf,  outputDir, dtype)
    
    print(f"preTraining finished succesfully! Version: {conf.version}")
    
    
    
    
    
    
    
    
    
