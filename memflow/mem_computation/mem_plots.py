import pickle
import torch.nn as nn
import torch
import numpy as np

import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import matplotlib as mpl
from matplotlib import cm

import mplhep as hep
plt.style.use(hep.style.CMS)

def hist1d_var(tensor, log=True, label='label', range=None, bins=100, save_dir=None):

    #plt.hist(tensor, bins=bins, range=range)
    plt.hist(tensor, bins=bins)
    plt.xlabel(label)
    if log:
        plt.yscale('log')

    if save_dir != None:
        plt.savefig(save_dir)

    plt.close()

def hist2d_var(tensor_1, tensor_2, log=True, cmin=1e-10, label_1='label', label_2='label', range=None, bins=100, save_dir=None):

    plt.hist2d(tensor_1, tensor_2, range=range, bins=bins, cmin=cmin if not log else None, cmap="viridis", norm=LogNorm() if log else None);

    plt.xlabel(label_1)
    plt.ylabel(label_2)

    if save_dir != None:
        plt.savefig(save_dir)

    plt.close()



def plot_profile1d_max(list_arrays, list_labels, log=True, bins=100, save_dir=None):


    for i, array in enumerate(list_arrays):
        plt.hist(np.max(array) / array, bins=bins, label=list_labels[i], histtype='step');
        plt.xlabel('MAX[log(samples)] / log(samples)', labelpad=20)

    if log:
        plt.yscale('log')

    plt.legend()
    if save_dir != None:
        plt.savefig(save_dir)

    plt.close()


def plot_var(tensor, event, var, range_var=(-1000,1000), label='En', figSize=15, save_dir=None):
    f, axs = plt.subplots(4, 4, figsize=(figSize, figSize),dpi=100)
    plt.subplots_adjust(wspace=0.2, hspace=0.12)
    # py
    inputs = [tensor[event,1:,0,var], tensor[event,1:,1,var],
              tensor[event,1:,2,var], tensor[event,1:,3,var]]
    
    truth = [tensor[event,0,0,var], tensor[event,0,1,var],
             tensor[event,0,2,var], tensor[event,0,3,var]]
    
    labels = [f"Top {label}", f"Anti top {label}", f"Higgs {label}", f"Gluon {label}"]
    
    for i in range(4):
        for j in range(4):
            if j > i :
                axs[i][j].remove()
                continue 
    
            if i!=j:
                axs[i][j].hist2d(inputs[j],inputs[i], range=(range_var,range_var), bins=100, cmin=1, cmap="viridis");
                axs[i][j].plot(truth[j], truth[i], marker="X", markersize=15,  markeredgecolor="red", markerfacecolor="red", label="True")
    
            if i==j:
                axs[i][j].hist(inputs[i], range=range_var, bins=50, histtype="step", label="Samples")
                axs[i][j].axvline(truth[i], c="r", label="True")
                axs[i][j].legend()
    
            if i == 3:
                axs[i][j].set_xlabel(labels[j])
            
            if i==0 and j==0:
                hep.cms.label(rlabel="", llabel="Simulation Preliminary", loc=0, ax=axs[i][j],fontsize=24)
            if i==0 and j==3:
                hep.cms.label(rlabel="13 TeV", llabel="", loc=0, ax=axs[i][j],fontsize=24, exp="")
                axs[i][j].legend()
            
            if j == 0:
                axs[i][j].set_ylabel(labels[i])

    if save_dir != None:
        f.savefig(save_dir)

    plt.close(f)



def plot_var_weight(tensor, event, var, range_var=(-1000,1000), weights=0, label='En', figSize=25, log=False, bins=100, save_dir=None):
    f, axs = plt.subplots(4, 4, figsize=(figSize, figSize),dpi=100)
    plt.subplots_adjust(wspace=0.2, hspace=0.12)
    # py
    inputs = [tensor[event,1:,0,var], tensor[event,1:,1,var],
              tensor[event,1:,2,var], tensor[event,1:,3,var]]
    
    truth = [tensor[event,0,0,var], tensor[event,0,1,var],
             tensor[event,0,2,var], tensor[event,0,3,var]]
    
    labels = [f"Top {label}", f"Anti top {label}", f"Higgs {label}", f"Gluon {label}"]
    
    for i in range(4):
        for j in range(4):
            if j > i :
                axs[i][j].remove()
                continue 
    
            if i!=j:
                axs[i][j].hist2d(inputs[j],inputs[i], range=(range_var,range_var), bins=bins, cmin=1e-10 if not log else None, cmap="viridis", weights=weights, norm=LogNorm() if log else None);
                axs[i][j].plot(truth[j], truth[i], marker="X", markersize=15,  markeredgecolor="red", markerfacecolor="red", label="True")
    
            if i==j:
                axs[i][j].hist(inputs[i], range=range_var, bins=bins, histtype="step", label="Samples", weights=weights)
                axs[i][j].axvline(truth[i], c="r", label="True")
                if log:
                    axs[i][j].set_yscale('log')
                axs[i][j].legend()
    
            if i == 3:
                axs[i][j].set_xlabel(labels[j])
            
            if i==0 and j==0:
                hep.cms.label(rlabel="", llabel="Simulation Preliminary", loc=0, ax=axs[i][j],fontsize=24)
            if i==0 and j==3:
                hep.cms.label(rlabel="13 TeV", llabel="", loc=0, ax=axs[i][j],fontsize=24, exp="")
                axs[i][j].legend()
            
            if j == 0:
                axs[i][j].set_ylabel(labels[i])

    if save_dir != None:
        f.savefig(save_dir)

    plt.close(f)



def plot_MEM_fewEvents(MEM_PerRecoEvent, std_MEM_PerRecoEvent, save_dir=None):
    barWidth = 0.25
    fig, ax = plt.subplots(figsize =(15, 8)) 
    
    br1 = np.arange(len(MEM_PerRecoEvent)) 
    br2 = [x + barWidth for x in br1] 
    br3 = [x + barWidth for x in br2] 
    
    ax.bar(br1, MEM_PerRecoEvent, color ='r', width = barWidth, 
            edgecolor ='grey', label ='estimate') 
    ax.bar(br2, std_MEM_PerRecoEvent, color ='g', width = barWidth, 
            edgecolor ='grey', label ='std')
    
    ax.set_ylabel('Value', fontweight ='bold', fontsize = 15) 
    ax.set_yscale('log')
    ax.set_xticks([r + barWidth for r in range(len(MEM_PerRecoEvent))], 
            [f'event {i}' for i in range(len(MEM_PerRecoEvent))])
    
    ax.legend()

    if save_dir != None:
        fig.savefig(save_dir + '/MEM_Result_Std.png')
    
    fig, ax = plt.subplots(figsize =(15, 8)) 
    
    ax.set_ylabel('Value [%]')  # we already handled the x-label with ax1
    ax.bar(br1, std_MEM_PerRecoEvent / MEM_PerRecoEvent, color ='b', width = barWidth, 
            edgecolor ='grey', label ='std / estimate') 
    
    ax.set_xticks([r + barWidth for r in range(len(MEM_PerRecoEvent))], 
            [f'event {i}' for i in range(len(MEM_PerRecoEvent))])
    
    ax.legend()  # otherwise the right y-label is slightly clipped

    if save_dir != None:
        fig.savefig(save_dir + '/MEM_RelativeStd.png')


    plt.close(fig)


def banana_plots(array_1, array_2, bins=100, label_1='label', label_2='label', save_dir=None):

    out = plt.hist2d(array_1, array_2, bins=bins, norm=LogNorm())
    plt.xlabel(label_1)
    plt.ylabel(label_2)
    
    mean_bin_x = []
    std_bin_x = []
    
    for i in range(len(out[1]) - 1):
        mask_lower = array_1 > out[1][i]
        mask_higher = array_1 < out[1][i + 1]
    
        array_2_currentBin = array_2 [ (mask_lower & mask_higher) ]
        if len(array_2_currentBin) == 0:
            mean_bin_x.append(0)
            std_bin_x.append(0)
        else:
            mean_bin_x.append(np.mean(array_2_currentBin))
            std_bin_x.append(np.std(array_2_currentBin))

    plt.plot(out[1][:-1], mean_bin_x, c='blue', label='mean')
    plt.plot(out[1][:-1], [mean_bin_x[i] + std_bin_x[i] for i in range(len(mean_bin_x))], c='red', label='1 $\sigma$')
    plt.plot(out[1][:-1], [mean_bin_x[i] - std_bin_x[i] for i in range(len(mean_bin_x))], c='red')
    
    plt.plot(out[1][:-1], [mean_bin_x[i] + 2*std_bin_x[i] for i in range(len(mean_bin_x))], c='black', label='2 $\sigma$')
    plt.plot(out[1][:-1], [mean_bin_x[i] - 2*std_bin_x[i] for i in range(len(mean_bin_x))], c='black')
    plt.legend(loc=3, prop={'size': 14})
    
    if save_dir != None:
        plt.savefig(save_dir)

    plt.close()