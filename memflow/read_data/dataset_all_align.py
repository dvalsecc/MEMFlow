import torch
from torch.utils.data import Dataset, DataLoader
from itertools import chain
from operator import itemgetter

from os import listdir
from os.path import isfile, join

class DatasetCombined_align(Dataset):
    def __init__(self, root, dev=None, dtype=None):

        onlyfiles = [f for f in listdir(root) if isfile(join(root, f))]
        if any("full" in s for s in onlyfiles):
            matching = [s for s in onlyfiles if "full" in s]
            onlyFiles = list(set(onlyfiles) - set(matching))

        print(onlyFiles)
        print()

        # remove some substrings from strings in list
        onlyFiles_goodNames = list(map(lambda st: str.replace(st, ".pt", ""), onlyFiles))
        self.nameAttribs = list(map(lambda st: str.replace(st, "alignTFwithUnfolding_trainData_", ""), onlyFiles_goodNames))
        print(self.nameAttribs)
        print()

        path_tensors_files = [root + '/' + fileName for fileName in onlyFiles]

        #print(path_tensors_files)
        
        for attr, value in zip(self.nameAttribs, path_tensors_files):
            setattr(self, attr, torch.load(value))

        self.dtype = dtype
        self.dev = dev

    @property
    def raw_file_names(self):
        return [self.root]

    def __getitem__(self, index):
        
        return [getattr(self, field).to(self.dtype).to(self.dev) for field in self.nameAttribs if 'mean' not in field and 'std' not in field]

    def __len__(self):
        size = len(getattr(self, self.nameAttribs[0]))
        return size
