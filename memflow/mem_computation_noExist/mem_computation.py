import pickle
import torch.nn as nn
import torch
import numpy as np
from memflow.unfolding_flow.utils import Compute_ParticlesTensor as particle_tools
import os

from pdfflow import mkPDFs
import tensorflow as tf

def evaluate_pdf(flavours, x1_x2_samples):

    # type of first parton
    flavours_0 = tf.cast([flav[0] for flav in flavours], tf.int32)
    # type of 2nd parton
    flavours_1 = tf.cast([flav[1] for flav in flavours], tf.int32)

    No_RecoEvents = x1_x2_samples.shape[0]
    No_PartonSamples = x1_x2_samples.shape[1]
    
    # pdf library
    pdf = mkPDFs('NNPDF31_nnlo_as_0118_mc_hessian_pdfas', [0]) # TODO --> check what this member is
    
    En_proton_squared = tf.ones([No_RecoEvents * No_PartonSamples], dtype=tf.float64) * 6500**2
    
    x1_x2_samples_tf = tf.convert_to_tensor(x1_x2_samples)
    x1_x2_samples_tf_reshaped = tf.reshape(x1_x2_samples_tf, [-1, 2])
    
    f1 = pdf.xfxQ2(flavours_0, x1_x2_samples_tf_reshaped[:,0], En_proton_squared)
    f2 = pdf.xfxQ2(flavours_1, x1_x2_samples_tf_reshaped[:,1], En_proton_squared)
    
    f1 = tf.reshape(f1, [No_RecoEvents, No_PartonSamples, 31]).numpy()
    f2 = tf.reshape(f2, [No_RecoEvents, No_PartonSamples, 31]).numpy()

    return f1, f2



# meanRecoParticles = [ 4.3282e+00, -3.5510e-03,  1.0714e-02] --> to unscale jets
# stdRecoParticles = [0.5821, 0.9797, 1.8153] --> to unscale jets
# We PASS jets already unscaled
def mem_computation(unscaled_jetsPt, unscaled_PartonsMomenta, LogProbSamples, x1_x2_samples, mask_ExistReco,
                    flavours,
                    path_ME_evaluation,
                    evaluate_ME=['SM', 'lin', 'quad'],
                    objects_ME_evaluation=None,
                    std_pt_Jets=0.5822, std_eta_Jets=0.9795,  # std for logScaled_jets_OrderBySPANET
                    scaling_jac = [ 9.8929, 10.0761, 10.1887,  9.0733, 10.9712,  9.0767, 13.9509,  9.0681, 3.5302,  3.6533], # std for logit(Rambo) transformation
                    Calibration_dir=None, NoJets_total=9, NoJets_classifier=5, No_maxJets_cut=100):


    if len(path_ME_evaluation) != len(evaluate_ME):
        raise Exception('Shape mismatch between path_ME_evaluation and evaluate_ME')
    
    # Take only 1: since the first event is the MC target
    log_prob_kin_perObj = LogProbSamples[:,1:,3: 3+NoJets_total] # kinematics for all NoJets_total
    log_prob_classifier_perObj = LogProbSamples[:,1:, 3+NoJets_total:] # should be NoJets_classifier
    jacobians_partonSpace = LogProbSamples[:,1:,:3] # first 3 = jacobians: going from Unfolding sampling space to the parton space

    if log_prob_classifier_perObj.shape[2] != NoJets_classifier:
        raise Exception('No jets or No jets classified are wrong')

    # compute TF_perEv
    TF_perEv = torch.zeros((log_prob_kin_perObj.shape[:2]))

    # NO CLASSIFIER
    if True:
        for i in range(NoJets_total):
            if i >= No_maxJets_cut:
                break
            if i < NoJets_total - NoJets_classifier:
                TF_perEv += torch.log(1. / (std_pt_Jets * std_eta_Jets) / unscaled_jetsPt[:,i,None]) + log_prob_kin_perObj[:,:,i]
               
            else:
                TF_perEv += torch.log(1. / (std_pt_Jets * std_eta_Jets) / unscaled_jetsPt[:,i,None]) + log_prob_kin_perObj[:,:,i]
               

    else:
        TF_perEv = torch.ones((log_prob_kin_perObj.shape[:2]))
        for i in range(NoJets_total):
            if i < NoJets_total - NoJets_classifier:
                TF_perEv *= 1. / (std_pt_Jets * std_eta_Jets) * torch.exp(log_prob_kin_perObj[:,:,i]) / unscaled_jetsPt[:,i,None]
            else:
                # / (std_pt_Jets * std_eta_Jets) / unscaled_jetsPt[:,i,None]
                TF_perEv *= 1. / (std_pt_Jets * std_eta_Jets) * torch.exp(log_prob_kin_perObj[:,:,i]) / unscaled_jetsPt[:,i,None]

                  
    TF_perEv = torch.exp(TF_perEv)

    if False:
        if torch.count_nonzero(TF_perEv == 0.0) > 0:
            raise Exception('TF is 0 for some events --> Check them')


    parton_kinematics = unscaled_PartonsMomenta[:,1:].numpy().astype(np.float64)
    TF_perEv = TF_perEv.numpy()
    jacobians_partonSpace = jacobians_partonSpace.numpy()
    x1_x2_samples_numpy = x1_x2_samples[:,1:].numpy()

    No_RecoEvents = TF_perEv.shape[0]
    No_PartonSamples = TF_perEv.shape[1]
    
    scaling_jacobian = np.prod(scaling_jac)

    # log_prob_samples / (rambo_weight * sigmoid_jacobian * std_scaling)
    fullSample_prob = (jacobians_partonSpace[...,0] / (np.abs(jacobians_partonSpace[...,2]) * np.abs(jacobians_partonSpace[...,1]) * scaling_jacobian)) 

    parton_kinematics_reshaped = parton_kinematics.reshape(-1, 6, 4)
    ME = []

    for i, evaluation in enumerate(evaluate_ME):
        print(evaluation)
        if evaluation == 'SM':
            os.chdir(path_ME_evaluation[i])
            ME_SM = objects_ME_evaluation[i].compute_mem_old(parton_kinematics_reshaped)
            ME_SM_correctShape = ME_SM.reshape(No_RecoEvents, No_PartonSamples, 31)
            ME.append( ME_SM_correctShape )
            
            if np.all(ME_SM_correctShape[0,0] != ME_SM[0]) or np.all(ME_SM_correctShape[0,1] != ME_SM[1]) or \
                np.all(ME_SM_correctShape[1,0] != ME_SM[No_PartonSamples]) or np.all(ME_SM_correctShape[1,1] != ME_SM[No_PartonSamples+1]):
                    raise Exception ('Reshape done wrong')
            

        elif evaluation == 'lin':
            os.chdir(path_ME_evaluation[i])
            ME_lin = objects_ME_evaluation[i].compute_mem_old(parton_kinematics_reshaped)
            ME.append( ME_lin.reshape(No_RecoEvents, No_PartonSamples, 31) )
            

        elif evaluation == 'quad':
            os.chdir(path_ME_evaluation[i])
            ME_quad = objects_ME_evaluation[i].compute_mem(parton_kinematics_reshaped)
            ME.append( ME_quad.reshape(No_RecoEvents, No_PartonSamples, 31) )


    pdf_f1, pdf_f2 = evaluate_pdf(flavours, x1_x2_samples_numpy)

    s = 13000

    ME_PerPartonSample_list = []; MEandPDF_PerPartonSample_list = []; MEandTF_PerPartonSample_list = []; TFandProbSample_list = [];
    MEM_PerPartonSample_list = []; MEM_PerRecoEvent_list = []; std_MEM_PerRecoEvent_list = [];


    # first element: SM, 2nd: lin, 3rd: quad
    for ME_component in ME:

        ME_PerPartonEvent = np.sum(ME_component, axis=-1)
        MEandPDF_PerPartonEvent = np.sum(ME_component * pdf_f1 * pdf_f2, axis=-1)
        
        ME_PerPartonSample_list.append( ME_PerPartonEvent ) 
        MEandPDF_PerPartonSample_list.append( MEandPDF_PerPartonEvent )
        MEandTF_PerPartonSample_list.append( ME_PerPartonEvent * TF_perEv )
        TFandProbSample_list.append( TF_perEv / fullSample_prob )

        FullMEM_PerPartonSample = (1. /(2 * s**2 * x1_x2_samples_numpy[...,0] * x1_x2_samples_numpy[...,1])) * MEandPDF_PerPartonEvent * TF_perEv / fullSample_prob

        MEM_PerPartonSample_list.append( FullMEM_PerPartonSample )

        MEM_PerRecoEvent_list.append( np.sum(FullMEM_PerPartonSample, axis=1) / No_PartonSamples )
        std_MEM_PerRecoEvent_list.append( np.std(FullMEM_PerPartonSample, axis=1) / np.sqrt(No_PartonSamples) )        

    return [ME_PerPartonSample_list, MEandPDF_PerPartonSample_list, TF_perEv, fullSample_prob, MEandTF_PerPartonSample_list, TFandProbSample_list, MEM_PerPartonSample_list, MEM_PerRecoEvent_list, std_MEM_PerRecoEvent_list]


