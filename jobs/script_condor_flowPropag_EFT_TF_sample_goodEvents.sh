#!/bin/bash

#creating venv in local job dir
python -m venv myenv --system-site-packages
source myenv/bin/activate

cd $1
pip install -e .


python scripts/run_flow_propag_sampling_EFT_TF_goodEvents.py --path-model $2 --path-transfer-flow $3 --on-GPU --chunk 0 --test
