#!/bin/bash

#creating venv in local job dir
python -m venv myenv --system-site-packages
source myenv/bin/activate

cd $1
pip install -e .

python scripts/TransferFlow_propagators_Sep24/run_transferFlow_paperVersion_Propag_autoreg_latentSpace_gaussian_v4_sampling.py \
      --path-model $2 --on-GPU $3