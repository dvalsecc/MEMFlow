#!/bin/bash

#creating venv in local job dir
python -m venv myenv --system-site-packages
source myenv/bin/activate

cd $1
pip install -e .

python scripts/Unfolding_basicStrategy/run_flowModel_LikelihoodLoss_withRegr_alignTF.py \
      --path-config  "$1/$2" --output-dir  $3 --path-regression $4 --on-GPU $5