#!/bin/bash

#creating venv in local job dir
python -m venv myenv --system-site-packages
source myenv/bin/activate

cd $1
pip install -e .

a=$(( $4*10 ))
echo $a

for i in $(seq 0 10);
do
    echo $i
    chunk=$((a + i))
    echo chunk
    echo $chunk
    echo "$chunk"
    python scripts/run_flow_propag_sampling_EFT_TF.py --path-model $2 --path-transfer-flow $3 --on-GPU --chunk "$chunk"
done