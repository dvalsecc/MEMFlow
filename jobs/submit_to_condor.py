import htcondor
import sys
import argparse
import os
import yaml

parser = argparse.ArgumentParser()
parser.add_argument('--model', type=str, required=True)
parser.add_argument('--version', type=str, required=True)
parser.add_argument('--path-configFile', type=str, required=True) # should be ./path-config.yaml
parser.add_argument('--dry', action="store_true")
parser.add_argument('--interactive', action="store_true")
parser.add_argument('--ngpu', type=int, default=1)
parser.add_argument('--ncpu', type=int, default=3)
parser.add_argument('--path-model', type=str, help='path to model directory')
parser.add_argument('--path-TF', type=str, help='path to TF directory')
parser.add_argument("--good-gpus", action="store_true")
parser.add_argument("--good-gpus-a100", action="store_true")
parser.add_argument("--good-gpus-v100", action="store_true")
parser.add_argument('--chunk', type=int, default=0)
parser.add_argument("--args", nargs="+", type=str, help="additional args")
args = parser.parse_args()

model = args.model
version = args.version
dry = args.dry
interactive = args.interactive
path_to_conf = args.path_configFile
path_to_dir = args.path_model
path_to_TF = args.path_TF
chunk = args.chunk

col = htcondor.Collector()
credd = htcondor.Credd()
credd.add_user_cred(htcondor.CredTypes.Kerberos, None)

# Read config file in 'conf'
with open(path_to_conf) as f:
    try:
        conf = yaml.safe_load(f)
    except yaml.YAMLError as exc:
        print(exc)

print("Path to root & outputDir: \n", conf)

basedir = conf['path_to_root']
outputDir = conf['path_to_outputDir']

sub = htcondor.Submit()

if interactive:
    sub['InteractiveJob'] = True

if model == "huber_mmd":
    sub['Executable'] = f"{basedir}/jobs/script_condor_pretraining_huber_mmd.sh"
    sub['Error'] = f"{basedir}/jobs/error/huber-mmd-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/huber-mmd-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/huber-mmd-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd/pretraining_huber_mmd_v{version}.yaml {outputDir}/flow_pretraining_huber_mmd"

    
elif model == "mmd_huber":
    sub['Executable'] = f"{basedir}/jobs/script_condor_pretraining_mmd_huber.sh"
    sub['Error'] = f"{basedir}/jobs/error/mmd-huber-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/mmd-huber-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/mmd-huber-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_mmd_huber/pretraining_mmd_huber_v{version}.yaml {outputDir}/flow_pretraining_mmd_huber"  

elif model == "huber_mmd_labframe":
    sub['Executable'] = f"{basedir}/jobs/script_condor_pretraining_huber_mmd_labframe.sh"
    sub['Error'] = f"{basedir}/jobs/error/huber-mmd-labframe-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/huber-mmd-labframe-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/huber-mmd-labframe-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"testmatch"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/flow_pretraining_huber_mmd_labframe"

elif model == "huber_mmd_labframe_gluon":
    sub['Executable'] = f"{basedir}/jobs/script_condor_pretraining_huber_mmd_labframe_gluon.sh"
    sub['Error'] = f"{basedir}/jobs/error/huber-mmd-labframe-gluon-{version}-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/huber-mmd-labframe-gluon-{version}-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/huber-mmd-labframe-gluon-{version}-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"testmatch"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/flow_pretraining_huber_mmd_labframe_gluon"

elif model == "huber_mmd_labframe_gluon_distributed":
    sub['Executable'] = f"{basedir}/jobs/script_condor_pretraining_distributed_labframe_gluon.sh"
    sub['Error'] = f"{basedir}/jobs/error/huber-mmd-labframe-gluon-distributed-{version}-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/huber-mmd-labframe-gluon-distributed-{version}-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/huber-mmd-labframe-gluon-distributed-{version}-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"testmatch"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/flow_pretraining_huber_mmd_labframe_gluon/distrubuted_lxplus"


elif model == "flow_labframe":
    sub['Executable'] = f"{basedir}/jobs/script_condor_flow_labframe.sh"
    sub['Error'] = f"{basedir}/jobs/error/flow-pretrain-labframe-v{version}-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/flow-pretrain-labframe-v{version}-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/flow-pretrain-labframe-v{version}-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/flow_pretrain_labframe_logit/flow_pretrain_labframe_logit_v{version}.yaml {outputDir}/flow_pretrain_labframe_logit"


elif model == "flow_labframe_psloss":
    sub['Executable'] = f"{basedir}/jobs/script_condor_flow_labframe_psloss.sh"
    sub['Error'] = f"{basedir}/jobs/error/flow-pretrain-labframe-psloss-v{version}-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/flow-pretrain-labframe-psloss-v{version}-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/flow-pretrain-labframe-psloss-v{version}-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/flow_pretrain_labframe_psloss_logit/flow_labframe_psloss_v{version}.yaml {outputDir}/flow_labframe_psloss_logit"


elif model == "flow_labframe_sampling":
    sub['Executable'] = f"{basedir}/jobs/script_condor_flow_labframe_sampling.sh"
    sub['Error'] = f"{basedir}/jobs/error/flow-pretrain-labframe-sampling-v{version}-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/flow-pretrain-labframe-sampling-v{version}-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/flow-pretrain-labframe-sampling-v{version}-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/flow_pretrain_labframe_sampling/flow_labframe_sampling_v{version}.yaml {outputDir}/flow_labframe_sampling"


elif model == "flow_nopretrain":
    sub['Executable'] = f"{basedir}/jobs/script_condor_flow_nopretrain.sh"
    sub['Error'] = f"{basedir}/jobs/error/flow-nopretrain-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/flow-nopretrain-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/flow-nopretrain-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/flow_nopretrain_spanet_logit/flow_nopretrain_v{version}.yaml {outputDir}/flow_nopretrain_spanet_logit"


elif model == "flow_evaluation_labframe":
    sub['Executable'] = f"{basedir}/jobs/script_condor_flow_evaluation_labframe.sh"
    sub['Error'] = f"{basedir}/jobs/error/flow-evaluation-labframe-v{version}-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/flow-evaluation-labframe-v{version}-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/flow-evaluation-labframe-v{version}-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"workday"'
    sub['arguments'] = " ".join(args.args)

elif model == "transfer_flow_firstVersion":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transfer_flow.sh"
    sub['Error'] = f"{basedir}/jobs/error/transfer_flow-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transfer_flow-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transfer_flow-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_v{version}"

elif model == "transfer_flow_paperVersion":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transfer_flow_paper.sh"
    sub['Error'] = f"{basedir}/jobs/error/transfer_flow_paper-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transfer_flow_paper-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transfer_flow_paper-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paper_v{version}"

elif model == "run_transferFlow_paperVersion-Nofake_partons-NoBtag":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transfer_flow_paper_Nofakepartons_nobtag.sh"
    sub['Error'] = f"{basedir}/jobs/error/transfer_flow_paper_Nofakepartons_nobtag-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transfer_flow_paper_Nofakepartons_nobtag-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transfer_flow_paper_Nofakepartons_nobtag-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paper__Nofakepartons_nobtag_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion-pretrained":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_paperVersion-pretrained.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion-pretrained-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion-pretrained-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion-pretrained-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion-pretrained_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion-pretrained_v2":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_paperVersion-pretrained_v2.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion-pretrained_v2-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion-pretrained_v2-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion-pretrained_v2-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion-pretrained_2ndVersion_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion-pretrained_v2_onlyExist":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_paperVersion-pretrained_onlyExist.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion-pretrained_v2_onlyExist-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion-pretrained_v2_onlyExist-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion-pretrained_v2_onlyExist-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion-pretrained_2ndVersion_onlyExist_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion-pretrained_v3_onlyExist":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_paperVersion-pretrained_v3_onlyExist.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion-pretrained_v3_onlyExist-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion-pretrained_v3_onlyExist-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion-pretrained_v3_onlyExist-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion-pretrained_3rdVersion_onlyExist_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion-pretrained_v3_onlyExist_noMDMM":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_paperVersion-pretrained_v3_onlyExist-noMDMM.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion-pretrained_v3_onlyExist-noMDMM-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion-pretrained_v3_onlyExist-noMDMM-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion-pretrained_v3_onlyExist-noMDMM-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion-pretrained_3rdVersion_onlyExist_noMDMM_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion-pretrained_v3_onlyExist_leptonMET":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_paperVersion-pretrained_v3_onlyExist-leptonMET.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion-pretrained_v3_onlyExist-leptonMET-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion-pretrained_v3_onlyExist-leptonMET-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion-pretrained_v3_onlyExist-leptonMET-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion-pretrained_3rdVersion_onlyExist_leptonMET_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion-pretrained_v3_onlyExist_leptonMET_bPartons":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_paperVersion-pretrained_v3_onlyExist-leptonMET_bPartons.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion-pretrained_v3_onlyExist-leptonMET_bPartons-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion-pretrained_v3_onlyExist-leptonMET_bPartons-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion-pretrained_v3_onlyExist-leptonMET_bPartons-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion-pretrained_3rdVersion_onlyExist_leptonMET_bPartons_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion-pretrained_v3_onlyExist_leptonMET_OnlyPartons":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_paperVersion-pretrained_v3_onlyExist-leptonMET_OnlyPartons.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion-pretrained_v3_onlyExist-leptonMET_OnlyPartons-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion-pretrained_v3_onlyExist-leptonMET_OnlyPartons-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion-pretrained_v3_onlyExist-leptonMET_OnlyPartons-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion-pretrained_3rdVersion_onlyExist_leptonMET_OnlyPartons_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion-pretrained_v3_onlyExist_leptonMET_OnlyPartons_btag":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_paperVersion-pretrained_v3_onlyExist-leptonMET_OnlyPartons_btag.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion-pretrained_v3_onlyExist-leptonMET_OnlyPartons_btag-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion-pretrained_v3_onlyExist-leptonMET_OnlyPartons_btag-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion-pretrained_v3_onlyExist-leptonMET_OnlyPartons_btag-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion-pretrained_3rdVersion_onlyExist_leptonMET_OnlyPartons_btag_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_OnlyPartons_btag_boxcox":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_OnlyPartons_btag_boxcox.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_OnlyPartons_btag_boxcox-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_OnlyPartons_btag_boxcox-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_OnlyPartons_btag_boxcox-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_OnlyPartons_btag_boxcox_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_AllPartons_btag":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_AllPartons_btag.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_AllPartons_btag-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_AllPartons_btag-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_AllPartons_btag-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_AllPartons_btag_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_AllPartons_btag_4D":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_AllPartons_btag_4D.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_AllPartons_btag_4D-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_AllPartons_btag_4D-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_AllPartons_btag_4D-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_AllPartons_btag_4D_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_AllPartons_btag_autoreg":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_AllPartons_btag_autoreg.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_AllPartons_btag_autoreg-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_AllPartons_btag_autoreg-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_AllPartons_btag_autoreg-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_AllPartons_btag_autoreg_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_AllPartons_Nobtag_autoreg":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_AllPartons_Nobtag_autoreg.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_AllPartons_Nobtag_autoreg-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_AllPartons_Nobtag_autoreg-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_AllPartons_Nobtag_autoreg-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_AllPartons_Nobtag_autoreg_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_AllPartons_Nobtag_autoreg_latentSpace":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_AllPartons_Nobtag_autoreg_latentSpace.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_AllPartons_Nobtag_autoreg_latentSpace-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_AllPartons_Nobtag_autoreg_latentSpace-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_AllPartons_Nobtag_autoreg_latentSpace-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_AllPartons_Nobtag_autoreg_latentSpace_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_Propagators_autoreg_latentSpace":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_Propagators_autoreg_latentSpace.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_Propagators_autoreg_latentSpace-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_Propagators_autoreg_latentSpace-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_Propagators_autoreg_latentSpace-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_Propagators_autoreg_latentSpace_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_AllPartons_Nobtag_autoreg_latentSpace_gaussian":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_AllPartons_Nobtag_autoreg_latentSpace_gaussian.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_AllPartons_Nobtag_autoreg_latentSpace_gaussian-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_AllPartons_Nobtag_autoreg_latentSpace_gaussian-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_AllPartons_Nobtag_autoreg_latentSpace_gaussian-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_AllPartons_Nobtag_autoreg_latentSpace_gaussian_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_Propagators_autoreg_latentSpace_gaussian.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_v3":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_Propagators_autoreg_latentSpace_gaussian_v3.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_v3-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_v3-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_v3-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_v3_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"


elif model == "run_transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_v3_CM":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_Propagators_autoreg_latentSpace_gaussian_v3_CM.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_v3_CM-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_v3_CM-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_v3_CM-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_v3_CMv{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_v3_classifierLater":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_Propagators_autoreg_latentSpace_gaussian_v3_classifierLater.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_v3_classifierLater-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_v3_classifierLater-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_v3_classifierLater-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_v3_classifierLater_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"


elif model == "run_transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_v4":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_Propagators_autoreg_latentSpace_gaussian_v4.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_v4-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_v4-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_v4-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_v4_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ISRfirst":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_Propagators_autoreg_latentSpace_gaussian_ISRfirst.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ISRfirst-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ISRfirst-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ISRfirst-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ISRfirst_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ISRfifth":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_Propagators_autoreg_latentSpace_gaussian_ISRfifth.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ISRfifth-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ISRfifth-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ISRfifth-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ISRfifth_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_AllPartons_Nobtag_autoreg_latentSpace_gaussian_ptcut":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_AllPartons_Nobtag_autoreg_latentSpace_gaussian_ptcut.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_AllPartons_Nobtag_autoreg_latentSpace_gaussian_ptcut-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_AllPartons_Nobtag_autoreg_latentSpace_gaussian_ptcut-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_AllPartons_Nobtag_autoreg_latentSpace_gaussian_ptcut-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_AllPartons_Nobtag_autoreg_latentSpace_gaussian_ptcut_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_Propagators_autoreg_latentSpace_gaussian_ptcut.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_AllPartons_Propagators_latentSpace_gaussian_ptcut_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut_v3":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_Propagators_autoreg_latentSpace_gaussian_ptcut_v3.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut_v3-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut_v3-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut_v3-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_AllPartons_Propagators_latentSpace_gaussian_ptcut_v3_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"


elif model == "run_transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut_v4":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_Propagators_autoreg_latentSpace_gaussian_ptcut_v4.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut_v4-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut_v4-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut_v4-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_AllPartons_Propagators_latentSpace_gaussian_ptcut_v4_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut_ISRfirst":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_Propagators_autoreg_latentSpace_gaussian_ptcut_ISRfirst.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut_ISRfirst-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut_ISRfirst-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut_ISRfirst-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_AllPartons_Propagators_latentSpace_gaussian_ptcut_ISRfirst_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut_ISRfifth":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_Propagators_autoreg_latentSpace_gaussian_ptcut_ISRfifth.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut_ISRfifth-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut_ISRfifth-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut_ISRfifth-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_AllPartons_Propagators_latentSpace_gaussian_ptcut_ISRfifth_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_AllPartons_Nobtag_autoreg_latentSpace_gaussian_ptcut_classifier":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_AllPartons_Nobtag_autoreg_latentSpace_gaussian_ptcut_classifier.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_AllPartons_Nobtag_autoreg_latentSpace_gaussian_ptcut_classifier-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_AllPartons_Nobtag_autoreg_latentSpace_gaussian_ptcut_classifier-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_AllPartons_Nobtag_autoreg_latentSpace_gaussian_ptcut_classifier-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_AllPartons_Nobtag_autoreg_latentSpace_gaussian_ptcut_classifier_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut_classifier":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_Propagators_autoreg_latentSpace_gaussian_ptcut_classifier.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut_classifier-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut_classifier-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut_classifier-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_Propagators_autoreg_latentSpace_gaussian_ptcut_classifier_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_DecayPartons_Nobtag_autoreg_latentSpace_gaussian_ptcutPartons_classifier":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_DecayPartons_Nobtag_autoreg_latentSpace_gaussian_ptcutPartons_classifier.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_DecayPartons_Nobtag_autoreg_latentSpace_gaussian_ptcutPartons_classifier-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_DecayPartons_Nobtag_autoreg_latentSpace_gaussian_ptcutPartons_classifier-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_DecayPartons_Nobtag_autoreg_latentSpace_gaussian_ptcutPartons_classifier-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_DecayPartons_Nobtag_autoreg_latentSpace_gaussian_ptcutPartons_classifier_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_DecayPartons_Nobtag_autoreg_latentSpace_gaussian_ptcutPartons_classifier_sin":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_AllPartons_Nobtag_autoreg_latentSpace_gaussian_ptcut_classifier_sin.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion_DecayPartons_Nobtag_autoreg_latentSpace_gaussian_ptcutPartons_classifier_sin-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion_DecayPartons_Nobtag_autoreg_latentSpace_gaussian_ptcutPartons_classifier_sin-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion_DecayPartons_Nobtag_autoreg_latentSpace_gaussian_ptcutPartons_classifier_sin-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion_DecayPartons_Nobtag_autoreg_latentSpace_gaussian_ptcutPartons_classifier_sin_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_noGluonMMD":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_noGluonMMD.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_noGluonMMD_v3-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_noGluonMMD_v3-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_noGluonMMD_v3-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_noGluonMMD_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_propagators":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_propagators.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_propagators-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_propagators-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_propagators-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_propagators_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_old":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_old.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_old-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_old-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_old-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_old_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_old_withBoost":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_old_withBoost.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_old_withBoost-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_old_withBoost-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_old_withBoost-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_old_withBoost_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_old_withBoost_newPos":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_old_withBoost_newPos.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_old_withBoost_newPos-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_old_withBoost_newPos-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_old_withBoost_newPos-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_old_withBoost_newPos_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_old_decayAndPropag":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_old_decayAndPropag.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_old_withBoost_decayAndPropag-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_old_decayAndPropag_newPos-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_old_decayAndPropag_newPos-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_old_withBoost_decayAndPropag_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_old_decayAndPropag_arctanh_weights":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_old_decayAndPropag_arctanh_weights.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_old_withBoost_decayAndPropag_arctanh_weights-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_old_decayAndPropag_newPos_arctanh_weights-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_old_decayAndPropag_newPos_arctanh_weights-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_old_withBoost_decayAndPropag_arctanh_weights_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_old_decayAndPropag_arctanh_weights_moreMMD":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_old_decayAndPropag_arctanh_weights_moreMMD.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_old_withBoost_decayAndPropag_arctanh_weights_moreMMD-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_old_decayAndPropag_newPos_arctanh_weights_moreMMD-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_old_decayAndPropag_newPos_arctanh_weights_moreMMD-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_old_withBoost_decayAndPropag_arctanh_weights_moreMMD_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_old_decayAndPropag_noISR":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_old_decayAndPropag_noISR.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_old_withBoost_decayAndPropag_noISR-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_old_decayAndPropag_newPos_noISR-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_old_decayAndPropag_newPos_noISR-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_old_withBoost_decayAndPropag_noISR_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_old_decayAndPropag_noISR_arctanh":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_old_decayAndPropag_noISR_arctanh.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_old_withBoost_decayAndPropag_noISR_arctanh-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_old_decayAndPropag_newPos_noISR_arctanh-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_old_decayAndPropag_newPos_noISR_arctanh-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_old_withBoost_decayAndPropag_noISR_arctanh_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_old_decayAndPropag_noISR_arctanh_weights":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_old_decayAndPropag_noISR_arctanh_weights.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_old_withBoost_decayAndPropag_noISR_arctanh_weights-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_old_decayAndPropag_newPos_noISR_arctanh_weights-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_old_decayAndPropag_newPos_noISR_arctanh_weights-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_old_withBoost_decayAndPropag_noISR_arctanh_weights_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"
        
elif model == "run_ConditionalTransformer_v3_old_decay_sincos":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_old_decay_sincos.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_old_withBoost_decay_sincos-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_old_decay_sincos-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_old_decay_sincos-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_old_withBoost_decay_sincos_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_old_decay_arctanh":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_old_decay_arctanh.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_old_withBoost_decay_arctanh-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_old_decay_arctanh-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_old_decay_arctanh-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_old_withBoost_decay_arctanh_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_old_decay_v2":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_old_decay_v2.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_old_withBoost_decay_v2-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_old_decay_v2-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_old_decay_v2-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_old_withBoost_decay_v2_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_old_decay_v2_arctanh":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_old_decay_v2_arctanh.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_old_withBoost_decay_v2_arctanh-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_old_decay_v2_arctanh-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_old_decay_v2_arctanh-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_old_withBoost_decay_v2_arctanh_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_old_decay_v2_moreMDMM":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_old_decay_v2_moreMDMM.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_old_withBoost_decay_v2_moreMDMM-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_old_decay_v2_moreMDMM-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_old_decay_v2_moreMDMM-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_old_withBoost_decay_v2_moreMDMM_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"


elif model == "run_ConditionalTransformer_v3_combinedDecayAndPropag":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_combinedDecayAndPropag.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_combinedDecayAndPropag-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_combinedDecayAndPropag-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_combinedDecayAndPropag-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_combinedDecayAndPropag_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_combinedDecayAndPropag_moreMMD":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_combinedDecayAndPropag_moreMMD.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_combinedDecayAndPropag_moreMMD-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_combinedDecayAndPropag_moreMMD-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_combinedDecayAndPropag_moreMMD-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_combinedDecayAndPropag_moreMMD_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_combinedDecayAndPropag_moreMMD_mmdLoss":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_combinedDecayAndPropag_moreMMD_mmdLoss.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_combinedDecayAndPropag_moreMMD_mmdLoss-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_combinedDecayAndPropag_moreMMD_mmdLoss-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_combinedDecayAndPropag_moreMMD_mmdLoss-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_combinedDecayAndPropag_moreMMD_mmdLoss_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_combinedDecayAndPropag_anglesCM_moreMMD_mmdLoss":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_combinedDecayAndPropag_anglesCM_moreMMD_mmdLoss.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_combinedDecayAndPropag_anglesCM_moreMMD_mmdLoss-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_combinedDecayAndPropag_anglesCM_moreMMD_mmdLoss-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_combinedDecayAndPropag_moreMMD_anglesCM_mmdLoss-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_combinedDecayAndPropag_anglesCM_moreMMD_mmdLoss_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_combinedDecayAndPropag_anglesCM_moreMMD_mmdLoss_onlyRegressionLosses":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_combinedDecayAndPropag_anglesCM_moreMMD_mmdLoss_onlyRegressionLosses.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_combinedDecayAndPropag_anglesCM_moreMMD_mmdLoss_onlyRegressionLosses-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_combinedDecayAndPropag_anglesCM_moreMMD_mmdLoss_onlyRegressionLosses-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_combinedDecayAndPropag_moreMMD_anglesCM_mmdLoss_onlyRegressionLosses-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_combinedDecayAndPropag_anglesCM_moreMMD_mmdLoss_onlyRegressionLosses_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_combinedDecayAndPropag_anglesCM_1dMMDconstraints":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_combinedDecayAndPropag_anglesCM_1dMMDconstraints.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_combinedDecayAndPropag_anglesCM_1dMMDconstraint-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_combinedDecayAndPropag_anglesCM_1dMMDconstraint-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_combinedDecayAndPropag_moreMMD_anglesCM_1dMMDconstraint-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_combinedDecayAndPropag_anglesCM_1dMMDconstraint_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_combinedDecayAndPropag_anglesCM_1dMMDconstraints_optimized":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_combinedDecayAndPropag_anglesCM_1dMMDconstraints_optimized.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_combinedDecayAndPropag_anglesCM_1dMMDconstraint_optimized-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_combinedDecayAndPropag_anglesCM_1dMMDconstraint_optimized-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_combinedDecayAndPropag_moreMMD_anglesCM_1dMMDconstraint_optimized-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_combinedDecayAndPropag_anglesCM_1dMMDconstraint_optimized_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        print(args_string)
        exit(0)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_combinedDecayAndPropag_anglesCM_1dMMDconstraints_phiOptimized":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_combinedDecayAndPropag_anglesCM_1dMMDconstraints_phiOptimized.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_combinedDecayAndPropag_anglesCM_1dMMDconstraint_phiOptimized-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_combinedDecayAndPropag_anglesCM_1dMMDconstraint_phiOptimized-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_combinedDecayAndPropag_moreMMD_anglesCM_1dMMDconstraint_phiOptimized-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_combinedDecayAndPropag_anglesCM_1dMMDconstraint_phiOptimized_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_Propagators_1dMMDconstraints_phiOptimized":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_Propagators_1dMMDconstraints_phiOptimized.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_Propagators_1dMMDconstraint_phiOptimized-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_Propagators_1dMMDconstraint_phiOptimized-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_Propagators_1dMMDconstraint_phiOptimized-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_Propagators_1dMMDconstraint_phiOptimized_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_Propagators_1dMMDconstraints_phiOptimized_ISR":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_Propagators_1dMMDconstraints_phiOptimized_ISR.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_Propagators_1dMMDconstraint_phiOptimized_ISR-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_Propagators_1dMMDconstraint_phiOptimized_ISR-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_Propagators_1dMMDconstraint_phiOptimized_ISR-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_Propagators_1dMMDconstraint_phiOptimized_ISR_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_Propagators_1dMMDconstraints_phiOptimized_ISR_CM":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_Propagators_1dMMDconstraints_phiOptimized_ISR_CM.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_Propagators_1dMMDconstraint_phiOptimized_ISR_CM-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_Propagators_1dMMDconstraint_phiOptimized_ISR_CM-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_Propagators_1dMMDconstraint_phiOptimized_ISR_CM-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_Propagators_1dMMDconstraint_phiOptimized_ISR_CM_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"


elif model == "run_ConditionalTransformer_v3_Propagators_1dMMDconstraints_phiOptimized_ISR_alignTF":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_Propagators_1dMMDconstraints_phiOptimized_ISR_alignTF.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_Propagators_1dMMDconstraint_phiOptimized_ISR_alignTF-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_Propagators_1dMMDconstraint_phiOptimized_ISR_alignTF-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_Propagators_1dMMDconstraint_phiOptimized_ISR_alignTF-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_Propagators_1dMMDconstraint_phiOptimized_ISR_alignTF_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_flow_phiOptimized_ISR_alignTF":
    sub['Executable'] = f"{basedir}/jobs/script_flow_phiOptimized_ISR_alignTF.sh"
    sub['Error'] = f"{basedir}/jobs/error/flow_phiOptimized_ISR_alignTF-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/flow_phiOptimized_ISR_alignTF-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/flow_phiOptimized_ISR_alignTF-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/flow_phiOptimized_ISR_alignTF_v{version} {path_to_dir}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_Decay_1dMMDconstraints_phiOptimized_SimpleLoss":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_Decay_1dMMDconstraints_phiOptimized_SimpleLoss.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_Decay_1dMMDconstraint_phiOptimized_SimpleLoss-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_Decay_1dMMDconstraint_phiOptimized_SimpleLoss-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_Decay_1dMMDconstraint_phiOptimized_SimpleLoss-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_Decay_1dMMDconstraint_phiOptimized_SimpleLoss_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_Decay_1dMMDconstraints_phiOptimized_SimpleLoss_ISR":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_Decay_1dMMDconstraints_phiOptimized_SimpleLoss_ISR.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_Decay_1dMMDconstraint_phiOptimized_SimpleLoss_ISR-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_Decay_1dMMDconstraint_phiOptimized_SimpleLoss_ISR-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_Decay_1dMMDconstraint_phiOptimized_SimpleLoss_ISR-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_Decay_1dMMDconstraint_phiOptimized_SimpleLoss_ISR_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"


elif model == "run_ConditionalTransformer_v3_combinedDecayAndPropag_anglesCM_1dMMDconstraints_phiOptimized_thadB":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_combinedDecayAndPropag_anglesCM_1dMMDconstraints_phiOptimized_thadB.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_combinedDecayAndPropag_anglesCM_1dMMDconstraint_phiOptimized_thadB-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_combinedDecayAndPropag_anglesCM_1dMMDconstraint_phiOptimized_thadB-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_combinedDecayAndPropag_moreMMD_anglesCM_1dMMDconstraint_phiOptimized_thadB-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_combinedDecayAndPropag_anglesCM_1dMMDconstraint_phiOptimized_thadB_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_allCheck":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_allCheck.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_allCheck-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_allCheck-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_allCheck-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_allCheck_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_ConditionalTransformer_v3_Propagators_jetPtOrder_noBtag":
    sub['Executable'] = f"{basedir}/jobs/script_condor_condTransformer_v3_Propagators_jetPtOrder_noBtag.sh"
    sub['Error'] = f"{basedir}/jobs/error/condTransformer_v3_Propagators_jetPtOrder_noBtag-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/condTransformer_v3_Propagators_jetPtOrder_noBtag-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/condTransformer_v3_Propagators_jetPtOrder_noBtag-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/condTransformer_v3_Propagators_jetPtOrder_noBtag_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_flowModel_DecayProducts_LikelihoodLoss":
    sub['Executable'] = f"{basedir}/jobs/script_condor_flowModel_DecayProducts_Likelihood.sh"
    sub['Error'] = f"{basedir}/jobs/error/flowModel_DecayProducts_Likelihood-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/flowModel_DecayProducts_Likelihood-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/flowModel_DecayProducts_Likelihood-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/flowModel_DecayProducts_Likelihood_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_flowModel_DecayProducts_LikelihoodLoss_newToken":
    sub['Executable'] = f"{basedir}/jobs/script_condor_flowModel_DecayProducts_Likelihood_newToken.sh"
    sub['Error'] = f"{basedir}/jobs/error/flowModel_DecayProducts_Likelihood_newToken-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/flowModel_DecayProducts_Likelihood_newToken-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/flowModel_DecayProducts_Likelihood_newToken-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/flowModel_DecayProducts_Likelihood_newToken_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_flowModel_DecayProducts_SampleLikelihoodLosses":
    sub['Executable'] = f"{basedir}/jobs/script_condor_flowModel_DecayProducts_SampleLikelihoodLosses.sh"
    sub['Error'] = f"{basedir}/jobs/error/flowModel_DecayProducts_SampleLikelihoodLosses-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/flowModel_DecayProducts_SampleLikelihoodLosses-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/flowModel_DecayProducts_SampleLikelihoodLosses-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/flowModel_DecayProducts_SampleLikelihoodLosses_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_flowModel_DecayProducts_onlyAngles_LikelihoodLosses":
    sub['Executable'] = f"{basedir}/jobs/script_condor_flowModel_DecayProducts_onlyAngles_LikelihoodLosses.sh"
    sub['Error'] = f"{basedir}/jobs/error/flowModel_DecayProducts_onlyAngles_LikelihoodLosses-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/flowModel_DecayProducts_onlyAngles_LikelihoodLosses-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/flowModel_DecayProducts_onlyAngles_LikelihoodLosses-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/flowModel_DecayProducts_onlyAngles_LikelihoodLosses_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_flowModel_DecayProducts_onlyAngles_SampleLosses":
    sub['Executable'] = f"{basedir}/jobs/script_condor_flowModel_DecayProducts_onlyAngles_SampleLosses.sh"
    sub['Error'] = f"{basedir}/jobs/error/flowModel_DecayProducts_onlyAngles_SampleLosses-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/flowModel_DecayProducts_onlyAngles_SampleLosses-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/flowModel_DecayProducts_onlyAngles_SampleLosses-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/flowModel_DecayProducts_onlyAngles_Losses_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_flowModel_DecayProducts_onlyAngles_SampleLikelihoodLosses":
    sub['Executable'] = f"{basedir}/jobs/script_condor_flowModel_DecayProducts_onlyAngles_SampleLikelihoodLosses.sh"
    sub['Error'] = f"{basedir}/jobs/error/flowModel_DecayProducts_onlyAngles_SampleLikelihoodLosses-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/flowModel_DecayProducts_onlyAngles_SampleLikelihoodLosses-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/flowModel_DecayProducts_onlyAngles_SampleLikelihoodLosses-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/flowModel_DecayProducts_onlyAngles_SampleLikelihoodLosses_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_flowModel_DecayProducts_onlyAngles_SampleLikelihoodLosses_eta":
    sub['Executable'] = f"{basedir}/jobs/script_condor_flowModel_DecayProducts_onlyAngles_SampleLikelihoodLosses_eta.sh"
    sub['Error'] = f"{basedir}/jobs/error/flowModel_DecayProducts_onlyAngles_SampleLikelihoodLosses_eta-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/flowModel_DecayProducts_onlyAngles_SampleLikelihoodLosses_eta-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/flowModel_DecayProducts_onlyAngles_SampleLikelihoodLosses_eta-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/flowModel_DecayProducts_onlyAngles_SampleLikelihoodLosses_eta_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_flowModel_DecayProducts_SampleLikelihoodLosses_eta":
    sub['Executable'] = f"{basedir}/jobs/script_condor_flowModel_DecayProducts_SampleLikelihoodLosses_eta.sh"
    sub['Error'] = f"{basedir}/jobs/error/flowModel_DecayProducts_SampleLikelihoodLosses_eta-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/flowModel_DecayProducts_SampleLikelihoodLosses_eta-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/flowModel_DecayProducts_SampleLikelihoodLosses_eta-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/flowModel_DecayProducts_SampleLikelihoodLosses_eta_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_flowModel_DecayProducts_SampleLikelihoodLosses_eta_mdmm":
    sub['Executable'] = f"{basedir}/jobs/script_condor_flowModel_DecayProducts_SampleLikelihoodLosses_eta_mdmm.sh"
    sub['Error'] = f"{basedir}/jobs/error/flowModel_DecayProducts_SampleLikelihoodLosses_eta_mdmm-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/flowModel_DecayProducts_SampleLikelihoodLosses_eta_mdmm-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/flowModel_DecayProducts_SampleLikelihoodLosses_eta_mdmm-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/flowModel_DecayProducts_SampleLikelihoodLosses_eta_mdmm_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_flowModel_DecayProducts_SampleLikelihoodLosses_eta_mdmm_newToken":
    sub['Executable'] = f"{basedir}/jobs/script_condor_flowModel_DecayProducts_SampleLikelihoodLosses_eta_mdmm_newToken.sh"
    sub['Error'] = f"{basedir}/jobs/error/flowModel_DecayProducts_SampleLikelihoodLosses_eta_newToken_mdmm-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/flowModel_DecayProducts_SampleLikelihoodLosses_eta_newToken_mdmm-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/flowModel_DecayProducts_SampleLikelihoodLosses_eta_mdmm_newToken-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/flowModel_DecayProducts_SampleLikelihoodLosses_eta_mdmm_newToken_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_flowModel_DecayProducts_SampleLikelihoodLosses_eta_mdmm_newToken_DNNtoken":
    sub['Executable'] = f"{basedir}/jobs/script_condor_flowModel_DecayProducts_SampleLikelihoodLosses_eta_mdmm_newToken_DNNtoken.sh"
    sub['Error'] = f"{basedir}/jobs/error/flowModel_DecayProducts_SampleLikelihoodLosses_eta_newToken_mdmm_DNNtoken-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/flowModel_DecayProducts_SampleLikelihoodLosses_eta_newToken_mdmm_DNNtoken-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/flowModel_DecayProducts_SampleLikelihoodLosses_eta_mdmm_newToken_DNNtoken-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/flowModel_DecayProducts_SampleLikelihoodLosses_eta_mdmm_newToken_DNNtoken_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_flowModel_DecayProducts_SampleLikelihoodLosses_newToken":
    sub['Executable'] = f"{basedir}/jobs/script_condor_flowModel_DecayProducts_SampleLikelihoodLosses_newToken.sh"
    sub['Error'] = f"{basedir}/jobs/error/flowModel_DecayProducts_SampleLikelihoodLosses_newToken-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/flowModel_DecayProducts_SampleLikelihoodLosses_newToken-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/flowModel_DecayProducts_SampleLikelihoodLosse_newTokens-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/flowModel_DecayProducts_SampleLikelihoodLosses_newToken_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_flowModel_Propag_SampleLikelihoodLosses":
    sub['Executable'] = f"{basedir}/jobs/script_condor_flowModel_Propag_SampleLikelihoodLosses.sh"
    sub['Error'] = f"{basedir}/jobs/error/flowModel_Propag_SampleLikelihoodLosses-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/flowModel_Propag_SampleLikelihoodLosses-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/flowModel_Propag_SampleLikelihoodLosses-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/flowModel_Propag_SampleLikelihoodLosses_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_flowModel_Propag_SampleLikelihoodLosses_ISR":
    sub['Executable'] = f"{basedir}/jobs/script_condor_flowModel_Propag_SampleLikelihoodLosses_ISR.sh"
    sub['Error'] = f"{basedir}/jobs/error/flowModel_Propag_SampleLikelihoodLosses_ISR-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/flowModel_Propag_SampleLikelihoodLosses_ISR-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/flowModel_Propag_SampleLikelihoodLosses_ISR-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/flowModel_Propag_SampleLikelihoodLosses_ISR_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_unfoldingFlow_v2":
    sub['Executable'] = f"{basedir}/jobs/script_condor_unfoldingFlow_v2.sh"
    sub['Error'] = f"{basedir}/jobs/error/unfoldingFlow_v2-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/unfoldingFlow_v2-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/unfoldingFlow_v2-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/unfoldingFlow_v2_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_unfoldingFlow_basicLoss_noRegression_alignTF":
    sub['Executable'] = f"{basedir}/jobs/script_condor_unfoldingFlow_basicLoss_noRegression_alignTF.sh"
    sub['Error'] = f"{basedir}/jobs/error/unfoldingFlow_basicLoss_noRegression_alignTF-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/unfoldingFlow_basicLoss_noRegression_alignTF-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/unfoldingFlow_basicLoss_noRegression_alignTF-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/unfoldingFlow_basicLoss_noRegression_alignTF_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_unfoldingFlow_basicLoss_withRegression_alignTF":
    sub['Executable'] = f"{basedir}/jobs/script_condor_unfoldingFlow_basicLoss_withRegression_alignTF.sh"
    sub['Error'] = f"{basedir}/jobs/error/unfoldingFlow_basicLoss_withRegression_alignTF-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/unfoldingFlow_basicLoss_withRegression_alignTF-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/unfoldingFlow_basicLoss_withRegression_alignTF-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/pretraining_huber_mmd_labframe_gluon/pretraining_huber_mmd_labframe_v{version}.yaml {outputDir}/unfoldingFlow_basicLoss_withRegression_alignTF_v{version} {path_to_dir}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"
        
elif model == "run_transferFlow_paperVersion-pretrained_v2_ExistCond":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_paperVersion-pretrained_ExistCond.sh"
    sub['Error'] = f"{basedir}/jobs/error/transferFlow_paperVersion-pretrained_v2_ExistCond-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transferFlow_paperVersion-pretrained_v2_ExistCond-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transferFlow_paperVersion-pretrained_v2_ExistCond-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paperVersion-pretrained_2ndVersion_ExistCond_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion-MDMM-Nofake_partons-NoBtag":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transfer_flow_paper_MDMM_Nofakepartons_nobtag.sh"
    sub['Error'] = f"{basedir}/jobs/error/transfer_flow_paper_MDMM_Nofakepartons_nobtag-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transfer_flow_paper_MDMM_Nofakepartons_nobtag-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transfer_flow_paper_Nofakepartons_MDMM_nobtag-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paper_MDMM_Nofakepartons_nobtag_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"


elif model == "run_transferFlow_paperVersion_orderedByJets":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transfer_flow_paper_orderedByJets.sh"
    sub['Error'] = f"{basedir}/jobs/error/transfer_flow_paper__orderedByJets-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transfer_flow_paper__orderedByJets-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transfer_flow_paper__orderedByJets-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paper_orderedByJets_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_orderedByJets_sampling":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transfer_flow_paper_orderedByJets_sampling.sh"
    sub['Error'] = f"{basedir}/jobs/error/transfer_flow_paper__orderedByJets_sampling-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transfer_flow_paper__orderedByJets_sampling-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transfer_flow_paper__orderedByJets_sampling-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} {path_to_dir}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_orderedByJets_ptJac":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transfer_flow_paper_orderedByJets_ptJac.sh"
    sub['Error'] = f"{basedir}/jobs/error/transfer_flow_paper__orderedByJets_ptJac-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transfer_flow_paper__orderedByJets_ptJac-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transfer_flow_paper__orderedByJets-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paper_orderedByJets_ptJac_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"


elif model == "pretrain_transferFlow_exist":
    sub['Executable'] = f"{basedir}/jobs/script_condor_pretrain_transferFlow_exist.sh"
    sub['Error'] = f"{basedir}/jobs/error/pretrain_transferFlow_exist-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/pretrain_transferFlow_exist-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/pretrain_transferFlow_exist-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_paper_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_paper_MDMM_Nofakepartons_nobtag_v{version}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion-lepMET_sampling":    
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_paper_lepMET_sampling.sh"
    sub['Error'] = f"{basedir}/jobs/error/transfer_flow_paper_lepMET_sampling-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transfer_flow_paper_lepMET_sampling-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transfer_flow_paper_lepMET_sampling-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} {path_to_dir}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion-lepMET_onlyPartons_sampling":    
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_paper_lepMET_onlyPartons_sampling.sh"
    sub['Error'] = f"{basedir}/jobs/error/transfer_flow_paper_lepMET_onlyPartons_sampling-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transfer_flow_paper_lepMET_onlyPartons_sampling-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transfer_flow_paper_lepMET_onlyPartons_sampling-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} {path_to_dir}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_AllPartons_autoreg_sampling":    
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_paper_AllPartons_autoreg_sampling.sh"
    sub['Error'] = f"{basedir}/jobs/error/transfer_flow_paper_AllPartons_autoreg_sampling-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transfer_flow_paper_AllPartons_autoreg_sampling-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transfer_flow_paper_AllPartons_autoreg_sampling-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} {path_to_dir}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_AllPartons_autoreg_gaussian_sampling":    
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_paper_AllPartons_autoreg_gaussian_sampling.sh"
    sub['Error'] = f"{basedir}/jobs/error/transfer_flow_paper_AllPartons_autoreg_gaussian_sampling-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transfer_flow_paper_AllPartons_autoreg_gaussian_sampling-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transfer_flow_paper_AllPartons_autoreg_gaussian_sampling-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} {path_to_dir}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_AllPartons_autoreg_gaussian_ptcut_sampling":    
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_paper_AllPartons_autoreg_gaussian_ptcut_sampling.sh"
    sub['Error'] = f"{basedir}/jobs/error/transfer_flow_paper_AllPartons_autoreg_gaussian_ptcut_sampling-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transfer_flow_paper_AllPartons_autoreg_gaussian_ptcut_sampling-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transfer_flow_paper_AllPartons_autoreg_gaussian_ptcut_sampling-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} {path_to_dir}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_Propagators_autoreg_gaussian_sampling":    
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_paper_Propagators_autoreg_gaussian_sampling.sh"
    sub['Error'] = f"{basedir}/jobs/error/transfer_flow_paper_Propagators_autoreg_gaussian_sampling-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transfer_flow_paper_Propagators_autoreg_gaussian_sampling-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transfer_flow_paper_Propagators_autoreg_gaussian_sampling-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} {path_to_dir}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_Propagators_autoreg_gaussian_v4_sampling":    
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_Propagators_autoreg_latentSpace_gaussian_v4_sampling.sh"
    sub['Error'] = f"{basedir}/jobs/error/transfer_flow_paper_Propagators_autoreg_gaussian_v4_sampling-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transfer_flow_paper_Propagators_autoreg_gaussian_v4_sampling-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transfer_flow_paper_Propagators_autoreg_gaussian_v4_sampling-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} {path_to_dir}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_Propagators_autoreg_gaussian_v3_sampling":    
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_Propagators_autoreg_latentSpace_gaussian_v3_sampling.sh"
    sub['Error'] = f"{basedir}/jobs/error/transfer_flow_paper_Propagators_autoreg_gaussian_v3_sampling-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transfer_flow_paper_Propagators_autoreg_gaussian_v3_sampling-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transfer_flow_paper_Propagators_autoreg_gaussian_v3_sampling-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} {path_to_dir}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_Propagators_autoreg_gaussian_ptcut_v4_sampling":    
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_Propagators_autoreg_latentSpace_gaussian_ptcut_v4_sampling.sh"
    sub['Error'] = f"{basedir}/jobs/error/transfer_flow_paper_Propagators_autoreg_gaussian_ptcut_v4_sampling-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transfer_flow_paper_Propagators_autoreg_gaussian_ptcut_v4_sampling-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transfer_flow_paper_Propagators_autoreg_gaussian_ptcut_v4_sampling-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} {path_to_dir}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_transferFlow_paperVersion_Propagators_autoreg_gaussian_ptcut_v3_sampling":    
    sub['Executable'] = f"{basedir}/jobs/script_condor_transferFlow_Propagators_autoreg_latentSpace_gaussian_ptcut_v3_sampling.sh"
    sub['Error'] = f"{basedir}/jobs/error/transfer_flow_paper_Propagators_autoreg_gaussian_ptcut_v3_sampling-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transfer_flow_paper_Propagators_autoreg_gaussian_ptcut_v3_sampling-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transfer_flow_paper_Propagators_autoreg_gaussian_ptcut_v3_sampling-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} {path_to_dir}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_flowPropag_sample":    
    sub['Executable'] = f"{basedir}/jobs/script_condor_flowPropag_sample.sh"
    sub['Error'] = f"{basedir}/jobs/error/run_flowPropag_sample-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/run_flowPropag_sample-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/run_flowPropag_sample-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} {path_to_dir}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_flowPropag_EFT_sample":    
    sub['Executable'] = f"{basedir}/jobs/script_condor_flowPropag_EFT_sample.sh"
    sub['Error'] = f"{basedir}/jobs/error/run_flowPropag_EFT_sample-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/run_flowPropag_EFT_sample-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/run_flowPropag_EFT_sample-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} {path_to_dir}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_flowPropag_EFT_TF_sample":    
    sub['Executable'] = f"{basedir}/jobs/script_condor_flowPropag_EFT_TF_sample.sh"
    sub['Error'] = f"{basedir}/jobs/error/run_flowPropag_EFT_TF_sample-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/run_flowPropag_EFT_TF_sample-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/run_flowPropag_EFT_TF_sample-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} {path_to_dir} {path_to_TF} {chunk}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "run_flowPropag_EFT_TF_sample_goodEvents":    
    sub['Executable'] = f"{basedir}/jobs/script_condor_flowPropag_EFT_TF_sample_goodEvents.sh"
    sub['Error'] = f"{basedir}/jobs/error/run_flowPropag_EFT_TF_sample_goodEvents-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/run_flowPropag_EFT_TF_sample_goodEvents-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/run_flowPropag_EFT_TF_sample_goodEvents-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} {path_to_dir} {path_to_TF} {chunk}"
    if args.args != None:
        args_string = " ".join(args.args)
        sub['arguments'] = sub['arguments'] + f" {args_string}"

elif model == "transfer_flow_idea3":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transfer_flow_idea3.sh"
    sub['Error'] = f"{basedir}/jobs/error/transfer_flow_idea3-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transfer_flow_idea3-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transfer_flow_idea3-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_idea3_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_idea3_train_v{version}"

elif model == "transfer_flow_idea3_conditioned":
    sub['Executable'] = f"{basedir}/jobs/script_condor_transfer_flow_idea3_conditioned.sh"
    sub['Error'] = f"{basedir}/jobs/error/transfer_flow_idea3_conditioned-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/transfer_flow_idea3_conditioned-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/transfer_flow_idea3_conditioned-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/transferFlow_idea3_conditioning_train/transferFlow_v{version}.yaml {outputDir}/transferFlow_idea3_conditioned_train_v{version}"
    
elif model == "classifier_nojets":
    sub['Executable'] = f"{basedir}/jobs/script_classifier_nojets.sh"
    sub['Error'] = f"{basedir}/jobs/error/classifier_nojets-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/classifier_nojets-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/classifier_nojets-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"longlunch"'
    sub['arguments'] = f"{basedir} configs/classifier_no_jets/classifier_v{version}.yaml {outputDir}/classifier_nojets_train_v{version}"
    
elif model == "classifier_nojets_v2":
    sub['Executable'] = f"{basedir}/jobs/script_classifier_nojets_v2.sh"
    sub['Error'] = f"{basedir}/jobs/error/classifier_nojets_v2-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/classifier_nojets_v2-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/classifier_nojets_v2-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"longlunch"'
    sub['arguments'] = f"{basedir} configs/classifier_no_jets_v2/classifier_v{version}.yaml {outputDir}/classifier_nojets_2nd_train_v{version}"
    
elif model == "classifier_nojets_v3":
    sub['Executable'] = f"{basedir}/jobs/script_classifier_nojets_v3.sh"
    sub['Error'] = f"{basedir}/jobs/error/classifier_nojets_v3-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/classifier_nojets_v3-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/classifier_nojets_v3-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"longlunch"'
    sub['arguments'] = f"{basedir} configs/classifier_no_jets_v2/classifier_v{version}.yaml {outputDir}/classifier_nojets_3rdVersion_2024_v{version}"

elif model == "classifier_exist":
    sub['Executable'] = f"{basedir}/jobs/script_condor_classifier_exist.sh"
    sub['Error'] = f"{basedir}/jobs/error/classifier_exist-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/classifier_exist-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/classifier_exist-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/preTrain_exist/transferFlow_v{version}.yaml {outputDir}/classifier_exist_{version}"

elif model == "classifier_exist_weights":
    sub['Executable'] = f"{basedir}/jobs/script_condor_classifier_exist_weights.sh"
    sub['Error'] = f"{basedir}/jobs/error/classifier_exist_weights-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/classifier_exist_weights-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/classifier_exist_weights-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/preTrain_exist/transferFlow_v{version}.yaml {outputDir}/classifier_exist_weights_{version}"

elif model == "classifier_exist_matched":
    sub['Executable'] = f"{basedir}/jobs/script_condor_classifier_exist_matched.sh"
    sub['Error'] = f"{basedir}/jobs/error/classifier_exist-matched-$(ClusterId).$(ProcId).err"
    sub['Output'] = f"{basedir}/jobs/output/classifier_exist-matched-$(ClusterId).$(Proc1Id).out"
    sub['Log'] = f"{basedir}/jobs/log/classifier_exist-matched-$(ClusterId).log"
    sub['MY.SendCredential'] = True
    sub['MY.SingularityImage'] = '"/cvmfs/unpacked.cern.ch/gitlab-registry.cern.ch/dvalsecc/memflow:latest"'
    sub['+JobFlavour'] = '"nextweek"'
    sub['arguments'] = f"{basedir} configs/preTrain_exist/transferFlow_v{version}.yaml {outputDir}/classifier_exist_matched_{version}"

    if args.args != None:
            args_string = " ".join(args.args)
            sub['arguments'] = sub['arguments'] + f" {args_string}"

# General
sub['request_cpus'] = f"{args.ncpu}"
sub['request_gpus'] = f"{args.ngpu}"

if args.good_gpus:
    sub['requirements'] = 'regexp("A100", TARGET.GPUs_DeviceName) || regexp("V100", TARGET.GPUs_DeviceName) || regexp("H100", TARGET.GPUs_DeviceName)'
elif args.good_gpus_a100:
    sub['requirements'] = 'regexp("A100", TARGET.GPUs_DeviceName)'
elif args.good_gpus_v100:
    sub['requirements'] = 'regexp("V100", TARGET.GPUs_DeviceName)'

no_config = ["flow_evaluation_labframe", "run_transferFlow_paperVersion-lepMET_sampling", "run_transferFlow_paperVersion-lepMET_onlyPartons_sampling", "run_transferFlow_paperVersion_AllPartons_autoreg_sampling", "run_transferFlow_paperVersion_AllPartons_autoreg_gaussian_sampling", "run_transferFlow_paperVersion_AllPartons_autoreg_gaussian_ptcut_sampling", "run_transferFlow_paperVersion_Propagators_autoreg_gaussian_sampling", "run_transferFlow_paperVersion_Propagators_autoreg_gaussian_ptcut_sampling", "run_transferFlow_paperVersion_Propagators_autoreg_gaussian_v4_sampling", "run_transferFlow_paperVersion_Propagators_autoreg_gaussian_ptcut_v4_sampling", "run_transferFlow_paperVersion_Propagators_autoreg_gaussian_v3_sampling", "run_transferFlow_paperVersion_Propagators_autoreg_gaussian_ptcut_v3_sampling", "run_flowPropag_sample", "run_flowPropag_EFT_sample", "run_flowPropag_EFT_TF_sample", "run_flowPropag_EFT_TF_sample_goodEvents", "run_transferFlow_paperVersion_orderedByJets_sampling"]

if model not in no_config  and not os.path.exists(f"{basedir}/{sub['arguments'].split()[1]}"):
    print("Missing configuration file! The jobs has not been submitted")
    exit(1)
    
print(sub)
if not dry:
    schedd = htcondor.Schedd()
    with schedd.transaction() as txn:
        cluster_id = sub.queue(txn)

    print(f"Submitted to {cluster_id:=}")
