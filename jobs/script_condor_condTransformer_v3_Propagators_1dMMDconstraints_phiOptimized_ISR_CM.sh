#!/bin/bash

#creating venv in local job dir
python -m venv myenv --system-site-packages
source myenv/bin/activate

cd $1
pip install -e .

python scripts/CondTransformer_CM_Jan25/run_pretraining_UnfoldingFlow_Propag_arctanh_1dMMDconstraint_phiOptimized_ISR_CM.py \
      --path-config  "$1/$2" --output-dir  $3 --on-GPU $4